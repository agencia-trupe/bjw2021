<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TiposRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'titulo'    => 'required',
            'titulo_en' => 'required',
            'titulo_es' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Preencha todos os campos corretamente',
        ];
    }
}
