<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BannersJoalheirosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'imagem'    => 'image',
            'titulo'    => '',
            'titulo_en' => '',
            'titulo_es' => '',
            'cor_texto' => '',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Preencha todos os campos corretamente',
            'image'    => 'Insira uma imagem válida.'
        ];
    }
}
