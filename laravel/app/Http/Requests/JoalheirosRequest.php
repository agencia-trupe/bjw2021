<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class JoalheirosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'pais_id'   => 'required',
            'nome'      => 'required',
            'texto'     => 'required',
            'texto_en'  => 'required',
            'texto_es'  => 'required',
            'whatsapp'  => '',
            'instagram' => '',
            'email'     => 'email',
            'capa'      => 'image',
            'capa_ateliers' => 'image',
        ];
    }

    public function messages()
    {
        return [
            'nome.required' => 'Insira o nome do joalheiro.',
            'pais_id.required' => 'Selecione um país.',
            'required' => 'Preencha todos os campos corretamente.',
            'email'    => 'Insira um e-mail válido.',
            'image'    => 'Insira a imagem válida.'
        ];
    }
}
