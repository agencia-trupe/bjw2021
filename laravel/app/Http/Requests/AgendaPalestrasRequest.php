<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AgendaPalestrasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'horario'      => 'required',
            'tipo'         => 'required',
            'tipo_en'      => 'required',
            'tipo_es'      => 'required',
            'titulo'       => 'required',
            'titulo_en'    => 'required',
            'titulo_es'    => 'required',
            'nome'         => 'required',
            'descricao'    => 'required',
            'descricao_en' => 'required',
            'descricao_es' => 'required',
            'nome_link'    => '',
            'nome_link_en' => '',
            'nome_link_es' => '',
            'link'         => 'required',
            'id_reuniao'   => '',
            'senha_acesso' => '',
            'texto_link'   => '',
            'texto_link_en'=> '',
            'texto_link_es'=> '',
            'imagem'       => 'image',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => 'Preencha todos os campos corretamente.',
        ];
    }
}
