<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EventoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome'                => 'required',
            'data_inicio'         => 'required',
            'data_fim'            => '',
            'periodo'             => '',
            'periodo_en'          => '',
            'periodo_es'          => '',
            'evento_texto'        => 'required',
            'evento_texto_en'     => 'required',
            'evento_texto_es'     => 'required',
            'evento_duracao'      => 'required',
            'evento_duracao_en'   => 'required',
            'evento_duracao_es'   => 'required',
            'nucleo_logo'         => 'image',
            'nucleo_logo_en'      => 'image',
            'nucleo_logo_es'      => 'image',
            'nucleo_texto'        => 'required',
            'nucleo_texto_en'     => 'required',
            'nucleo_texto_es'     => 'required',
            'realizacao_logo'     => 'image',
            'realizacao_logo_en'  => 'image',
            'realizacao_logo_es'  => 'image',
            'realizacao_nome'     => 'required',
            'realizacao_nome_en'  => 'required',
            'realizacao_nome_es'  => 'required',
            'realizacao_website'  => 'required',
            'realizacao_whatsapp' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Preencha todos os campos corretamente',
            'image'    => 'Insira uma imagem válida.'
        ];
    }
}
