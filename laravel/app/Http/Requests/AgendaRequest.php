<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AgendaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'dia_evento'    => '',
            'dia_evento_en' => '',
            'dia_evento_es' => '',
            'data'          => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Preencha todos os campos corretamente',
        ];
    }
}
