<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class JoalheirosJoaisRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome_joia'    => 'required',
            'nome_joia_en' => 'required',
            'nome_joia_es' => 'required',
            'descricao'    => 'required',
            'descricao_en' => 'required',
            'descricao_es' => 'required',
            'imagem1'      => 'image',
            'imagem2'      => 'image',
            'imagem3'      => 'image',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Preencha todos os campos corretamente',
            'image'    => 'Insira a imagem'
        ];
    }
}
