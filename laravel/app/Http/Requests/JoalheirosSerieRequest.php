<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class JoalheirosSerieRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'imagem1'   => 'image',
            'imagem2'   => 'image',
            'imagem3'   => 'image',
            'imagem4'   => 'image',
        ];
    }

    public function messages()
    {
        return [
            'image' => 'Insira a imagem'
        ];
    }
}
