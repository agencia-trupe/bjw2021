<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class GruposEscolasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'pais_id'   => 'required',
            'perfil'    => 'required',
            'nome'      => 'required',
            'texto'     => 'required',
            'texto_en'  => 'required',
            'texto_es'  => 'required',
            'whatsapp'  => '',
            'instagram' => '',
            'email'     => 'email',
            'capa'      => 'image',
        ];
    }

    public function messages()
    {
        return [
            'pais_id.required' => 'Selecione um país.',
            'perfil.required' => 'Selecione um perfil (Grupo ou Escola).',
            'required' => 'Preencha todos os campos corretamente.',
            'email'    => 'Insira um e-mail válido.',
            'image'    => 'Insira a imagem válida.'
        ];
    }
}
