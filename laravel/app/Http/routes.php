<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('encontros-e-mais', 'AgendaController@index')->name('agenda');
    Route::get('joalheiros', 'JoalheirosController@index')->name('joalheiros');
    Route::get('joalheiros/{slug}', 'JoalheirosController@show')->name('joalheiros.show');
    Route::get('escolas-e-grupos', 'EscolasGruposController@index')->name('escolas-e-grupos');
    Route::get('escolas-e-grupos/{slug}', 'EscolasGruposController@show')->name('escolas-e-grupos.show');
    Route::get('politica-de-privacidade', 'PoliticaDePrivacidadeController@index')->name('politica-de-privacidade');
    Route::get('filtro', 'FiltroController@filtrar')->name('filtro');
    Route::get('filtro/{pais}', 'FiltroController@getJoalheirosPais')->name('filtro.pais');

    // USUARIO LOGADO
    Route::get('ebook/download/{user}', 'HomeController@download')->name('ebook.download');
    Route::get('assine-livro/{user}', 'AssineLivroController@index')->name('assine-livro');
    Route::post('assine-livro/{user}', 'AssineLivroController@post')->name('assine-livro.post');
    Route::get('area-restrita/{user}', 'CadastrosFavoritasController@index')->name('favoritas');
    Route::post('favoritas/{user}/{joia}', 'CadastrosFavoritasController@favoritar')->name('favoritas.post');
    Route::delete('favoritas/{user}/{joia}', 'CadastrosFavoritasController@desfavoritar')->name('favoritas.delete');
    Route::get('palestra/zoom/{user}/{palestra}', 'AgendaController@relatorio')->name('relatorio.zoom');
    Route::get('compartilhar/whatsapp/{user}/{joia}', 'CadastrosFavoritasController@relatorioWhatsapp')->name('relatorio.whatsapp.compartilhar');
    Route::get('compartilhar/facebook/{user}/{joia}', 'CadastrosFavoritasController@relatorioFacebook')->name('relatorio.facebook.compartilhar');
    Route::get('compartilhar/email/{user}/{joia}', 'CadastrosFavoritasController@relatorioEmail')->name('relatorio.email.compartilhar');

    // Idiomas
    Route::get('lang/{idioma?}', function ($idioma = 'pt') {
        if (in_array($idioma, ['pt', 'en', 'es'])) {
            Session::put('locale', $idioma);
        }
        return redirect()->back();
    })->name('lang');

    // Login Evento
    Route::group([
        'prefix' => 'evento',
        'namespace' => 'Evento',
    ], function () {
        Route::post('login', 'Auth\AuthController@login')->name('evento.login.post');
        Route::post('cadastro', 'Auth\AuthController@create')->name('evento.register.post');
        Route::post('atualizar/{user}', 'Auth\AuthController@update')->name('evento.atualizar.user');
        Route::post('esqueci-minha-senha', 'Auth\PasswordController@sendResetLinkEmail')->name('evento.password-request.post');
        Route::get('redefinicao-de-senha/{token}', 'Auth\PasswordController@showResetForm')->name('evento.password-reset');
        Route::post('redefinicao-de-senha', 'Auth\PasswordController@reset')->name('evento.password-reset.post');
        Route::get('logout', 'Auth\AuthController@logout')->name('evento.logout');
    });

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function () {
        Route::get('/', 'PainelController@index')->name('painel');
        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::resource('grupos-escolas', 'GruposEscolasController');
        Route::resource('joalheiros', 'JoalheirosController');
        Route::resource('joalheiros.joias', 'JoalheirosJoiasController');
        Route::resource('joalheiros.processo', 'JoalheirosProcessoController');
        Route::resource('joalheiros.serie', 'JoalheirosSerieController');
        Route::resource('banners-evento', 'BannersEventoController');
        Route::resource('banners-joalheiros', 'BannersJoalheirosController');
        Route::resource('banners-revista', 'BannersRevistaController');
        Route::resource('evento', 'EventoController', ['only' => ['index', 'update']]);
        Route::resource('tipos', 'TiposController');
        Route::resource('agenda', 'AgendaController', ['except' => 'show']);
        Route::resource('agenda.palestras', 'AgendaPalestrasController');
        Route::resource('videos', 'VideosController');
        Route::resource('politica-de-privacidade', 'PoliticaDePrivacidadeController', ['only' => ['index', 'update']]);
        Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);
        Route::resource('usuarios', 'UsuariosController');

        Route::get('cadastros', 'CadastrosController@index')->name('painel.cadastros.index');
        Route::resource('assinaturas-livro', 'AssinaturasLivroController', ['only' => ['index', 'destroy']]);
        Route::get('relatorios/download-ebook', 'RelatoriosController@downloadEbook')->name('painel.relatorios.download-ebook');
        Route::get('relatorios/palestra-zoom', 'RelatoriosController@palestraZoom')->name('painel.relatorios.palestra-zoom');
        Route::get('relatorios/compartilhar-joia', 'RelatoriosController@compartilharJoia')->name('painel.relatorios.compartilhar-joia');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function () {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
