<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\TiposRequest;
use App\Models\Tipo;

class TiposController extends Controller
{
    public function index()
    {
        $registros = Tipo::orderBy('titulo', 'asc')->get();

        return view('painel.tipos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.tipos.create');
    }

    public function store(TiposRequest $request)
    {
        try {
            $input = $request->all();

            Tipo::create($input);

            return redirect()->route('painel.tipos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Tipo $registro)
    {
        return view('painel.tipos.edit', compact('registro'));
    }

    public function update(TiposRequest $request, Tipo $registro)
    {
        try {
            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.tipos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Tipo $registro)
    {
        try {
            $registro->delete();

            return redirect()->route('painel.tipos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
