<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\JoalheirosProcessoRequest;
use App\Models\Joalheiro;
use App\Models\JoalheiroProcesso;

class JoalheirosProcessoController extends Controller
{
    public function index(Joalheiro $joalheiro)
    {
        $processo = JoalheiroProcesso::joalheiro($joalheiro->id)->first();

        return view('painel.joalheiros.processo.index', compact('processo', 'joalheiro'));
    }

    public function create(Joalheiro $joalheiro)
    {
        return view('painel.joalheiros.processo.create', compact('joalheiro'));
    }

    public function store(JoalheirosProcessoRequest $request, Joalheiro $joalheiro)
    {
        try {
            $input = $request->all();
            $input['joalheiro_id'] = $joalheiro->id;

            if (isset($input['imagem1'])) $input['imagem1'] = JoalheiroProcesso::uploadImagem1();
            if (isset($input['imagem2'])) $input['imagem2'] = JoalheiroProcesso::uploadImagem2();
            if (isset($input['imagem3'])) $input['imagem3'] = JoalheiroProcesso::uploadImagem3();
            if (isset($input['imagem4'])) $input['imagem4'] = JoalheiroProcesso::uploadImagem4();

            JoalheiroProcesso::create($input);

            return redirect()->route('painel.joalheiros.processo.index', $joalheiro->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Joalheiro $joalheiro, JoalheiroProcesso $processo)
    {
        return view('painel.joalheiros.processo.edit', compact('joalheiro', 'processo'));
    }

    public function update(JoalheirosProcessoRequest $request, Joalheiro $joalheiro, JoalheiroProcesso $processo)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem1'])) $input['imagem1'] = JoalheiroProcesso::uploadImagem1();
            if (isset($input['imagem2'])) $input['imagem2'] = JoalheiroProcesso::uploadImagem2();
            if (isset($input['imagem3'])) $input['imagem3'] = JoalheiroProcesso::uploadImagem3();
            if (isset($input['imagem4'])) $input['imagem4'] = JoalheiroProcesso::uploadImagem4();

            $processo->update($input);

            return redirect()->route('painel.joalheiros.processo.index', $joalheiro->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Joalheiro $joalheiro, JoalheiroProcesso $processo)
    {
        try {
            $processo->delete();

            return redirect()->route('painel.joalheiros.processo.index', $joalheiro->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
