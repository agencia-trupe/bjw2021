<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\Assinatura;

class AssinaturasLivroController extends Controller
{
    public function index()
    {
        $assinaturas = Assinatura::join('cadastros', 'cadastros.id', '=', 'assinaturas.cadastro_id')
            ->select('assinaturas.created_at as created_at', 'assinaturas.id as id', 'cadastros.nome as nome', 'cadastros.pais as pais', 'assinaturas.mensagem as mensagem')
            ->orderBy('assinaturas.created_at', 'DESC')->get();

        return view('painel.assinaturas.index', compact('assinaturas'));
    }

    public function destroy($id)
    {
        try {
            $assinatura = Assinatura::find($id);
            $assinatura->delete();

            return redirect()->route('painel.assinaturas-livro.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
