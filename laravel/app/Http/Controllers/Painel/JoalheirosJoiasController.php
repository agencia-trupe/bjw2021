<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\JoalheirosJoaisRequest;
use App\Models\Joalheiro;
use App\Models\JoalheiroJoia;
use App\Models\Tipo;

class JoalheirosJoiasController extends Controller
{
    public function index(Joalheiro $joalheiro)
    {
        $joias = JoalheiroJoia::joalheiro($joalheiro->id)->ordenados()->get();

        return view('painel.joalheiros.joias.index', compact('joias', 'joalheiro'));
    }

    public function create(Joalheiro $joalheiro)
    {
        $tipos = Tipo::orderBy('titulo', 'asc')->pluck('titulo', 'id');

        return view('painel.joalheiros.joias.create', compact('joalheiro', 'tipos'));
    }

    public function store(JoalheirosJoaisRequest $request, Joalheiro $joalheiro)
    {
        try {
            $input = $request->all();
            $input['joalheiro_id'] = $joalheiro->id;

            if (isset($input['imagem1'])) $input['imagem1'] = JoalheiroJoia::uploadImagem1();
            if (isset($input['imagem2'])) $input['imagem2'] = JoalheiroJoia::uploadImagem2();
            if (isset($input['imagem3'])) $input['imagem3'] = JoalheiroJoia::uploadImagem3();

            JoalheiroJoia::create($input);

            return redirect()->route('painel.joalheiros.joias.index', $joalheiro->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Joalheiro $joalheiro, JoalheiroJoia $joia)
    {
        $tipos = Tipo::orderBy('titulo', 'asc')->pluck('titulo', 'id');
        // $joia = ExpositorIndividualJoia::find($joia);

        return view('painel.joalheiros.joias.edit', compact('joia', 'joalheiro', 'tipos'));
    }

    public function update(JoalheirosJoaisRequest $request, Joalheiro $joalheiro, JoalheiroJoia $joia)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem1'])) $input['imagem1'] = JoalheiroJoia::uploadImagem1();
            if (isset($input['imagem2'])) $input['imagem2'] = JoalheiroJoia::uploadImagem2();
            if (isset($input['imagem3'])) $input['imagem3'] = JoalheiroJoia::uploadImagem3();

            // $joia = ExpositorIndividualJoia::find($joia)->update($input);
            $joia->update($input);

            return redirect()->route('painel.joalheiros.joias.index', $joalheiro->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Joalheiro $joalheiro, JoalheiroJoia $joia)
    {
        try {
            // $joia = ExpositorIndividualJoia::find($joia);
            $joia->delete();

            return redirect()->route('painel.joalheiros.joias.index', $joalheiro->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
