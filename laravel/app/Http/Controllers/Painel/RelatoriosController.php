<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\RelatorioCompartilhar;
use App\Models\RelatorioDownload;
use App\Models\RelatorioZoom;

class RelatoriosController extends Controller
{
    public function downloadEbook()
    {
        $registros = RelatorioDownload::join('cadastros', 'cadastros.id', '=', 'relatorio_download.cadastro_id')
            ->select('cadastros.id as cadastro_id', 'cadastros.nome as nome', 'cadastros.email as email', 'relatorio_download.created_at as created_at', 'relatorio_download.id as id')
            ->orderBy('relatorio_download.created_at', 'DESC')
            ->get();
        
        return view('painel.relatorios.download', compact('registros'));
    }

    public function palestraZoom()
    {
        $registros = RelatorioZoom::join('cadastros', 'cadastros.id', '=', 'relatorio_zoom.cadastro_id')
            ->join('agenda_palestras', 'agenda_palestras.id', '=', 'relatorio_zoom.palestra_id')
            ->join('agenda', 'agenda.id', '=', 'agenda_palestras.agenda_id')
            ->select('cadastros.id as cadastro_id', 'cadastros.nome as nome', 'cadastros.email as email', 'relatorio_zoom.created_at as created_at', 'relatorio_zoom.id as id', 'agenda.data as palestra_data', 'agenda_palestras.titulo as palestra_titulo')
            ->orderBy('created_at', 'DESC')
            ->get();
        
        return view('painel.relatorios.zoom', compact('registros'));
    }

    public function compartilharJoia()
    {
        $registros = RelatorioCompartilhar::join('cadastros', 'cadastros.id', '=', 'relatorio_compartilhar.cadastro_id')
            ->join('joalheiros_joias', 'joalheiros_joias.id', '=', 'relatorio_compartilhar.joia_id')
            ->join('joalheiros', 'joalheiros.id', '=', 'joalheiros_joias.joalheiro_id')
            ->select('cadastros.id as cadastro_id', 'cadastros.nome as nome', 'cadastros.email as email', 'relatorio_compartilhar.created_at as created_at', 'relatorio_compartilhar.id as id', 'joalheiros_joias.nome_joia as nome_joia', 'joalheiros.nome as expositor_nome')
            ->orderBy('created_at', 'DESC')
            ->get();
        
        return view('painel.relatorios.compartilhar', compact('registros'));
    }
}
