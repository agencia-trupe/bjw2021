<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\BannersEventoRequest;
use App\Models\BannerEvento;

class BannersEventoController extends Controller
{
    public function index()
    {
        $registros = BannerEvento::ordenados()->get();
        $cores = ['#000000' => 'Preto', '#FFF' => 'Branco'];

        return view('painel.banners-evento.index', compact('registros', 'cores'));
    }

    public function create()
    {
        $cores = ['#000000' => 'Preto', '#FFF' => 'Branco'];

        return view('painel.banners-evento.create', compact('cores'));
    }

    public function store(BannersEventoRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = BannerEvento::upload_imagem();

            BannerEvento::create($input);

            return redirect()->route('painel.banners-evento.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(BannerEvento $registro)
    {
        $cores = ['#000000' => 'Preto', '#FFF' => 'Branco'];

        return view('painel.banners-evento.edit', compact('registro', 'cores'));
    }

    public function update(BannersEventoRequest $request, BannerEvento $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = BannerEvento::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.banners-evento.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(BannerEvento $registro)
    {
        try {
            $registro->delete();

            return redirect()->route('painel.banners-evento.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
