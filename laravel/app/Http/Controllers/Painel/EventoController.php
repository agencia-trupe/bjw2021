<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\EventoRequest;
use App\Models\Evento;

class EventoController extends Controller
{
    public function index()
    {
        $registro = Evento::first();

        return view('painel.evento.edit', compact('registro'));
    }

    public function update(EventoRequest $request, Evento $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['nucleo_logo'])) $input['nucleo_logo'] = Evento::upload_nucleo_logo_pt();
            if (isset($input['nucleo_logo_en'])) $input['nucleo_logo_en'] = Evento::upload_nucleo_logo_en();
            if (isset($input['nucleo_logo_es'])) $input['nucleo_logo_es'] = Evento::upload_nucleo_logo_es();

            if (isset($input['realizacao_logo'])) $input['realizacao_logo'] = Evento::upload_realizacao_logo_pt();
            if (isset($input['realizacao_logo_en'])) $input['realizacao_logo_en'] = Evento::upload_realizacao_logo_en();
            if (isset($input['realizacao_logo_es'])) $input['realizacao_logo_es'] = Evento::upload_realizacao_logo_es();

            $registro->update($input);

            return redirect()->route('painel.evento.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
