<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\AgendaRequest;
use App\Models\Agenda;

class AgendaController extends Controller
{
    public function index()
    {
        $registros = Agenda::orderBy('data', 'ASC')->get();

        return view('painel.agenda.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.agenda.create');
    }

    public function store(AgendaRequest $request)
    {
        try {
            $input = $request->all();

            Agenda::create($input);

            return redirect()->route('painel.agenda.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Agenda $registro)
    {
        return view('painel.agenda.edit', compact('registro'));
    }

    public function update(AgendaRequest $request, Agenda $registro)
    {
        try {
            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.agenda.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Agenda $registro)
    {
        try {
            $registro->delete();

            return redirect()->route('painel.agenda.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
