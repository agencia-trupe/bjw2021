<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\GruposEscolasRequest;
use App\Models\GrupoEscola;
use App\Models\Pais;

class GruposEscolasController extends Controller
{
    public function index()
    {
        $registros = GrupoEscola::join('paises', 'paises.id', '=', 'grupos_escolas.pais_id')
            ->select('paises.nome as pais_nome', 'grupos_escolas.*')
            ->orderBy('nome', 'ASC')->get();
        $perfis = ['grupos' => 'Grupos', 'escolas' => 'Escolas'];

        return view('painel.grupos-escolas.index', compact('registros', 'perfis'));
    }

    public function create()
    {
        $paises = Pais::orderBy('nome', 'asc')->pluck('nome', 'id');
        $perfis = ['grupos' => 'Grupos', 'escolas' => 'Escolas'];

        return view('painel.grupos-escolas.create', compact('paises', 'perfis'));
    }

    public function store(GruposEscolasRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = GrupoEscola::upload_capa();
            if (isset($input['imagem1'])) $input['imagem1'] = GrupoEscola::uploadImagem1();
            if (isset($input['imagem2'])) $input['imagem2'] = GrupoEscola::uploadImagem2();
            if (isset($input['imagem3'])) $input['imagem3'] = GrupoEscola::uploadImagem3();
            if (isset($input['imagem4'])) $input['imagem4'] = GrupoEscola::uploadImagem4();
            if (isset($input['imagem5'])) $input['imagem5'] = GrupoEscola::uploadImagem5();
            if (isset($input['imagem6'])) $input['imagem6'] = GrupoEscola::uploadImagem6();

            GrupoEscola::create($input);

            return redirect()->route('painel.grupos-escolas.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(GrupoEscola $registro)
    {
        $paises = Pais::orderBy('nome', 'asc')->pluck('nome', 'id');
        $perfis = ['grupos' => 'Grupos', 'escolas' => 'Escolas'];

        return view('painel.grupos-escolas.edit', compact('registro', 'paises', 'perfis'));
    }

    public function update(GruposEscolasRequest $request, GrupoEscola $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = GrupoEscola::upload_capa();
            if (isset($input['imagem1'])) $input['imagem1'] = GrupoEscola::uploadImagem1();
            if (isset($input['imagem2'])) $input['imagem2'] = GrupoEscola::uploadImagem2();
            if (isset($input['imagem3'])) $input['imagem3'] = GrupoEscola::uploadImagem3();
            if (isset($input['imagem4'])) $input['imagem4'] = GrupoEscola::uploadImagem4();
            if (isset($input['imagem5'])) $input['imagem5'] = GrupoEscola::uploadImagem5();
            if (isset($input['imagem6'])) $input['imagem6'] = GrupoEscola::uploadImagem6();

            $registro->update($input);

            return redirect()->route('painel.grupos-escolas.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(GrupoEscola $registro)
    {
        try {
            $registro->delete();

            return redirect()->route('painel.grupos-escolas.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
