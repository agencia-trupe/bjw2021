<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\AgendaPalestrasRequest;
use App\Models\Agenda;
use App\Models\AgendaPalestra;

class AgendaPalestrasController extends Controller
{
    public function index(Agenda $agenda)
    {
        $palestras = AgendaPalestra::agenda($agenda->id)->ordenados()->get();

        return view('painel.agenda.palestras.index', compact('palestras', 'agenda'));
    }

    public function create(Agenda $agenda)
    {
        return view('painel.agenda.palestras.create', compact('agenda'));
    }

    public function store(AgendaPalestrasRequest $request, Agenda $agenda)
    {
        try {
            $input = $request->all();
            $input['agenda_id'] = $agenda->id;

            if (isset($input['imagem'])) $input['imagem'] = AgendaPalestra::uploadImagem();

            AgendaPalestra::create($input);

            return redirect()->route('painel.agenda.palestras.index', $agenda->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Agenda $agenda, AgendaPalestra $palestra)
    {
        return view('painel.agenda.palestras.edit', compact('agenda', 'palestra'));
    }

    public function update(AgendaPalestrasRequest $request, Agenda $agenda, AgendaPalestra $palestra)
    {
        try {
            $input = $request->all();
            $input['agenda_id'] = $agenda->id;

            if (isset($input['imagem'])) $input['imagem'] = AgendaPalestra::uploadImagem();

            $palestra->update($input);

            return redirect()->route('painel.agenda.palestras.index', $agenda->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Agenda $agenda, AgendaPalestra $palestra)
    {
        try {
            $palestra->delete();

            return redirect()->route('painel.agenda.palestras.index', $agenda->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
