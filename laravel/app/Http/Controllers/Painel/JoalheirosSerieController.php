<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\JoalheirosSerieRequest;
use App\Models\Joalheiro;
use App\Models\JoalheiroSerie;

class JoalheirosSerieController extends Controller
{
    public function index(Joalheiro $joalheiro)
    {
        $serie = JoalheiroSerie::joalheiro($joalheiro->id)->first();

        return view('painel.joalheiros.serie.index', compact('serie', 'joalheiro'));
    }

    public function create(Joalheiro $joalheiro)
    {
        return view('painel.joalheiros.serie.create', compact('joalheiro'));
    }

    public function store(JoalheirosSerieRequest $request, Joalheiro $joalheiro)
    {
        try {
            $input = $request->all();
            $input['joalheiro_id'] = $joalheiro->id;

            if (isset($input['imagem1'])) $input['imagem1'] = JoalheiroSerie::uploadImagem1();
            if (isset($input['imagem2'])) $input['imagem2'] = JoalheiroSerie::uploadImagem2();
            if (isset($input['imagem3'])) $input['imagem3'] = JoalheiroSerie::uploadImagem3();
            if (isset($input['imagem4'])) $input['imagem4'] = JoalheiroSerie::uploadImagem4();

            JoalheiroSerie::create($input);

            return redirect()->route('painel.joalheiros.serie.index', $joalheiro->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Joalheiro $joalheiro, JoalheiroSerie $serie)
    {
        return view('painel.joalheiros.serie.edit', compact('joalheiro', 'serie'));
    }

    public function update(JoalheirosSerieRequest $request, Joalheiro $joalheiro, JoalheiroSerie $serie)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem1'])) $input['imagem1'] = JoalheiroSerie::uploadImagem1();
            if (isset($input['imagem2'])) $input['imagem2'] = JoalheiroSerie::uploadImagem2();
            if (isset($input['imagem3'])) $input['imagem3'] = JoalheiroSerie::uploadImagem3();
            if (isset($input['imagem4'])) $input['imagem4'] = JoalheiroSerie::uploadImagem4();

            $serie->update($input);

            return redirect()->route('painel.joalheiros.serie.index', $joalheiro->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Joalheiro $joalheiro, JoalheiroSerie $serie)
    {
        try {
            $serie->delete();

            return redirect()->route('painel.joalheiros.serie.index', $joalheiro->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
