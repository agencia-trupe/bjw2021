<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\BannersRevistaRequest;
use App\Models\BannerRevista;

class BannersRevistaController extends Controller
{
    public function index()
    {
        $registros = BannerRevista::ordenados()->get();
        $cores = ['#000000' => 'Preto', '#FFF' => 'Branco'];

        return view('painel.banners-revista.index', compact('registros', 'cores'));
    }

    public function create()
    {
        $cores = ['#000000' => 'Preto', '#FFF' => 'Branco'];

        return view('painel.banners-revista.create', compact('cores'));
    }

    public function store(BannersRevistaRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = BannerRevista::upload_imagem();

            BannerRevista::create($input);

            return redirect()->route('painel.banners-revista.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(BannerRevista $registro)
    {
        $cores = ['#000000' => 'Preto', '#FFF' => 'Branco'];

        return view('painel.banners-revista.edit', compact('registro', 'cores'));
    }

    public function update(BannersRevistaRequest $request, BannerRevista $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = BannerRevista::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.banners-revista.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(BannerRevista $registro)
    {
        try {
            $registro->delete();

            return redirect()->route('painel.banners-revista.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
