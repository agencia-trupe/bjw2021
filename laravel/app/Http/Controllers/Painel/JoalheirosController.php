<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\JoalheirosRequest;
use App\Models\Joalheiro;
use App\Models\Pais;

class JoalheirosController extends Controller
{
    public function index()
    {
        $registros = Joalheiro::join('paises', 'paises.id', '=', 'joalheiros.pais_id')
            ->select('paises.nome as pais_nome', 'joalheiros.*')
            ->orderBy('nome', 'ASC')->get();

        return view('painel.joalheiros.index', compact('registros'));
    }

    public function create()
    {
        $paises = Pais::orderBy('nome', 'asc')->pluck('nome', 'id');

        return view('painel.joalheiros.create', compact('paises'));
    }

    public function store(JoalheirosRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Joalheiro::upload_capa();
            if (isset($input['capa_ateliers'])) $input['capa_ateliers'] = Joalheiro::upload_capa_ateliers();

            Joalheiro::create($input);

            return redirect()->route('painel.joalheiros.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Joalheiro $registro)
    {
        $paises = Pais::orderBy('nome', 'asc')->pluck('nome', 'id');

        return view('painel.joalheiros.edit', compact('registro', 'paises'));
    }

    public function update(JoalheirosRequest $request, Joalheiro $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Joalheiro::upload_capa();
            if (isset($input['capa_ateliers'])) $input['capa_ateliers'] = Joalheiro::upload_capa_ateliers();

            $registro->update($input);

            return redirect()->route('painel.joalheiros.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Joalheiro $registro)
    {
        try {
            $registro->delete();

            return redirect()->route('painel.joalheiros.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
