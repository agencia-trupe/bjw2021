<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\VideosRequest;
use App\Models\Video;

class VideosController extends Controller
{
    public function index()
    {
        $videos = Video::ordenados()->get();

        return view('painel.videos.index', compact('videos'));
    }

    public function create()
    {
        return view('painel.videos.create');
    }

    public function store(VideosRequest $request)
    {
        try {
            $input = $request->all();

            Video::create($input);

            return redirect()->route('painel.videos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Video $video)
    {
        return view('painel.videos.edit', compact('video'));
    }

    public function update(VideosRequest $request, Video $video)
    {
        try {
            $input = $request->all();

            $video->update($input);

            return redirect()->route('painel.videos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Video $video)
    {
        try {
            $video->delete();

            return redirect()->route('painel.videos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
