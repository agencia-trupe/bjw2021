<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\BannersJoalheirosRequest;
use App\Models\BannerJoalheiros;

class BannersJoalheirosController extends Controller
{
    public function index()
    {
        $registros = BannerJoalheiros::ordenados()->get();
        $cores = ['#000000' => 'Preto', '#FFF' => 'Branco'];

        return view('painel.banners-joalheiros.index', compact('registros', 'cores'));
    }

    public function create()
    {
        $cores = ['#000000' => 'Preto', '#FFF' => 'Branco'];

        return view('painel.banners-joalheiros.create', compact('cores'));
    }

    public function store(BannersJoalheirosRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = BannerJoalheiros::upload_imagem();

            BannerJoalheiros::create($input);

            return redirect()->route('painel.banners-joalheiros.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(BannerJoalheiros $registro)
    {
        $cores = ['#000000' => 'Preto', '#FFF' => 'Branco'];

        return view('painel.banners-joalheiros.edit', compact('registro', 'cores'));
    }

    public function update(BannersJoalheirosRequest $request, BannerJoalheiros $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = BannerJoalheiros::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.banners-joalheiros.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(BannerJoalheiros $registro)
    {
        try {
            $registro->delete();

            return redirect()->route('painel.banners-joalheiros.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
