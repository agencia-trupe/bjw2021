<?php

namespace App\Http\Controllers;

use App\Models\Cadastro;
use App\Models\CadastroFavorita;
use App\Models\Evento;
use App\Models\Joalheiro;
use App\Models\JoalheiroJoia;
use App\Models\RelatorioCompartilhar;

class CadastrosFavoritasController extends Controller
{
    public function index($user)
    {
        $userLogado = auth('evento')->user($user);
        $favoritas = CadastroFavorita::where('cadastro_id', $user)->get();

        $itens = [];
        foreach ($favoritas as $favorita) {
            $itens[] = JoalheiroJoia::join('joalheiros', 'joalheiros.id', '=', 'joalheiros_joias.joalheiro_id')
                ->join('paises', 'paises.id', '=', 'joalheiros.pais_id')
                ->select('joalheiros.nome', 'paises.nome as pais_nome', 'paises.nome_en as pais_nome_en', 'paises.nome_es as pais_nome_es', 'joalheiros_joias.*')
                ->where('joalheiros_joias.id', $favorita->joia_id)->get();
        }

        return view('frontend.favoritas', compact('userLogado', 'itens'));
    }

    public function favoritar($user, $joia)
    {
        try {
            $input['cadastro_id'] = $user;
            $input['joia_id'] = $joia;

            $find = CadastroFavorita::where('cadastro_id', $user)->where('joia_id', $joia)->first();

            if (!$find) {
                CadastroFavorita::create($input);
            }

            $notification = array(
                'message' => 'Joia adicionada a sua lista de favoritas',
                'alert-type' => 'success'
            );

            return redirect()->route('favoritas', $user)->with($notification);
        } catch (\Exception $e) {

            $notification = array(
                'message' => 'Erro ao adicionar a sua lista de favoritas',
                'alert-type' => 'error'
            );

            return back()->with($notification);
        }
    }

    public function desfavoritar($user, $joia)
    {
        try {
            $registro = CadastroFavorita::where('cadastro_id', $user)->where('joia_id', $joia)->first();

            $registro->delete();

            $notification = array(
                'message' => 'Joia removida da sua lista de favoritas',
                'alert-type' => 'success'
            );

            return redirect()->route('favoritas', $user)->with($notification);
        } catch (\Exception $e) {

            $notification = array(
                'message' => 'Erro ao remover da sua lista de favoritas',
                'alert-type' => 'error'
            );

            return back()->with($notification);
        }
    }

    public function relatorioWhatsapp(Cadastro $user, JoalheiroJoia $joia)
    {
        RelatorioCompartilhar::create([
            'cadastro_id' => $user->id,
            'joia_id' => $joia->id,
        ]);

        $evento = Evento::first();
        $joalheiro = Joalheiro::where('id', $joia->joalheiro_id)->first();
        $urlJoalheiro = route('joalheiros.show', $joalheiro->slug);

        $urlWhatsapp = "https://api.whatsapp.com/send?text=" . $evento->nome . " - Link: " . $urlJoalheiro;

        return redirect($urlWhatsapp);
    }

    public function relatorioFacebook(Cadastro $user, JoalheiroJoia $joia)
    {
        RelatorioCompartilhar::create([
            'cadastro_id' => $user->id,
            'joia_id' => $joia->id,
        ]);

        $evento = Evento::first();
        $joalheiro = Joalheiro::where('id', $joia->joalheiro_id)->first();
        $urlJoalheiro = route('joalheiros.show', $joalheiro->slug);

        $urlFacebook = "https://www.facebook.com/sharer/sharer.php?u=" . $urlJoalheiro . "&t=" . $evento->nome;

        return redirect($urlFacebook);
    }

    public function relatorioEmail(Cadastro $user, JoalheiroJoia $joia)
    {
        RelatorioCompartilhar::create([
            'cadastro_id' => $user->id,
            'joia_id' => $joia->id,
        ]);

        $evento = Evento::first();
        $joalheiro = Joalheiro::where('id', $joia->joalheiro_id)->first();
        $urlJoalheiro = route('joalheiros.show', $joalheiro->slug);

        $urlEmail = "mailto:?subject=" . $evento->nome . "&body=" . $urlJoalheiro;

        return redirect($urlEmail);
    }
}
