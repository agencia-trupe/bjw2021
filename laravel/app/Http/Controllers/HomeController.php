<?php

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Models\BannerEvento;
use App\Models\BannerJoalheiros;
use App\Models\BannerRevista;
use App\Models\Cadastro;
use App\Models\Evento;
use App\Models\GrupoEscola;
use App\Models\Joalheiro;
use App\Models\JoalheiroJoia;
use App\Models\RelatorioDownload;

class HomeController extends Controller
{
    public function index()
    {
        $evento = Evento::first();

        $bannersEvento = BannerEvento::ordenados()->get();
        $tituloBannerEvento = BannerEvento::ordenados()->first();
        $bannersJoalheiros = BannerJoalheiros::ordenados()->get();
        $bannersRevista = BannerRevista::ordenados()->get();

        $joias = JoalheiroJoia::join('joalheiros', 'joalheiros.id', '=', 'joalheiros_joias.joalheiro_id')
            ->join('paises', 'paises.id', '=', 'joalheiros.pais_id')
            ->select('joalheiros.nome as nome_joalheiro', 'joalheiros.slug as slug_joalheiro', 'paises.nome as pais_nome', 'paises.nome_en as pais_nome_en', 'paises.nome_es as pais_nome_es', 'joalheiros_joias.*')
            ->inRandomOrder()->take(8)->get();

        $joalheiros = Joalheiro::join('paises', 'paises.id', '=', 'joalheiros.pais_id')
            ->select('paises.nome as pais_nome', 'paises.nome_en as pais_nome_en', 'paises.nome_es as pais_nome_es', 'joalheiros.*')
            ->inRandomOrder()->take(4)->get();

        $escolas = GrupoEscola::escolas()->join('paises', 'paises.id', '=', 'grupos_escolas.pais_id')
            ->select('paises.nome as pais_nome', 'paises.nome_en as pais_nome_en', 'paises.nome_es as pais_nome_es', 'grupos_escolas.*')
            ->inRandomOrder()->take(4)->get();

        $grupos = GrupoEscola::grupos()->join('paises', 'paises.id', '=', 'grupos_escolas.pais_id')
            ->select('paises.nome as pais_nome', 'paises.nome_en as pais_nome_en', 'paises.nome_es as pais_nome_es', 'grupos_escolas.*')
            ->inRandomOrder()->take(4)->get();

        $ateliers = Joalheiro::join('paises', 'paises.id', '=', 'joalheiros.pais_id')
            ->select('paises.nome as pais_nome', 'paises.nome_en as pais_nome_en', 'paises.nome_es as pais_nome_es', 'joalheiros.*')
            ->inRandomOrder()->take(4)->get();

        return view('frontend.home', compact('evento', 'bannersEvento', 'tituloBannerEvento', 'bannersJoalheiros', 'bannersRevista', 'joias', 'joalheiros', 'escolas', 'grupos', 'ateliers'));
    }

    public function download(Cadastro $user)
    {
        RelatorioDownload::create([
            'cadastro_id' => $user->id,
        ]);

        $ebookPath = public_path() . "/assets/ebook/" . "ebook-BJW2020.pdf";

        return response()->download($ebookPath);
    }
}
