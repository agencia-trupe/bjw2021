<?php

namespace App\Http\Controllers;

use App\Models\Agenda;
use App\Models\AgendaPalestra;
use App\Models\Cadastro;
use App\Models\JoalheiroJoia;
use App\Models\Pais;
use App\Models\RelatorioZoom;
use App\Models\Video;

class AgendaController extends Controller
{
    public function index()
    {
        $dataAtual = date('Y-m-d');
        
        $agendas = Agenda::where('data', '>=', $dataAtual)->orderBy('data', 'ASC')->get();

        $palestras = AgendaPalestra::ordenados()->get();

        $videos = Video::ordenados()->get();

        $paises = Pais::all();

        $joias = JoalheiroJoia::join('joalheiros', 'joalheiros.id', '=', 'joalheiros_joias.joalheiro_id')
            ->join('paises', 'paises.id', '=', 'joalheiros.pais_id')
            ->select('joalheiros.slug as slug_joalheiro', 'joalheiros.nome as nome_joalheiro', 'paises.nome as pais_nome', 'paises.nome_en as pais_nome_en', 'paises.nome_es as pais_nome_es', 'joalheiros_joias.*')
            ->inRandomOrder()->take(4)->get();

        return view('frontend.agenda', compact('agendas', 'palestras', 'videos', 'paises', 'joias'));
    }

    public function relatorio(Cadastro $user, AgendaPalestra $palestra)
    {
        RelatorioZoom::create([
            'cadastro_id' => $user->id,
            'palestra_id' => $palestra->id,
        ]);

        return redirect($palestra->link);
    }
}
