<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\GrupoEscola;

class EscolasGruposController extends Controller
{
    public function index()
    {
        $grupos = GrupoEscola::join('paises', 'paises.id', '=', 'grupos_escolas.pais_id')
            ->select('paises.nome as pais_nome', 'paises.nome_en as pais_nome_en', 'paises.nome_es as pais_nome_es', 'grupos_escolas.*')
            ->orderBy('nome', 'asc')->get();

        return view('frontend.escolas-e-grupos', compact('grupos'));
    }

    public function show($slug)
    {
        $grupo = GrupoEscola::join('paises', 'paises.id', '=', 'grupos_escolas.pais_id')
            ->select('paises.nome as pais_nome', 'paises.nome_en as pais_nome_en', 'paises.nome_es as pais_nome_es', 'grupos_escolas.*')
            ->where('slug', $slug)->first();

        $escolas = GrupoEscola::escolas()->join('paises', 'paises.id', '=', 'grupos_escolas.pais_id')
            ->select('paises.nome as pais_nome', 'paises.nome_en as pais_nome_en', 'paises.nome_es as pais_nome_es', 'grupos_escolas.*')
            ->inRandomOrder()->take(4)->get();

        return view('frontend.escolas-e-grupos-show', compact('grupo', 'escolas'));
    }
}
