<?php

namespace App\Http\Controllers\Evento\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CadastrosRequest;
use App\Models\Cadastro;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected function create(CadastrosRequest $request)
    {
        try {
            $input = $request->except('senha_confirmation');
            $input['senha'] = bcrypt($request->get('senha'));

            $usuario = Cadastro::create($input);

            Auth::guard('evento')->loginUsingId($usuario->id);

            $notification = array(
                'message' => 'Usuario criado e logado com sucesso!',
                'alert-type' => 'success'
            );
            return redirect()->route('home')->with($notification);
            
        } catch (\Exception $e) {

            $notification = array(
                'message' => 'Erro ao criar cadastro. Verifique se você já possui cadastro. Clique em "esqueci minha senha" para atualizá-la.',
                'alert-type' => 'error'
            );

            return back()->with($notification);
        }
    }

    protected function login(Request $request)
    {
        if (Auth::guard('evento')->attempt([
            'email'    => $request->get('email'),
            'password' => $request->get('senha')
        ])) {
            $notification = array(
                'message' => 'Usuario logado com sucesso!',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } else {
            $notification = array(
                'message' => 'Erro ao logar, e-mail ou senha inválidos',
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification);
        }
    }

    public function update(Request $request, $user)
    {
        try {
            $input = $request->all();
            $input = $request->except('senha_confirmation');
            $input['senha'] = bcrypt($request->get('senha'));

            $user = Cadastro::find($user);
            $user->update($input);

            $notification = array(
                'message' => 'Usuario atualizado com sucesso!',
                'alert-type' => 'success'
            );
            return redirect()->route('favoritas', $user->id)->with($notification);
        } catch (\Exception $e) {

            $notification = array(
                'message' => 'Erro ao atualizar cadastro',
                'alert-type' => 'error'
            );
            return redirect()->back() > with($notification);
        }
    }

    protected function logout()
    {
        auth('evento')->logout();

        $notification = array(
            'message' => 'Usuário deslogado com sucesso!',
            'alert-type' => 'success'
        );
        return redirect()->route('home')->with($notification);
    }
}
