<?php

namespace App\Http\Controllers\Evento\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

class PasswordController extends Controller
{
    use ResetsPasswords;

    protected $subject = 'Recuperação de senha';

    protected $broker          = 'cadastros';

    public function sendResetLinkEmail(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email'
        ], [
            'email.required' => 'insira um endereço de e-mail válido',
            'email.email'    => 'insira um endereço de e-mail válido',
        ]);

        $broker = $this->getBroker();

        $response = Password::broker($broker)->sendResetLink($request->only('email'), function (\Illuminate\Mail\Message $message) {
            $message->subject($this->getEmailSubject());
        });

        switch ($response) {
            case Password::RESET_LINK_SENT:
                $notification = array(
                    'message' => 'Um e-mail foi enviado com instruções para a redefinição de senha.',
                    'alert-type' => 'success'
                );
                return redirect()->back()->with($notification);

            case Password::INVALID_USER:
            default:
                $notification = array(
                    'message' => 'e-mail não encontrado',
                    'alert-type' => 'error'
                );
                return redirect()->back()->with($notification);
        }
    }

    protected function getSendResetLinkEmailSuccessResponse()
    {
        return redirect()->route('evento.login')->with([
            'senhaRedefinida' => request('email')
        ]);
    }

    public function reset(Request $request)
    {
        $this->validate($request, $this->getResetValidationRules(), [
            'password.required'  => 'insira uma senha',
            'password.min'       => 'sua senha deve ter no mínimo 6 caracteres',
            'password.confirmed' => 'a confirmação de senha não confere',
        ]);

        $credentials = $request->only(
            'email',
            'password',
            'password_confirmation',
            'token'
        );

        $broker = $this->getBroker();

        $response = Password::broker($broker)->reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                return $this->getResetSuccessResponse($response);

            default:
                return $this->getResetFailureResponse($request, $response);
        }
    }

    protected function resetPassword($user, $password)
    {
        $user->senha = bcrypt($password);

        $user->save();

        Auth::guard($this->getGuard())->login($user);
    }

    protected function getResetSuccessResponse($response)
    {

        $notification = array(
            'message' => 'Senha redefinida com sucesso',
            'alert-type' => 'success'
        );
        return redirect()->route('home')->with($notification);
    }
}
