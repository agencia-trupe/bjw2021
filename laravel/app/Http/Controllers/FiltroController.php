<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\ExpositorColetivo;
use App\Models\ExpositorIndividual;
use App\Models\GrupoEscola;
use App\Models\Joalheiro;
use App\Models\JoalheiroJoia;
use App\Models\Pais;
use App\Models\Tipo;

class FiltroController extends Controller
{
    public function filtrar(Request $request)
    {
        if ($request->has('pais') && !$request->has('tipo') && !$request->has('palavraChave')) {
            $pais = Pais::where('id', $request->pais)->first();
            $joalheiros = Joalheiro::where('joalheiros.pais_id', $pais->id)
                ->select('joalheiros.id', 'joalheiros.slug', 'joalheiros.nome', 'joalheiros.capa')
                ->orderBy('nome', 'asc')->get();
            $grupos = GrupoEscola::where('grupos_escolas.pais_id', $pais->id)
                ->select('grupos_escolas.id', 'grupos_escolas.slug', 'grupos_escolas.nome', 'grupos_escolas.capa')
                ->orderBy('nome', 'asc')->get();

            return view('frontend.resultados-pais', compact('pais', 'joalheiros', 'grupos'));
        } else {
            $joias = JoalheiroJoia::join('joalheiros', 'joalheiros.id', '=', 'joalheiros_joias.joalheiro_id')
                ->join('tipos', 'tipos.id', '=', 'joalheiros_joias.tipo_id')
                ->join('paises', 'paises.id', '=', 'joalheiros.pais_id')
                ->select('paises.nome as pais_nome', 'paises.nome_en as pais_nome_en', 'paises.nome_es as pais_nome_es', 'tipos.titulo', 'tipos.titulo_en', 'tipos.titulo_es', 'joalheiros.nome', 'joalheiros_joias.*')
                ->orderBy('joalheiros_joias.id', 'asc');

            if ($request->has('pais')) {
                $joias = $joias->where('joalheiros.pais_id', $request->pais);
                $pais = Pais::where('id', $request->pais)->first();
            }

            if ($request->has('tipo')) {
                $joias = $joias->where('joalheiros_joias.tipo_id', $request->tipo);
                $tipo = Tipo::where('id', $request->tipo)->first();
            }

            if ($request->has('palavraChave')) {
                $palavra = $request->palavraChave;

                $joias = $joias->where(function ($query) use ($palavra) {
                    $query->where('joalheiros_joias.nome_joia', 'LIKE', "%" . $palavra . "%")
                        ->orWhere('joalheiros_joias.nome_joia_en', 'LIKE', "%" . $palavra . "%")
                        ->orWhere('joalheiros_joias.nome_joia_es', 'LIKE', "%" . $palavra . "%")
                        ->orWhere('joalheiros.nome', 'LIKE', "%" . $palavra . "%");
                });
            }

            $joias = $joias->get();
            // dd($joias);

            return view('frontend.resultados-filtro', compact('joias', 'palavra', 'pais', 'tipo'));
        }
    }

    public function getJoalheirosPais($pais)
    {
        $joalheiros = Joalheiro::where('pais_id', $pais)->orderBy('nome', 'asc')->get();
        $escolas = GrupoEscola::escolas()->where('pais_id', $pais)->orderBy('nome', 'asc')->get();
        $grupos = GrupoEscola::grupos()->where('pais_id', $pais)->orderBy('nome', 'asc')->get();
        $ateliers = Joalheiro::where('pais_id', $pais)->orderBy('nome', 'asc')->get();

        count($joalheiros) > 0 ? $joalheiros = $joalheiros : $joalheiros = null;
        count($escolas) > 0 ? $escolas = $escolas : $escolas = null;
        count($grupos) > 0 ? $grupos = $grupos : $grupos = null;
        count($ateliers) > 0 ? $ateliers = $ateliers : $ateliers = null;

        return response()->json(['joalheiros' => $joalheiros, 'escolas' => $escolas, 'grupos' => $grupos, 'ateliers' => $ateliers]);
    }
}
