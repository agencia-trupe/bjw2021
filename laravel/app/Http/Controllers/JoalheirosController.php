<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Joalheiro;
use App\Models\JoalheiroJoia;
use App\Models\JoalheiroProcesso;
use App\Models\JoalheiroSerie;

class JoalheirosController extends Controller
{
    public function index()
    {
        $joalheiros = Joalheiro::join('paises', 'paises.id', '=', 'joalheiros.pais_id')
            ->select('paises.nome as pais_nome', 'paises.nome_en as pais_nome_en', 'paises.nome_es as pais_nome_es', 'joalheiros.*')
            ->orderBy('nome', 'asc')->get();

        return view('frontend.joalheiros', compact('joalheiros'));
    }

    public function show($slug)
    {
        $joalheiro = Joalheiro::join('paises', 'paises.id', '=', 'joalheiros.pais_id')
            ->select('paises.nome as pais_nome', 'paises.nome_en as pais_nome_en', 'paises.nome_es as pais_nome_es', 'joalheiros.*')
            ->where('slug', $slug)->first();

        $joias = JoalheiroJoia::join('tipos', 'tipos.id', '=', 'joalheiros_joias.tipo_id')
            ->select('tipos.titulo as tipo', 'tipos.titulo_en as tipo_en', 'tipos.titulo_es as tipo_es', 'joalheiros_joias.*')
            ->where('joalheiros_joias.joalheiro_id', $joalheiro->id)
            ->ordenados()->get();

        $processo = JoalheiroProcesso::where('joalheiro_id', $joalheiro->id)->first();

        $serie = JoalheiroSerie::where('joalheiro_id', $joalheiro->id)->first();

        $joalheiros = Joalheiro::join('paises', 'paises.id', '=', 'joalheiros.pais_id')
            ->select('paises.nome as pais_nome', 'paises.nome_en as pais_nome_en', 'paises.nome_es as pais_nome_es', 'joalheiros.*')
            ->inRandomOrder()->take(4)->get();

        // $access_token = "My_Access_Token";
        // $photo_count = 5;
        // $json_link = "https://api.instagram.com/v1/users/self/media/recent/?";
        // $json_link .= "access_token={$access_token}&count={$photo_count}";
        // $json = file_get_contents($json_link);
        // $obj = json_decode(preg_replace('/("\w+"):(\d+)/', '\\1:"\\2"', $json), true);

        // dd($obj);

        return view('frontend.joalheiros-show', compact('joalheiro', 'joias', 'processo', 'serie', 'joalheiros'));
    }
}
