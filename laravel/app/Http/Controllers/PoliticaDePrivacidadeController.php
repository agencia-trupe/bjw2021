<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\PoliticaDePrivacidade;

class PoliticaDePrivacidadeController extends Controller
{
    public function index()
    {
        $politica = PoliticaDePrivacidade::first();

        return view('frontend.politica', compact('politica'));
    }
}
