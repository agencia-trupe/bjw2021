<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class BannerJoalheiros extends Model
{
    protected $table = 'banners_joalheiros';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 1200,
            'height' => 465,
            'path'   => 'assets/img/banners/'
        ]);
    }
}
