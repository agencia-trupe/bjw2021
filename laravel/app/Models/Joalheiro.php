<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Joalheiro extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'nome',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'joalheiros';

    protected $guarded = ['id'];

    public function joias()
    {
        return $this->hasMany('App\Models\JoalheiroJoia', 'joalheiro_id')->ordenados();
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            [
                'width'  => 600,
                'height' => null,
                'path'   => 'assets/img/joalheiros/'
            ],
            [
                'width'  => 280,
                'height' => 280,
                'path'   => 'assets/img/joalheiros/thumbs/'
            ]
        ]);
    }

    public static function upload_capa_ateliers()
    {
        return CropImage::make('capa_ateliers', [
            'width'  => 280,
            'height' => 280,
            'path'   => 'assets/img/joalheiros/ateliers/'
        ]);
    }
}
