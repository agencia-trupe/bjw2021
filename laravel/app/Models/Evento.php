<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    protected $table = 'evento';

    protected $guarded = ['id'];

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = \Carbon\Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    }

    public function getCreatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }

    public function getCreatedAtOrderAttribute()
    {
        return \Carbon\Carbon::createFromFormat('d/m/Y', $this->created_at)->format('Y-m-d');
    }

    public static function upload_nucleo_logo_pt()
    {
        return CropImage::make('nucleo_logo', [
            'width'  => null,
            'height' => null,
            'transparent' => true,
            'path'   => 'assets/img/evento/'
        ]);
    }

    public static function upload_nucleo_logo_en()
    {
        return CropImage::make('nucleo_logo_en', [
            'width'  => null,
            'height' => null,
            'transparent' => true,
            'path'   => 'assets/img/evento/'
        ]);
    }

    public static function upload_nucleo_logo_es()
    {
        return CropImage::make('nucleo_logo_es', [
            'width'  => null,
            'height' => null,
            'transparent' => true,
            'path'   => 'assets/img/evento/'
        ]);
    }

    public static function upload_realizacao_logo_pt()
    {
        return CropImage::make('realizacao_logo', [
            'width'  => null,
            'height' => null,
            'transparent' => true,
            'path'   => 'assets/img/evento/'
        ]);
    }

    public static function upload_realizacao_logo_en()
    {
        return CropImage::make('realizacao_logo_en', [
            'width'  => null,
            'height' => null,
            'transparent' => true,
            'path'   => 'assets/img/evento/'
        ]);
    }

    public static function upload_realizacao_logo_es()
    {
        return CropImage::make('realizacao_logo_es', [
            'width'  => null,
            'height' => null,
            'transparent' => true,
            'path'   => 'assets/img/evento/'
        ]);
    }
}
