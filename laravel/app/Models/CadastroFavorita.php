<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CadastroFavorita extends Model
{
    protected $table = 'cadastros_favoritas';

    protected $guarded = ['id'];

    public function cadastros()
    {
        return $this->hasMany('App\Models\Cadastro', 'cadastro_id');
    }

    public function joias()
    {
        return $this->hasMany('App\Models\JoalheiroJoia', 'joia_id');
    }
}
