<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class GrupoEscola extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'nome',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'grupos_escolas';

    protected $guarded = ['id'];

    public function scopeEscolas($query)
    {
        return $query->where('perfil', '=', 'escolas');
    }

    public function scopeGrupos($query)
    {
        return $query->where('perfil', '=', 'grupos');
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            [
                'width'  => 1200,
                'height' => 500,
                'path'   => 'assets/img/grupos-escolas/'
            ],
            [
                'width'  => 280,
                'height' => 280,
                'path'   => 'assets/img/grupos-escolas/thumbs/'
            ]
        ]);
    }

    public static function uploadImagem1()
    {
        return CropImage::make('imagem1', [
            [
                'width'  => null,
                'height' => null,
                'path'   => 'assets/img/grupos-escolas/imagens/'
            ],
            [
                'width'  => 390,
                'height' => 390,
                'path'   => 'assets/img/grupos-escolas/imagens/thumbs/'
            ]
        ]);
    }

    public static function uploadImagem2()
    {
        return CropImage::make('imagem2', [
            [
                'width'  => null,
                'height' => null,
                'path'   => 'assets/img/grupos-escolas/imagens/'
            ],
            [
                'width'  => 390,
                'height' => 390,
                'path'   => 'assets/img/grupos-escolas/imagens/thumbs/'
            ]
        ]);
    }

    public static function uploadImagem3()
    {
        return CropImage::make('imagem3', [
            [
                'width'  => null,
                'height' => null,
                'path'   => 'assets/img/grupos-escolas/imagens/'
            ],
            [
                'width'  => 390,
                'height' => 390,
                'path'   => 'assets/img/grupos-escolas/imagens/thumbs/'
            ]
        ]);
    }

    public static function uploadImagem4()
    {
        return CropImage::make('imagem4', [
            [
                'width'  => null,
                'height' => null,
                'path'   => 'assets/img/grupos-escolas/imagens/'
            ],
            [
                'width'  => 390,
                'height' => 390,
                'path'   => 'assets/img/grupos-escolas/imagens/thumbs/'
            ]
        ]);
    }

    public static function uploadImagem5()
    {
        return CropImage::make('imagem5', [
            [
                'width'  => null,
                'height' => null,
                'path'   => 'assets/img/grupos-escolas/imagens/'
            ],
            [
                'width'  => 390,
                'height' => 390,
                'path'   => 'assets/img/grupos-escolas/imagens/thumbs/'
            ]
        ]);
    }

    public static function uploadImagem6()
    {
        return CropImage::make('imagem6', [
            [
                'width'  => null,
                'height' => null,
                'path'   => 'assets/img/grupos-escolas/imagens/'
            ],
            [
                'width'  => 390,
                'height' => 390,
                'path'   => 'assets/img/grupos-escolas/imagens/thumbs/'
            ]
        ]);
    }
}
