<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    protected $table = 'users';

    protected $fillable = ['name', 'email', 'password', 'tipo'];

    protected $hidden = ['password', 'remember_token'];

    public static function tipos($tipo = null)
    {
        $tipos = [
            'admin' => 'Administração',
            'evento' => 'Evento'
        ];

        if ($tipo !== null) {
            return array_key_exists($tipo, $tipos) ? $tipos[$tipo] : '-';
        }

        return $tipos;
    }
}
