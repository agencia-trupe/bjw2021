<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class JoalheiroJoia extends Model
{
    protected $table = 'joalheiros_joias';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeTipo($query, $id)
    {
        return $query->where('tipo_id', $id);
    }

    public function scopeJoalheiro($query, $id)
    {
        return $query->where('joalheiro_id', $id);
    }

    public function scopeConsulta($query)
    {
        return $query->where('preco_sob_consulta', '=', 1);
    }

    // public function getValorFormatadoAttribute()
    // {
    //     return 'US$ ' . number_format($this->valor, 2, '.', ',');
    // }

    public static function uploadImagem1()
    {
        return CropImage::make('imagem1', [
            [
                'width'  => 550,
                'height' => null,
                'path'   => 'assets/img/joalheiros/joias/'
            ],
            [
                'width'  => 90,
                'height' => 90,
                'path'   => 'assets/img/joalheiros/joias/thumbs/'
            ]
        ]);
    }

    public static function uploadImagem2()
    {
        return CropImage::make('imagem2', [
            [
                'width'  => 550,
                'height' => null,
                'path'   => 'assets/img/joalheiros/joias/'
            ],
            [
                'width'  => 90,
                'height' => 90,
                'path'   => 'assets/img/joalheiros/joias/thumbs/'
            ]
        ]);
    }

    public static function uploadImagem3()
    {
        return CropImage::make('imagem3', [
            [
                'width'  => 550,
                'height' => null,
                'path'   => 'assets/img/joalheiros/joias/'
            ],
            [
                'width'  => 90,
                'height' => 90,
                'path'   => 'assets/img/joalheiros/joias/thumbs/'
            ]
        ]);
    }
}
