<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class JoalheiroSerie extends Model
{
    protected $table = 'joalheiros_serie';

    protected $guarded = ['id'];

    public function scopeJoalheiro($query, $id)
    {
        return $query->where('joalheiro_id', $id);
    }

    public static function uploadImagem1()
    {
        return CropImage::make('imagem1', [
            [
                'width'  => null,
                'height' => null,
                'path'   => 'assets/img/joalheiros/serie/'
            ],
            [
                'width'  => 230,
                'height' => 200,
                'path'   => 'assets/img/joalheiros/serie/thumbs/'
            ]
        ]);
    }

    public static function uploadImagem2()
    {
        return CropImage::make('imagem2', [
            [
                'width'  => null,
                'height' => null,
                'path'   => 'assets/img/joalheiros/serie/'
            ],
            [
                'width'  => 230,
                'height' => 200,
                'path'   => 'assets/img/joalheiros/serie/thumbs/'
            ]
        ]);
    }

    public static function uploadImagem3()
    {
        return CropImage::make('imagem3', [
            [
                'width'  => null,
                'height' => null,
                'path'   => 'assets/img/joalheiros/serie/'
            ],
            [
                'width'  => 230,
                'height' => 200,
                'path'   => 'assets/img/joalheiros/serie/thumbs/'
            ]
        ]);
    }

    public static function uploadImagem4()
    {
        return CropImage::make('imagem4', [
            [
                'width'  => null,
                'height' => null,
                'path'   => 'assets/img/joalheiros/serie/'
            ],
            [
                'width'  => 230,
                'height' => 200,
                'path'   => 'assets/img/joalheiros/serie/thumbs/'
            ]
        ]);
    }
}
