<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class AgendaPalestra extends Model
{
    protected $table = 'agenda_palestras';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeAgenda($query, $id)
    {
        return $query->where('agenda_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            'width'  => 260,
            'height' => 260,
            'path'   => 'assets/img/agenda/'
        ]);
    }
}
