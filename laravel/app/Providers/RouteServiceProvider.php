<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'App\Http\Controllers';

    public function boot(Router $router)
    {
        $router->model('grupos_escolas', 'App\Models\GrupoEscola');
        $router->model('serie', 'App\Models\JoalheiroSerie');
        $router->model('processo', 'App\Models\JoalheiroProcesso');
        $router->model('joias', 'App\Models\JoalheiroJoia');
        $router->model('joalheiros', 'App\Models\Joalheiro');
        $router->model('banners_revista', 'App\Models\BannerRevista');
        $router->model('banners_joalheiros', 'App\Models\BannerJoalheiros');
        $router->model('banners_evento', 'App\Models\BannerEvento');
        $router->model('agenda', 'App\Models\Agenda');
        $router->model('palestras', 'App\Models\AgendaPalestra');
        $router->model('videos', 'App\Models\Video');
        $router->model('evento', 'App\Models\Evento');
        $router->model('tipos', 'App\Models\Tipo');
        $router->model('paises', 'App\Models\Pais');
        $router->model('cadastros', 'App\Models\Cadastro');
        $router->model('politica_de_privacidade', 'App\Models\PoliticaDePrivacidade');
        $router->model('configuracoes', 'App\Models\Configuracao');
        $router->model('assinaturas', 'App\Models\Assinatura');
        $router->model('cadastros_favoritas', 'App\Models\CadastroFavorita');
        $router->model('usuarios', 'App\Models\User');

        parent::boot($router);
    }

    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
