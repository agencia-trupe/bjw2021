<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateEventoTable extends Migration
{
    public function up()
    {
        Schema::create('evento', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->date('data_inicio');
            $table->date('data_fim');
            $table->string('periodo');
            $table->string('periodo_en');
            $table->string('periodo_es');
            $table->text('evento_texto');
            $table->text('evento_texto_en');
            $table->text('evento_texto_es');
            $table->string('evento_duracao');
            $table->string('evento_duracao_en');
            $table->string('evento_duracao_es');
            $table->string('nucleo_logo');
            $table->string('nucleo_logo_en');
            $table->string('nucleo_logo_es');
            $table->text('nucleo_texto');
            $table->text('nucleo_texto_en');
            $table->text('nucleo_texto_es');
            $table->string('realizacao_logo');
            $table->string('realizacao_logo_en');
            $table->string('realizacao_logo_es');
            $table->string('realizacao_nome');
            $table->string('realizacao_nome_en');
            $table->string('realizacao_nome_es');
            $table->string('realizacao_website');
            $table->string('realizacao_whatsapp');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('evento');
    }
}
