<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateBannersRevistaTable extends Migration
{
    public function up()
    {
        Schema::create('banners_revista', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('titulo');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('cor_texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('banners_revista');
    }
}
