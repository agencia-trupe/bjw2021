<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateAgendaTable extends Migration
{
    public function up()
    {
        Schema::create('agenda', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dia_evento');
            $table->string('dia_evento_en');
            $table->string('dia_evento_es');
            $table->date('data');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('agenda');
    }
}
