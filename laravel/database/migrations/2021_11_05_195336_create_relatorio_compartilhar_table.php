<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateRelatorioCompartilharTable extends Migration
{
    public function up()
    {
        Schema::create('relatorio_compartilhar', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cadastro_id')->unsigned();
            $table->foreign('cadastro_id')->references('id')->on('cadastros')->onDelete('cascade');
            $table->integer('joia_id')->unsigned();
            $table->foreign('joia_id')->references('id')->on('joalheiros_joias')->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('relatorio_compartilhar');
    }
}
