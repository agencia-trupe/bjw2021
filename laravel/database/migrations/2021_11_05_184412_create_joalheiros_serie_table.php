<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateJoalheirosSerieTable extends Migration
{
    public function up()
    {
        Schema::create('joalheiros_serie', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('joalheiro_id')->unsigned();
            $table->foreign('joalheiro_id')->references('id')->on('joalheiros')->onDelete('cascade');
            $table->string('video')->nullable();
            $table->string('imagem1')->nullable();
            $table->string('imagem2')->nullable();
            $table->string('imagem3')->nullable();
            $table->string('imagem4')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('joalheiros_serie');
    }
}
