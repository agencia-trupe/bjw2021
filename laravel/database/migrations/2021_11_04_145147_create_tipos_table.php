<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateTiposTable extends Migration
{
    public function up()
    {
        Schema::create('tipos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('titulo');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('tipos');
    }
}
