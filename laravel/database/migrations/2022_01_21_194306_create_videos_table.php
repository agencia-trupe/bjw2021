<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateVideosTable extends Migration
{
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('video');
            $table->string('titulo');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('videos');
    }
}
