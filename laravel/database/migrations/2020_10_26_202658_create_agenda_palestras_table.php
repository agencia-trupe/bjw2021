<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateAgendaPalestrasTable extends Migration
{
    public function up()
    {
        Schema::create('agenda_palestras', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agenda_id')->unsigned();
            $table->foreign('agenda_id')->references('id')->on('agenda')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->string('horario');
            $table->string('tipo');
            $table->string('tipo_en');
            $table->string('tipo_es');
            $table->string('titulo');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('nome');
            $table->text('descricao');
            $table->text('descricao_en');
            $table->text('descricao_es');
            $table->string('nome_link');
            $table->string('nome_link_en');
            $table->string('nome_link_es');
            $table->string('link');
            $table->string('id_reuniao');
            $table->string('senha_acesso');
            $table->text('texto_link');
            $table->text('texto_link_en');
            $table->text('texto_link_es');
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('agenda_palestras');
    }
}
