<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateJoalheirosJoiasTable extends Migration
{
    public function up()
    {
        Schema::create('joalheiros_joias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->integer('tipo_id')->unsigned();
            $table->foreign('tipo_id')->references('id')->on('tipos')->onDelete('cascade');
            $table->integer('joalheiro_id')->unsigned();
            $table->foreign('joalheiro_id')->references('id')->on('joalheiros')->onDelete('cascade');
            $table->string('nome_joia');
            $table->string('nome_joia_en');
            $table->string('nome_joia_es');
            $table->string('valor')->nullable();
            $table->boolean('preco_sob_consulta')->default(false);
            $table->text('descricao');
            $table->text('descricao_en');
            $table->text('descricao_es');
            $table->string('imagem1')->nullable();
            $table->string('imagem2')->nullable();
            $table->string('imagem3')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('joalheiros_joias');
    }
}
