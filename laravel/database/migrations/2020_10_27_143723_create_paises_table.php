<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreatePaisesTable extends Migration
{
    public function up()
    {
        Schema::create('paises', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('nome_en');
            $table->string('nome_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('paises');
    }
}
