<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateGruposEscolasTable extends Migration
{
    public function up()
    {
        Schema::create('grupos_escolas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pais_id')->unsigned();
            $table->foreign('pais_id')->references('id')->on('paises')->onDelete('cascade');
            $table->string('perfil'); // grupo ou escola
            $table->string('slug');
            $table->string('nome');
            $table->text('texto');
            $table->text('texto_en');
            $table->text('texto_es');
            $table->string('whatsapp')->nullable();
            $table->string('instagram')->nullable();
            $table->string('email')->nullable();
            $table->string('website')->nullable();
            $table->string('capa');
            $table->string('video')->nullable();
            $table->string('imagem1')->nullable();
            $table->string('imagem2')->nullable();
            $table->string('imagem3')->nullable();
            $table->string('imagem4')->nullable();
            $table->string('imagem5')->nullable();
            $table->string('imagem6')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('grupos_escolas');
    }
}
