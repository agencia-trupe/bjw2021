<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateRelatorioZoomTable extends Migration
{
    public function up()
    {
        Schema::create('relatorio_zoom', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cadastro_id')->unsigned();
            $table->foreign('cadastro_id')->references('id')->on('cadastros')->onDelete('cascade');
            $table->integer('palestra_id')->unsigned();
            $table->foreign('palestra_id')->references('id')->on('agenda_palestras')->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('relatorio_zoom');
    }
}
