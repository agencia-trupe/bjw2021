<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateJoalheirosTable extends Migration
{
    public function up()
    {
        Schema::create('joalheiros', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pais_id')->unsigned();
            $table->foreign('pais_id')->references('id')->on('paises')->onDelete('cascade');
            $table->string('slug');
            $table->string('nome');
            $table->text('texto');
            $table->text('texto_en');
            $table->text('texto_es');
            $table->string('whatsapp')->nullable();
            $table->string('instagram')->nullable();
            $table->string('email')->nullable();
            $table->string('website')->nullable();
            $table->string('capa');
            $table->string('capa_ateliers');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('joalheiros');
    }
}
