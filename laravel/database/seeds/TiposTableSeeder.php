<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TiposTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('tipos')->insert([
            'id'        => 1,
            'slug'      => 'anel',
            'titulo'    => 'Anel',
            'titulo_en' => 'Ring',
            'titulo_es' => 'Anillo',
        ]);

        DB::table('tipos')->insert([
            'id'        => 2,
            'slug'      => 'bracelete',
            'titulo'    => 'Bracelete',
            'titulo_en' => 'Bracelet',
            'titulo_es' => 'Pulsera',
        ]);

        DB::table('tipos')->insert([
            'id'        => 3,
            'slug'      => 'brinco',
            'titulo'    => 'Brinco',
            'titulo_en' => 'Earring',
            'titulo_es' => 'Arete',
        ]);

        DB::table('tipos')->insert([
            'id'        => 4,
            'slug'      => 'broche',
            'titulo'    => 'Broche',
            'titulo_en' => 'Brooch',
            'titulo_es' => 'Broche',
        ]);

        DB::table('tipos')->insert([
            'id'        => 5,
            'slug'      => 'colar',
            'titulo'    => 'Colar',
            'titulo_en' => 'Necklace',
            'titulo_es' => 'Collar',
        ]);

        DB::table('tipos')->insert([
            'id'        => 6,
            'slug'      => 'conjunto-de-aneis',
            'titulo'    => 'Conjunto de anéis',
            'titulo_en' => 'Ring set',
            'titulo_es' => 'Juego de anillos',
        ]);

        DB::table('tipos')->insert([
            'id'        => 7,
            'slug'      => 'joia-de-corpo',
            'titulo'    => 'Joia de corpo',
            'titulo_en' => 'Body jewelry',
            'titulo_es' => 'Joyería corporal',
        ]);

        DB::table('tipos')->insert([
            'id'        => 8,
            'slug'      => 'mascara',
            'titulo'    => 'Máscara',
            'titulo_en' => 'Mask',
            'titulo_es' => 'Máscara',
        ]);

        DB::table('tipos')->insert([
            'id'        => 9,
            'slug'      => 'peitoral',
            'titulo'    => 'Peitoral',
            'titulo_en' => 'Breastplate',
            'titulo_es' => 'Coraza',
        ]);

        DB::table('tipos')->insert([
            'id'        => 10,
            'slug'      => 'pingente',
            'titulo'    => 'Pingente',
            'titulo_en' => 'Pendant',
            'titulo_es' => 'Colgante',
        ]);

        DB::table('tipos')->insert([
            'id'        => 11,
            'slug'      => 'pulseira',
            'titulo'    => 'Pulseira',
            'titulo_en' => 'Bracelet',
            'titulo_es' => 'Pulsera',
        ]);

        DB::table('tipos')->insert([
            'id'        => 12,
            'slug'      => 'outros',
            'titulo'    => 'Outros',
            'titulo_en' => 'Others',
            'titulo_es' => 'Otros',
        ]);
    }
}
