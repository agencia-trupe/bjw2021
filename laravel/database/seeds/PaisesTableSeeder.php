<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaisesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('paises')->insert([
            'id'      => 1,
            'nome' => 'Argentina',
            'nome_en' => 'Argentina',
            'nome_es' => 'Argentina',
        ]);

        DB::table('paises')->insert([
            'id'      => 2,
            'nome' => 'Brasil',
            'nome_en' => 'Brazil',
            'nome_es' => 'Brasil',
        ]);

        DB::table('paises')->insert([
            'id'      => 3,
            'nome' => 'Chile',
            'nome_en' => 'Chile',
            'nome_es' => 'Chile',
        ]);

        DB::table('paises')->insert([
            'id'      => 4,
            'nome' => 'Colômbia',
            'nome_en' => 'Colombia',
            'nome_es' => 'Colombia',
        ]);

        DB::table('paises')->insert([
            'id'      => 5,
            'nome' => 'Coréia do Sul',
            'nome_en' => 'South Korea',
            'nome_es' => 'Corea del Sur',
        ]);

        DB::table('paises')->insert([
            'id'      => 6,
            'nome' => 'Costa Rica ',
            'nome_en' => 'Costa Rica',
            'nome_es' => 'Costa Rica',
        ]);

        DB::table('paises')->insert([
            'id'      => 7,
            'nome' => 'Espanha',
            'nome_en' => 'Spain',
            'nome_es' => 'España',
        ]);

        DB::table('paises')->insert([
            'id'      => 8,
            'nome' => 'México',
            'nome_en' => 'Mexico',
            'nome_es' => 'México',
        ]);

        DB::table('paises')->insert([
            'id'      => 9,
            'nome' => 'Portugal',
            'nome_en' => 'Portugal',
            'nome_es' => 'Portugal',
        ]);

        DB::table('paises')->insert([
            'id'      => 10,
            'nome' => 'Moçambique',
            'nome_en' => 'Mozambique',
            'nome_es' => 'Mozambique',
        ]);

        DB::table('paises')->insert([
            'id'      => 11,
            'nome' => 'Outros',
            'nome_en' => 'Others',
            'nome_es' => 'Otros',
        ]);

    }
}
