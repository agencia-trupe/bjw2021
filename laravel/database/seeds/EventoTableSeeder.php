<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventoTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('evento')->insert([
            'nome'                => 'BRAZIL JEWELRY WEEK',
            'data_inicio'         => Carbon::parse('2021-11-18 00:00')->format('Y-m-d h:m'),
            'data_fim'            => Carbon::parse('2021-11-28 23:59')->format('Y-m-d h:m'),
            'periodo'             => '18 a 28 de novembro de 2021',
            'periodo_en'          => 'November 18th to 28th, 2021',
            'periodo_es'          => '18 al 28 de noviembre de 2021',
            'evento_texto'        => '',
            'evento_texto_en'     => '',
            'evento_texto_es'     => '',
            'evento_duracao'      => '20 dias, 24 horas: galerias de arte e design do Brasil e do mundo apresentam projetos especialmente desenvolvidos para o BRAZIL JEWELRY WEEK. Boa navegação!',
            'evento_duracao_en'   => '20 days, 24 hours: art and design galleries from Brazil and around the world present projects specially developed for BRAZIL JEWELRY WEEK. Happy navigation!',
            'evento_duracao_es'   => '20 días, 24 horas: galerías de arte y diseño de Brasil y de todo el mundo presentan proyectos especialmente desarrollados para BRAZIL JEWELRY WEEK. ¡Feliz navegación!',
            'nucleo_logo'         => '',
            'nucleo_logo_en'      => '',
            'nucleo_logo_es'      => '',
            'nucleo_texto'        => '',
            'nucleo_texto_en'     => '',
            'nucleo_texto_es'     => '',
            'realizacao_logo'     => '',
            'realizacao_logo_en'  => '',
            'realizacao_logo_es'  => '',
            'realizacao_nome'     => 'Núcleo Joalheria Contemporânea Latina',
            'realizacao_nome_en'  => '',
            'realizacao_nome_es'  => '',
            'realizacao_website'  => 'https://www.nucleojoalheria.org/',
            'realizacao_whatsapp' => '+55 11 98765 4321',
        ]);
    }
}
