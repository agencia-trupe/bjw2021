@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Downloads de E-book
    </h2>
</legend>


@if(!count($registros))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="relatorio_download">
    <thead>
        <tr>
            <th>Data</th>
            <th>Nome</th>
            <th>E-mail</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($registros as $registro)
        <tr class="tr-row" id="{{ $registro->id }}">
            <td>{{ strftime("%d/%m/%Y", strtotime($registro->created_at)) }}</td>
            <td>{{ $registro->nome }}</td>
            <td>
                <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $registro->email }}" style="margin-right:5px;border:0;transition:background .3s">
                    <span class="glyphicon glyphicon-copy"></span>
                </button>
                {{ $registro->email }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection