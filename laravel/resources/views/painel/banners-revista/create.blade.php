@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Banners Revista /</small> Adicionar Banner</h2>
    </legend>

    {!! Form::open(['route' => 'painel.banners-revista.store', 'files' => true]) !!}

        @include('painel.banners-revista.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
