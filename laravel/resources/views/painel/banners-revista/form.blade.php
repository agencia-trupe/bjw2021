@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/banners/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo', 'Título [PT]') !!}
            {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo_en', 'Título [EN]') !!}
            {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo_es', 'Título [ES]') !!}
            {!! Form::text('titulo_es', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('cor_texto', 'Cor do Texto') !!}
    {!! Form::select('cor_texto', $cores , old('cor_texto'), ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.banners-revista.index') }}" class="btn btn-default btn-voltar">Voltar</a>