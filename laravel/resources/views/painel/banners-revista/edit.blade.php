@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Banners Revista /</small> Editar Banner</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.banners-revista.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.banners-revista.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
