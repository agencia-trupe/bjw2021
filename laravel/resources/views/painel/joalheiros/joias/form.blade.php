@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('tipo_id', 'Tipo de Joia') !!}
    {!! Form::select('tipo_id', $tipos , old('tipo_id'), ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('nome_joia', 'Nome da Joia [PT]') !!}
            {!! Form::text('nome_joia', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('nome_joia_en', 'Nome da Joia [EN]') !!}
            {!! Form::text('nome_joia_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('nome_joia_es', 'Nome da Joia [ES]') !!}
            {!! Form::text('nome_joia_es', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('valor', 'Valor da Joia [Dólar $]') !!}
            {!! Form::text('valor', null, ['class' => 'form-control input-dolar']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well" style="margin: 10px 0 0 0;">
            <div class="checkbox" style="margin:0">
                <label style="font-weight:bold">
                    {!! Form::hidden('preco_sob_consulta', 0) !!}
                    {!! Form::checkbox('preco_sob_consulta') !!}
                    [preço sob consulta]
                </label>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('descricao', 'Descrição da Joia [PT]') !!}
            {!! Form::textarea('descricao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('descricao_en', 'Descrição da Joia [EN]') !!}
            {!! Form::textarea('descricao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('descricao_es', 'Descrição da Joia [ES]') !!}
            {!! Form::textarea('descricao_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem1', 'Imagem 1') !!}
            @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/joalheiros/joias/'.$joia->imagem1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem1', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem2', 'Imagem 2') !!}
            @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/joalheiros/joias/'.$joia->imagem2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem2', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem3', 'Imagem 3') !!}
            @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/joalheiros/joias/'.$joia->imagem3) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem3', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.joalheiros.joias.index', $joalheiro->id) }}" class="btn btn-default btn-voltar">Voltar</a>