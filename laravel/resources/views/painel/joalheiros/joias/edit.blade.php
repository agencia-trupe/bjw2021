@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>{{ $joalheiro->nome }} /</small> Editar Joia</h2>
    </legend>

    {!! Form::model($joia, [
        'route'  => ['painel.joalheiros.joias.update', $joalheiro->id, $joia->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.joalheiros.joias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
