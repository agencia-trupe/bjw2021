@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<a href="{{ route('painel.joalheiros.index') }}" title="Voltar para Expositores" class="btn btn-sm btn-default">
    &larr; Voltar para Joalheiros </a>

<legend>
    <h2>
        {{ $joalheiro->nome }} | Joias
        <a href="{{ route('painel.joalheiros.joias.create', $joalheiro->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Joia</a>
    </h2>
</legend>

@if(!count($joias))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="joalheiros_joias">
    <thead>
        <tr>
            <th>Ordenar</th>
            <th>Nome da Joia</th>
            <th>Preço</th>
            <th>Imagens</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($joias as $joia)
        <tr class="tr-row" id="{{ $joia->id }}">
            <td>
                <a href="#" class="btn btn-info btn-sm btn-move">
                    <span class="glyphicon glyphicon-move"></span>
                </a>
            </td>
            <td>{{ $joia->nome_joia }}</td>
            <td>
                @if($joia->preco_sob_consulta == 1)
                [preço sob consulta]
                @else
                {{ $joia->valor }}
                @endif
            </td>
            <td>
                @if($joia->imagem1)
                <img src="{{ asset('assets/img/joalheiros/joias/thumbs/'.$joia->imagem1) }}" class="imgs-tabelas" alt="">
                @endif
                @if($joia->imagem2)
                <img src="{{ asset('assets/img/joalheiros/joias/thumbs/'.$joia->imagem2) }}" class="imgs-tabelas" alt="">
                @endif
                @if($joia->imagem3)
                <img src="{{ asset('assets/img/joalheiros/joias/thumbs/'.$joia->imagem3) }}" class="imgs-tabelas" alt="">
                @endif
                @if(!$joia->imagem1 && !$joia->imagem2 && !$joia->imagem3)
                <p>Sem imagens</p>
                @endif
            </td>
            <td class="crud-actions" style="width:180px;">
                {!! Form::open([
                'route' => ['painel.joalheiros.joias.destroy', $joalheiro->id, $joia->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.joalheiros.joias.edit', [$joalheiro->id, $joia->id] ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection