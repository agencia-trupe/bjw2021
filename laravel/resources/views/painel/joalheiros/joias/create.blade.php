@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>{{ $joalheiro->nome }} /</small> Adicionar Joia</h2>
</legend>

{!! Form::model($joalheiro, [
'route' => ['painel.joalheiros.joias.store', $joalheiro->id],
'method' => 'post',
'files' => true])
!!}

@include('painel.joalheiros.joias.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection