@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('video', 'Vídeo (Opcional)') !!}
    {!! Form::text('video', null, ['class' => 'form-control']) !!}
    <p class="alerta-quantidade">Incluir aqui somente a parte da url após o "v=". Exemplo: https://www.youtube.com/watch?v=<strong>Y4goaZhNt4k</strong></p>
</div>

<div class="well form-group">
    {!! Form::label('imagem1', 'Imagem 1 (Opcional)') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/joalheiros/processo/thumbs/'.$processo->imagem1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem1', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem2', 'Imagem 2 (Opcional)') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/joalheiros/processo/thumbs/'.$processo->imagem2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem2', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem3', 'Imagem 3 (Opcional)') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/joalheiros/processo/thumbs/'.$processo->imagem3) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem3', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem4', 'Imagem 4 (Opcional)') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/joalheiros/processo/thumbs/'.$processo->imagem4) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem4', ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}