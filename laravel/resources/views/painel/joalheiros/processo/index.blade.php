@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<a href="{{ route('painel.joalheiros.index') }}" title="Voltar para Expositores" class="btn btn-sm btn-default">
    &larr; Voltar para Joalheiros </a>

<legend>
    <h2>
        {{ $joalheiro->nome }} | Processo
        @if(empty($processo))
        <a href="{{ route('painel.joalheiros.processo.create', $joalheiro->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar</a>
        @endif
    </h2>
    <p class="alerta-quantidade">Podem ser adicionadas até 4 imagens e 1 vídeo</p>
</legend>

@if(empty($processo))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>

@else
<div class="processo">
    <h4 class="titulo">Vídeo (YouTube):</h4>
    @if ($processo->video)
    <iframe src="https://www.youtube.com/embed/{{ $processo->video }}" width="760" height="430" frameborder="0" allow="autoplay; fullscreen" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
    @else
    <p class="paragrafo">Sem vídeo</p>
    @endif

    <hr>

    <h4 class="titulo">Imagens:</h4>
    @if ($processo->imagem1)
    <img src="{{ asset('assets/img/joalheiros/processo/thumbs/'.$processo->imagem1) }}" alt="" class="processo-img">
    <hr>
    @endif

    @if ($processo->imagem2)
    <img src="{{ asset('assets/img/joalheiros/processo/thumbs/'.$processo->imagem2) }}" alt="" class="processo-img">
    <hr>
    @endif

    @if ($processo->imagem3)
    <img src="{{ asset('assets/img/joalheiros/processo/thumbs/'.$processo->imagem3) }}" alt="" class="processo-img">
    <hr>
    @endif

    @if ($processo->imagem4)
    <img src="{{ asset('assets/img/joalheiros/processo/thumbs/'.$processo->imagem4) }}" alt="" class="processo-img">
    <hr>
    @endif

    @if(!$processo->imagem1 && !$processo->imagem2 && !$processo->imagem3 && !$processo->imagem4)
    <p class="paragrafo">Sem imagens</p>
    <hr>
    @endif
</div>

{!! Form::open([
'route' => ['painel.joalheiros.processo.destroy', $joalheiro->id, $processo->id],
'method' => 'delete'
]) !!}

<button type="submit" class="btn btn-danger btn-sm btn-delete" style="margin-right:10px;"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>

<a href="{{ route('painel.joalheiros.processo.edit', [$joalheiro->id, $processo->id] ) }}" class="btn btn-primary btn-sm pull-left" style="margin-right:10px;">
    <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
</a>

@endif

@endsection