@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>{{ $joalheiro->nome }} /</small> Editar Processo</h2>
    </legend>

    {!! Form::model($processo, [
        'route'  => ['painel.joalheiros.processo.update', $joalheiro->id, $processo->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.joalheiros.processo.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
