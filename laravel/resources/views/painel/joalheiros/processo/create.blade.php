@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>{{ $joalheiro->nome }} /</small> Adicionar Processo</h2>
</legend>

{!! Form::model($joalheiro, [
'route' => ['painel.joalheiros.processo.store', $joalheiro->id],
'method' => 'post',
'files' => true])
!!}

@include('painel.joalheiros.processo.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection