@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Joalheiros /</small> Adicionar Joalheiro</h2>
    </legend>

    {!! Form::open(['route' => 'painel.joalheiros.store', 'files' => true]) !!}

        @include('painel.joalheiros.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
