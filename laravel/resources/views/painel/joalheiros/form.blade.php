@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('pais_id', 'País') !!}
    {!! Form::select('pais_id', $paises , old('pais_id'), ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto', 'Texto [PT]') !!}
            {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_en', 'Texto [EN]') !!}
            {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_es', 'Texto [ES]') !!}
            {!! Form::textarea('texto_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('whatsapp', 'Whatsapp') !!}
    {!! Form::text('whatsapp', null, ['class' => 'form-control']) !!}
    <p class="alerta-quantidade">Inclur numero completo <strong>sem traços</strong>, incluindo: +código-do-país código-da-cidade telefone-completo <strong>(Exemplo: +55 11 9 9999 9999)</strong></p>
    <p class="alerta-quantidade"><strong>Veja as definições do whatsapp</strong><a href="https://faq.whatsapp.com/general/about-international-phone-number-format/?lang=pt_br" target="_blank"> AQUI</a></p>

</div>

<div class="form-group">
    {!! Form::label('instagram', 'Instagram (link)') !!}
    {!! Form::text('instagram', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('website', 'Website (link)') !!}
    {!! Form::text('website', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/joalheiros/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa_ateliers', 'Capa Ateliers (Destaques)') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/joalheiros/ateliers/'.$registro->capa_ateliers) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('capa_ateliers', ['class' => 'form-control']) !!}
</div>


<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.joalheiros.index') }}" class="btn btn-default btn-voltar">Voltar</a>