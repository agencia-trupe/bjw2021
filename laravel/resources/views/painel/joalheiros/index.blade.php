@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Joalheiros
        <a href="{{ route('painel.joalheiros.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Joalheiro</a>
    </h2>
</legend>

@if(!count($registros))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="joalheiros">
    <thead>
        <tr>
            <th>Capa</th>
            <th>Nome</th>
            <th>País</th>
            <th>Gerenciar</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($registros as $registro)
        <tr class="tr-row" id="{{ $registro->id }}">
            <td>
                <img src="{{ asset('assets/img/joalheiros/thumbs/'.$registro->capa) }}" style="width: 100%; max-width:100px;" alt="">
            </td>
            <td>{{ $registro->nome }}</td>
            <td>{{ $registro->pais_nome }}</td>
            <td>
                <a href="{{ route('painel.joalheiros.joias.index', $registro->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Joias
                </a>
                <a href="{{ route('painel.joalheiros.processo.index', $registro->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Processo
                </a>
                <a href="{{ route('painel.joalheiros.serie.index', $registro->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Série
                </a>
            </td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.joalheiros.destroy', $registro->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.joalheiros.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection