@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>{{ $joalheiro->nome }} /</small> Editar Série</h2>
    </legend>

    {!! Form::model($serie, [
        'route'  => ['painel.joalheiros.serie.update', $joalheiro->id, $serie->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.joalheiros.serie.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
