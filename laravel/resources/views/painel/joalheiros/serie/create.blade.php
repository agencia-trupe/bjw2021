@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>{{ $joalheiro->nome }} /</small> Adicionar Série</h2>
</legend>

{!! Form::model($joalheiro, [
'route' => ['painel.joalheiros.serie.store', $joalheiro->id],
'method' => 'post',
'files' => true])
!!}

@include('painel.joalheiros.serie.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection