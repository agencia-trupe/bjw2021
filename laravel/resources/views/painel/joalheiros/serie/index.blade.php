@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<a href="{{ route('painel.joalheiros.index') }}" title="Voltar para Expositores" class="btn btn-sm btn-default">
    &larr; Voltar para Joalheiros </a>

<legend>
    <h2>
        {{ $joalheiro->nome }} | Mais sobre a série
        @if(empty($serie))
        <a href="{{ route('painel.joalheiros.serie.create', $joalheiro->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar</a>
        @endif
    </h2>
    <p class="alerta-quantidade">Podem ser adicionadas até 4 imagens e 1 vídeo</p>
</legend>

@if(empty($serie))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>

@else
<div class="processo">
    <h4 class="titulo">Vídeo (YouTube):</h4>
    @if ($serie->video)
    <iframe src="https://www.youtube.com/embed/{{ $serie->video }}" width="760" height="430" frameborder="0" allow="autoplay; fullscreen" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
    @else
    <p class="paragrafo">Sem vídeo</p>
    @endif

    <hr>

    <h4 class="titulo">Imagens:</h4>
    @if ($serie->imagem1)
    <img src="{{ asset('assets/img/joalheiros/serie/thumbs/'.$serie->imagem1) }}" alt="" class="processo-img">
    <hr>
    @endif

    @if ($serie->imagem2)
    <img src="{{ asset('assets/img/joalheiros/serie/thumbs/'.$serie->imagem2) }}" alt="" class="processo-img">
    <hr>
    @endif

    @if ($serie->imagem3)
    <img src="{{ asset('assets/img/joalheiros/serie/thumbs/'.$serie->imagem3) }}" alt="" class="processo-img">
    <hr>
    @endif

    @if ($serie->imagem4)
    <img src="{{ asset('assets/img/joalheiros/serie/thumbs/'.$serie->imagem4) }}" alt="" class="processo-img">
    <hr>
    @endif

    @if(!$serie->imagem1 && !$serie->imagem2 && !$serie->imagem3 && !$serie->imagem4)
    <p class="paragrafo">Sem imagens</p>
    <hr>
    @endif
</div>

{!! Form::open([
'route' => ['painel.joalheiros.serie.destroy', $joalheiro->id, $serie->id],
'method' => 'delete'
]) !!}

<button type="submit" class="btn btn-danger btn-sm btn-delete" style="margin-right:10px;"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>

<a href="{{ route('painel.joalheiros.serie.edit', [$joalheiro->id, $serie->id] ) }}" class="btn btn-primary btn-sm pull-left" style="margin-right:10px;">
    <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
</a>

@endif

@endsection