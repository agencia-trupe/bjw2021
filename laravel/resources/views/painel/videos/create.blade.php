@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Vídeos /</small> Adicionar Vídeo</h2>
</legend>

{!! Form::open(['route' => 'painel.videos.store', 'files' => true]) !!}

@include('painel.videos.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection