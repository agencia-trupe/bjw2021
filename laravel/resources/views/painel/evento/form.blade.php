@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome', 'Nome do Evento') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('data_inicio', 'Data de Inicio do Evento') !!}
            {!! Form::date('data_inicio', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('data_fim', 'Data do Fim do Evento') !!}
            {!! Form::date('data_fim', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-12">
        <p style="font-style: italic; color: red; margin: 0 0 10px 20px;">OBS: O <strong>cronômetro</strong> só será exibido se a <strong>data de início do evento for maior que a data atual</strong>.</p>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('periodo', 'Período do Evento [PT]') !!}
            {!! Form::text('periodo', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('periodo_en', 'Período do Evento [EN]') !!}
            {!! Form::text('periodo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('periodo_es', 'Período do Evento [ES]') !!}
            {!! Form::text('periodo_es', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('evento_texto', 'Texto sobre evento (área cinza) [PT]') !!}
            {!! Form::textarea('evento_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('evento_texto_en', 'Texto sobre evento (área cinza) [EN]') !!}
            {!! Form::textarea('evento_texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('evento_texto_es', 'Texto sobre evento (área cinza) [ES]') !!}
            {!! Form::textarea('evento_texto_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('evento_duracao', 'Texto sobre duração do evento [PT]') !!}
            {!! Form::textarea('evento_duracao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('evento_duracao_en', 'Texto sobre duração do evento [EN]') !!}
            {!! Form::textarea('evento_duracao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('evento_duracao_es', 'Texto sobre duração do evento [ES]') !!}
            {!! Form::textarea('evento_duracao_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('nucleo_logo', 'Núcleo - Logo Preto [PT]') !!}
            @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/evento/'.$registro->nucleo_logo) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('nucleo_logo', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('nucleo_logo_en', 'Núcleo - Logo Preto [EN]') !!}
            @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/evento/'.$registro->nucleo_logo_en) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('nucleo_logo_en', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('nucleo_logo_es', 'Núcleo - Logo Preto [ES]') !!}
            @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/evento/'.$registro->nucleo_logo_es) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('nucleo_logo_es', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('nucleo_texto', 'Núcleo - Texto [PT]') !!}
            {!! Form::textarea('nucleo_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('nucleo_texto_en', 'Núcleo - Texto [EN]') !!}
            {!! Form::textarea('nucleo_texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('nucleo_texto_es', 'Núcleo - Texto [ES]') !!}
            {!! Form::textarea('nucleo_texto_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('realizacao_logo', 'Realização - Logo Branco [PT]') !!}
            @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/evento/'.$registro->realizacao_logo) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('realizacao_logo', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('realizacao_logo_en', 'Realização - Logo Branco [EN]') !!}
            @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/evento/'.$registro->realizacao_logo_en) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('realizacao_logo_en', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('realizacao_logo_es', 'Realização - Logo Branco [ES]') !!}
            @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/evento/'.$registro->realizacao_logo_es) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('realizacao_logo_es', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('realizacao_nome', 'Realização - Nome [PT]') !!}
            {!! Form::text('realizacao_nome', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('realizacao_nome_en', 'Realização - Nome [EN]') !!}
            {!! Form::text('realizacao_nome_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('realizacao_nome_es', 'Realização - Nome [ES]') !!}
            {!! Form::text('realizacao_nome_es', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('realizacao_website', 'Realização - Website') !!}
            {!! Form::text('realizacao_website', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('realizacao_whatsapp', 'Realização - Whatsapp') !!}
            {!! Form::text('realizacao_whatsapp', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}