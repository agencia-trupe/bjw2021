@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Tipos de Joias /</small> Editar Tipo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.tipos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.tipos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
