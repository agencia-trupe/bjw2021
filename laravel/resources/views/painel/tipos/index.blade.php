@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Tipos de Joias
        <a href="{{ route('painel.tipos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Tipo de Joia</a>
    </h2>
</legend>


@if(!count($registros))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="tipos">
    <thead>
        <tr>
            <th>Título [PT]</th>
            <th>Título [EN]</th>
            <th>Título [ES]</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($registros as $registro)
        <tr class="tr-row" id="{{ $registro->id }}">
            <td>{{ $registro->titulo }}</td>
            <td>{{ $registro->titulo_en }}</td>
            <td>{{ $registro->titulo_es }}</td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.tipos.destroy', $registro->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.tipos.edit', $registro->id) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection