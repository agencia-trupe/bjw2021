@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Banners Joalheiros /</small> Editar Banner</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.banners-joalheiros.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.banners-joalheiros.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
