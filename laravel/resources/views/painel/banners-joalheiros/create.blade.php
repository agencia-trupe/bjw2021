@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Banners Joalheiros /</small> Adicionar Banner</h2>
    </legend>

    {!! Form::open(['route' => 'painel.banners-joalheiros.store', 'files' => true]) !!}

        @include('painel.banners-joalheiros.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
