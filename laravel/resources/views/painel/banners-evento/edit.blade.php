@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Banners Evento /</small> Editar Banner</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.banners-evento.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.banners-evento.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
