@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Banners Evento /</small> Adicionar Banner</h2>
    </legend>

    {!! Form::open(['route' => 'painel.banners-evento.store', 'files' => true]) !!}

        @include('painel.banners-evento.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
