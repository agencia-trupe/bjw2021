@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Grupo/Escola: {{ $registro->nome }} /</small> Editar Grupo/Escola</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.grupos-escolas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.grupos-escolas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
