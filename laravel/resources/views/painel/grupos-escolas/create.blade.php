@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Grupos & Escolas /</small> Adicionar Grupo/Escola</h2>
    </legend>

    {!! Form::open(['route' => 'painel.grupos-escolas.store', 'files' => true]) !!}

        @include('painel.grupos-escolas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
