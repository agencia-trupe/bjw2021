@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Grupos & Escolas
        <a href="{{ route('painel.grupos-escolas.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Grupo/Escola</a>
    </h2>
</legend>

@if(!count($registros))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="grupos_escolas">
    <thead>
        <tr>
            <th>Capa</th>
            <th>Nome</th>
            <th>Perfil</th>
            <th>País</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($registros as $registro)
        <tr class="tr-row" id="{{ $registro->id }}">
            <td>
                <img src="{{ asset('assets/img/grupos-escolas/thumbs/'.$registro->capa) }}" style="width: 100%; max-width:100px;" alt="">
            </td>
            <td>{{ $registro->nome }}</td>
            <td>{{ $perfis[$registro->perfil] }}</td>
            <td>{{ $registro->pais_nome }}</td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.grupos-escolas.destroy', $registro->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.grupos-escolas.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection