@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('data', 'Data') !!}
    {!! Form::date('data', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('dia_evento', 'Título p/ Dia do Evento (texto após a data - opcional) - [PT]') !!}
    {!! Form::text('dia_evento', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('dia_evento_en', 'Título p/ Dia do Evento (texto após a data - opcional) - [EN]') !!}
    {!! Form::text('dia_evento_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('dia_evento_es', 'Título p/ Dia do Evento (texto após a data - opcional) - [ES]') !!}
    {!! Form::text('dia_evento_es', null, ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.agenda.index') }}" class="btn btn-default btn-voltar">Voltar</a>
