@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Palestras /</small> Editar Palestra</h2>
    <br>
    <p>Agenda: {{ strftime("%d/%m/%Y", strtotime($agenda->data)) }}</p>
</legend>

{!! Form::model($palestra, [
'route' => ['painel.agenda.palestras.update', $agenda->id, $palestra->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.agenda.palestras.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection