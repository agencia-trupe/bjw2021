@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<a href="{{ route('painel.agenda.index') }}" title="Voltar para Agenda" class="btn btn-sm btn-default">
    &larr; Voltar para Agenda </a>

<legend>
    <h2>
        Palestras
        <a href="{{ route('painel.agenda.palestras.create', $agenda->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Palestra</a>
    </h2>
</legend>


@if(!count($palestras))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="agenda_palestras">
    <thead>
        <tr>
            <th>Ordenar</th>
            <th>Data</th>
            <th>Horário</th>
            <th>Titulo</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($palestras as $palestra)
        <tr class="tr-row" id="{{ $palestra->id }}">
            <td>
                <a href="#" class="btn btn-info btn-sm btn-move">
                    <span class="glyphicon glyphicon-move"></span>
                </a>
            </td>
            <td>{{ strftime("%d/%m/%Y", strtotime($agenda->data)) }}</td>
            <td>{{ $palestra->horario }}</td>
            <td>{{ $palestra->titulo }}</td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.agenda.palestras.destroy', $agenda->id, $palestra->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.agenda.palestras.edit', [$agenda->id, $palestra->id]) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection