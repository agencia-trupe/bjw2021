@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('horario', 'Horario (Exemplo: 9h)') !!}
    {!! Form::text('horario', null, ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('tipo', 'Tipo da Palestra [PT]') !!}
            {!! Form::text('tipo', null, ['class' => 'form-control']) !!}
            <p class="obs" style="color:red;font-style:italic;">Exemplo: Visita guiada pela exposição</p>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('tipo_en', 'Tipo da Palestra [EN]') !!}
            {!! Form::text('tipo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('tipo_es', 'Tipo da Palestra [ES]') !!}
            {!! Form::text('tipo_es', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo', 'Título da Palestra [PT]') !!}
            {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo_en', 'Título da Palestra [EN]') !!}
            {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo_es', 'Título da Palestra [ES]') !!}
            {!! Form::text('titulo_es', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('nome', 'Nome do Apresentador') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('descricao', 'Descrição da Palestra [PT]') !!}
            {!! Form::textarea('descricao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('descricao_en', 'Descrição da Palestra [EN]') !!}
            {!! Form::textarea('descricao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('descricao_es', 'Descrição da Palestra [ES]') !!}
            {!! Form::textarea('descricao_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('nome_link', 'Nome do Link (Encontros e mais) [PT]') !!}
            {!! Form::text('nome_link', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('nome_link_en', 'Nome do Link (Encontros e mais) [EN]') !!}
            {!! Form::text('nome_link_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('nome_link_es', 'Nome do Link (Encontros e mais) [ES]') !!}
            {!! Form::text('nome_link_es', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('link', 'Link') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_link', 'Texto após link (opcional) [PT]') !!}
            {!! Form::textarea('texto_link', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_link_en', 'Texto após link (opcional) [EN]') !!}
            {!! Form::textarea('texto_link_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_link_es', 'Texto após link (opcional) [ES]') !!}
            {!! Form::textarea('texto_link_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('id_reuniao', 'Id da Reunião (opcional)') !!}
    {!! Form::text('id_reuniao', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('senha_acesso', 'Senha de Acesso (opcional)') !!}
    {!! Form::text('senha_acesso', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/agenda/'.$palestra->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.agenda.palestras.index', $agenda->id) }}" class="btn btn-default btn-voltar">Voltar</a>