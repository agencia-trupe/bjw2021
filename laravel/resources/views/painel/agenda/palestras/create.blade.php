@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Palestras /</small> Adicionar Palestra</h2>
    <br>
    <p>Agenda: {{ strftime("%d/%m/%Y", strtotime($agenda->data)) }}</p>
</legend>

{!! Form::model($agenda, [
'route' => ['painel.agenda.palestras.store', $agenda->id],
'method' => 'post',
'files' => true])
!!}

@include('painel.agenda.palestras.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection