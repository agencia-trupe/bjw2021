@extends('painel.common.template')

@section('content')

<legend>
    <h2>Palestra</h2>
</legend>

<div class="form-group">
    <label>Data</label>
    <div class="well">{{ $registro->data }}</div>
</div>

<div class="form-group">
    <label>Horario</label>
    <div class="well">{{ $palestra->horario }}</div>
</div>

<div class="form-group">
    <label>Tipo da Palestra</label>
    <div class="well">{{ $palestra->tipo }}</div>
</div>

<div class="form-group">
    <label>Título da Palestra</label>
    <div class="well">{{ $palestra->titulo }}</div>
</div>

<div class="form-group">
    <label>Nome do Apresentador</label>
    <div class="well">{{ $palestra->nome }}</div>
</div>

<div class="form-group">
    <label>Descrição da Palestra</label>
    <div class="well">{!! $palestra->descricao !!}</div>
</div>

<div class="form-group">
    <label>Link (Transmissão Zoom)</label>
    <div class="well">{{ $palestra->link }}</div>
</div>

<div class="form-group">
    <label>Id da Reunião</label>
    <div class="well">{{ $palestra->id_reuniao }}</div>
</div>

<div class="form-group">
    <label>Senha de Acesso</label>
    <div class="well">{{ $palestra->senha_acesso }}</div>
</div>

<div class="form-group">
    <label>Imagem</label>
    <div class="well"><img src="{{ asset('assets/img/agenda/'.$palestra->imagem) }}" alt=""></div>
</div>

<a href="{{ route('painel.agenda.palestras.index', $registro->id) }}" class="btn btn-default btn-voltar">Voltar</a>

@stop