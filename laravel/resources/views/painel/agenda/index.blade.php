@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Agenda
        <a href="{{ route('painel.agenda.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Data</a>
    </h2>
</legend>


@if(!count($registros))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="agenda">
    <thead>
        <tr>
            <th>Data</th>
            <th>Título p/ Dia do Evento (opcional)</th>
            <th>Gerenciar</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($registros as $registro)
        <tr class="tr-row" id="{{ $registro->id }}">
            <td>{{ strftime("%d/%m/%Y", strtotime($registro->data)) }}</td>
            <td>{{ $registro->dia_evento }}</td>
            <td>
                <a href="{{ route('painel.agenda.palestras.index', $registro->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Palestras
                </a>
            </td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.agenda.destroy', $registro->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.agenda.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection