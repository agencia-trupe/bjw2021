@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Agenda /</small> Adicionar Data</h2>
    </legend>

    {!! Form::open(['route' => 'painel.agenda.store', 'files' => true]) !!}

        @include('painel.agenda.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
