<ul class="nav navbar-nav">

    <li class="dropdown @if(Tools::routeIs('painel.banners*')) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Home <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.banners-evento*')) class="active" @endif>
                <a href="{{ route('painel.banners-evento.index') }}">Banners Evento</a>
            </li>
            <li @if(Tools::routeIs('painel.banners-joalheiros*')) class="active" @endif>
                <a href="{{ route('painel.banners-joalheiros.index') }}">Banners Joalheiros</a>
            </li>
            <li @if(Tools::routeIs('painel.banners-revista*')) class="active" @endif>
                <a href="{{ route('painel.banners-revista.index') }}">Banners Revista</a>
            </li>
        </ul>
    </li>

    <li @if(Tools::routeIs('painel.evento*')) class="active" @endif>
        <a href="{{ route('painel.evento.index') }}">Evento</a>
    </li>

    <li class="dropdown @if(Tools::routeIs('painel.tipos*')) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Encontros e Mais <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.agenda*')) class="active" @endif>
                <a href="{{ route('painel.agenda.index') }}">Agenda</a>
            </li>
            <li @if(Tools::routeIs('painel.videos*')) class="active" @endif>
                <a href="{{ route('painel.videos.index') }}">Vídeos</a>
            </li>
        </ul>
    </li>

    <li class="dropdown @if(Tools::routeIs('painel.tipos*')) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Joias & Joalheiros <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.joalheiros*')) class="active" @endif>
                <a href="{{ route('painel.joalheiros.index') }}">Joalheiros</a>
            </li>
            <li @if(Tools::routeIs('painel.tipos*')) class="active" @endif>
                <a href="{{ route('painel.tipos.index') }}">Tipos de Joias</a>
            </li>
        </ul>
    </li>

    <li @if(Tools::routeIs('painel.grupos-escolas*')) class="active" @endif>
        <a href="{{ route('painel.grupos-escolas.index') }}">Grupos & Escolas</a>
    </li>

    <li @if(Tools::routeIs('painel.assinaturas-livro*')) class="active" @endif>
        <a href="{{ route('painel.assinaturas-livro.index') }}">Assinaturas</a>
    </li>

    <li @if(Tools::routeIs('painel.cadastros*')) class="active" @endif>
        <a href="{{ route('painel.cadastros.index') }}">Cadastros</a>
    </li>

    <li class="dropdown @if(Tools::routeIs('painel.relatorios*')) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Relatórios <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.relatorios.download-ebook')) class="active" @endif>
                <a href="{{ route('painel.relatorios.download-ebook') }}">Download E-book</a>
            </li>
            <li @if(Tools::routeIs('painel.relatorios.palestra-zoom')) class="active" @endif>
                <a href="{{ route('painel.relatorios.palestra-zoom') }}">Palestra Zoom</a>
            </li>
            <li @if(Tools::routeIs('painel.relatorios.compartilhar-joia')) class="active" @endif>
                <a href="{{ route('painel.relatorios.compartilhar-joia') }}">Compartilhar Joia</a>
            </li>
        </ul>
    </li>

</ul>