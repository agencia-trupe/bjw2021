@extends('frontend.common.template')

@section('content')

<div class="pag-favoritas">

    <div class="center">
        <h2 class="titulo">{{ trans('frontend.favoritas.titulo') }}</h2>
        <p class="user-nome">{{ $userLogado->nome }}</p>

        <div class="loading">
            <img src="{{ asset('assets/img/layout/load.gif') }}" alt="">
        </div>

        @if(count($itens) > 0)
        <div class="joias-favoritas">
            @foreach($itens as $joias)
            @foreach($joias as $joia)
            <div class="joia">
                <div class="imgs-joias">
                    @if(isset($joia->imagem1))
                    <img src="{{ asset('assets/img/joalheiros/joias/'.$joia->imagem1) }}" alt="" class="img-joia">
                    @endif
                    @if(isset($joia->imagem2))
                    <img src="{{ asset('assets/img/joalheiros/joias/'.$joia->imagem2) }}" alt="" class="img-joia">
                    @endif
                    @if(isset($joia->imagem3))
                    <img src="{{ asset('assets/img/joalheiros/joias/'.$joia->imagem3) }}" alt="" class="img-joia">
                    @endif
                </div>
                <p class="nome-joia">{{ $joia->{trans('banco.nome_joia')} }}</p>
                <p class="joalheiro">{{ $joia->nome }} | {{ $joia->{trans('banco.pais_nome')} }}</p>
                @if($joia->preco_sob_consulta == 1)
                <p class="valor">[{{ trans('frontend.joalheiros.preco') }}]</p>
                @else
                <p class="valor">{{ $joia->valor }}</p>
                @endif
                <div class="acoes">
                    {!! Form::open([
                    'route' => ['favoritas.delete', 'user' => auth('evento')->user()->id, 'joia' => $joia->id],
                    'method' => 'delete'
                    ]) !!}
                    <button type="submit" class="favoritar"><img src="{{ asset('assets/img/layout/icone-joia-desfavoritar.svg') }}" alt=""> {{ trans('frontend.favoritas.exluir') }}</button>
                    {!! Form::close() !!}
                    <button class="compartilhar" ype="button" id="btnModalCompartilhar" data-toggle="modal" data-target="#modalCompartilhar" joia="{{$joia->id}}"><img src="{{ asset('assets/img/layout/icone-joia-compartilhar.svg') }}" alt="">{{ trans('frontend.favoritas.compartilhar') }}</button>
                </div>
            </div>

            @if(isset($joia))
            <!-- MODAL COMPARTILHAR -->
            <div class="modal fade modal-compartilhar" id="modalCompartilhar{{$joia->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
                <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close">X</button>
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <h4 class="modal-titulo" id="myModalLabel">{{ trans('frontend.joalheiros.modal-compartilhar') }} <img src="{{ asset('assets/img/layout/ico-compartilhar.svg') }}" alt=""></h4>
                        <ul class="compartilhamento">
                            @if(auth('evento')->check())
                            <a class="comp-whatsapp" href="{{ route('relatorio.whatsapp.compartilhar', ['user' => auth('evento')->user()->id, 'joia' => $joia->id]) }}" target="_blank"><img src="{{ asset('assets/img/layout/ico-whatsapp30.svg') }}" alt=""></a>
                            <a class="comp-facebook" href="{{ route('relatorio.facebook.compartilhar', ['user' => auth('evento')->user()->id, 'joia' => $joia->id]) }}" title="Facebook" target="_blank"><img src="{{ asset('assets/img/layout/ico-facebook30.svg') }}" alt=""></a>
                            <a class="comp-email" href="{{ route('relatorio.email.compartilhar', ['user' => auth('evento')->user()->id, 'joia' => $joia->id]) }}" target="_blank" title="E-mail"><img src="{{ asset('assets/img/layout/ico-email30.svg') }}" alt=""></a>

                            @else
                            <a class="comp-whatsapp link-deslogado" href="" target="_blank"><img src="{{ asset('assets/img/layout/ico-whatsapp30.svg') }}" alt=""></a>
                            <a class="comp-facebook link-deslogado" href="" title="Facebook" target="_blank"><img src="{{ asset('assets/img/layout/ico-facebook30.svg') }}" alt=""></a>
                            <a class="comp-email link-deslogado" href="" target="_blank" title="E-mail"><img src="{{ asset('assets/img/layout/ico-email30.svg') }}" alt=""></a>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            @endif
            @endforeach
            @endforeach
        </div>
        @else
        <div class="joias-favoritas">
            <p class="nenhuma">{{ trans('frontend.favoritas.nenhuma') }}</p>
        </div>
        @endif
    </div>

    <hr class="hr-atualizar-cadastro center">

    <div class="atualizar-cadastro center">
        <h2 class="titulo">{{ trans('frontend.favoritas.atualizar-cadastro') }}</h2>
        <form action="{{ route('evento.atualizar.user', $userLogado->id) }}" method="POST">
            {!! csrf_field() !!}
            <div class="grupo">
                <label for="email">LOGIN (E-MAIL)</label>
                <input type="email" name="email" value="{{ $userLogado->email }}" required>
            </div>
            <div class="grupo">
                <label for="senha">{{ trans('frontend.modal.senha') }}</label>
                <input type="password" name="senha" value="{{ $userLogado->senha }}" required>
            </div>
            <div class="grupo">
                <label for="senha_confirmation">{{ trans('frontend.modal.repetir-senha') }}</label>
                <input type="password" name="senha_confirmation" id="senha_confirmation" required>
            </div>
            <div class="grupo">
                <label for="nome">{{ trans('frontend.modal.nome') }}</label>
                <input type="text" name="nome" value="{{ $userLogado->nome }}" required>
            </div>
            <div class="grupo">
                <label for="telefone">{{ trans('frontend.modal.telefone') }}</label>
                <input type="text" name="telefone" value="{{ $userLogado->telefone }}" required>
            </div>
            <div class="grupo">
                <label for="pais">{{ trans('frontend.modal.pais') }}</label>
                <input type="text" name="pais" value="{{ $userLogado->pais }}" required>
            </div>
            <button type="submit" class="btn btn-primary" id="btnAtualizar">{{ trans('frontend.favoritas.btn-atualizar') }}</button>
        </form>
    </div>
</div>

@include('frontend.filtro')

@include('frontend.links')

@endsection