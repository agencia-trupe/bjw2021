@extends('frontend.common.template')

@section('content')

<div class="resultados-filtro">

    <div class="center">
        <h2 class="titulo">{{ trans('frontend.filtro.resultados') }}</h2>
        <div class="itens-pesquisados">
            @if(isset($pais))<p class="pais"><span>{{ trans('frontend.filtro.pais') }}:</span> {{ $pais->{trans('banco.nome')} }}</p>@endif
        </div>

        @if(count($joalheiros) > 0)
        <div class="resultados">
            <h4 class="titulo-filtro">{{ trans('frontend.filtro.titulo-joalheiros') }}</h4>
            <div class="joalheiros">
                @foreach($joalheiros as $joalheiro)
                <a href="{{ route('joalheiros.show', $joalheiro->slug) }}" class="link-joalheiro">
                    <img src="{{ asset('assets/img/joalheiros/thumbs/'.$joalheiro->capa) }}" alt="" class="img-joalheiro">
                    <p class="nome">{{ $joalheiro->nome }}</p>
                </a>
                @endforeach
            </div>
        </div>
        @endif
        @if(count($grupos) > 0)
        <div class="resultados">
            <h4 class="titulo-filtro">{{ trans('frontend.filtro.titulo-grupos') }}</h4>
            <div class="grupos">
                @foreach($grupos as $grupo)
                <a href="{{ route('escolas-e-grupos.show', $grupo->slug) }}" class="link-grupo">
                    <img src="{{ asset('assets/img/grupos-escolas/thumbs/'.$grupo->capa) }}" alt="" class="img-grupo">
                    <p class="nome">{{ $grupo->nome }}</p>
                </a>
                @endforeach
            </div>
        </div>
        @endif
        @if(count($joalheiros) == 0 && count($grupos) == 0)
        <div class="resultados">
            <p class="nenhuma">{{ trans('frontend.filtro.nenhuma') }}</p>
        </div>
        @endif
    </div>

</div>

@include('frontend.filtro')

@include('frontend.links')

@endsection