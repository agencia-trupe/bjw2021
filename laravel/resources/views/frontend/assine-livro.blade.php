@extends('frontend.common.template')

@section('content')

<div class="pag-assine-livro">
    <div class="center">
        <div class="left">
            <h4 class="titulo">{{ trans('frontend.assine-livro.titulo') }}</h4>
            <p class="mensagem">{{ trans('frontend.assine-livro.mensagem') }}</p>
            <p class="aviso">{{ trans('frontend.assine-livro.aviso') }}</p>
        </div>
        <form action="{{ route('assine-livro.post', $user->id) }}" method="POST">
            {!! csrf_field() !!}
            <input type="hidden" name="user" class="user-cadastro" value="{{ $user->id }}">
            <textarea name="mensagem" required>{{ old('mensagem') }}</textarea>
            <button type="submit" class="btn btn-primary" id="btnAssinarLivro">
                <img src="{{ asset('assets/img/layout/icone-assine-preto.svg') }}" alt="">
                {{ trans('frontend.assine-livro.btn-assinar') }}
            </button>
        </form>
    </div>

    <hr class="linha-assinaturas">

    <div class="assinaturas center masonry-grid">
        @foreach($assinaturas as $assinatura)
        <div class="assinatura grid-item">
            <div class="mensagem">{!! $assinatura->mensagem !!}</div>
            <p class="nome">{{ $assinatura->nome }} • {{ $assinatura->pais }}</p>
            <p class="data-assinatura">{{ strftime("%d %B %Y", strtotime($assinatura->created_at)) }}</p>
        </div>
        @endforeach
    </div>
    <button class="btn-assinaturas-mais">{{ trans('frontend.assine-livro.ver-mais') }} +</button>
</div>

@endsection