@extends('frontend.common.template')

@section('content')

<main class="home">

    <section class="banners-evento center">
        @if(count($bannersEvento) == 1)
        @foreach($bannersEvento as $banner)
        <article class="banner" style="background-image: url({{ asset('assets/img/banners/'.$banner->imagem) }})">
            <p class="titulo" style="color: {{$banner->cor_texto}};">{{ $banner->{trans('banco.titulo')} }}</p>
            <a href="{{ route('agenda') }}" class="link-agenda" style="color: {{$banner->cor_texto}};border: 1px solid {{$banner->cor_texto}};">{{ trans('frontend.home.agenda') }}</a>
        </article>
        @endforeach
        @else
        <!-- Start WOWSlider.com BODY section -->
        <div id="wowslider-container1" style="overflow: hidden; border-radius: 20px;">
            <div class="ws_images">
                <ul>
                    @foreach($bannersEvento as $banner)
                    <li><img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" alt="" title="" id="wows1_0" /></li>
                    @endforeach
                </ul>
            </div>
            <div class="ws_shadow"></div>
        </div>
        <p class="titulo-banner-evento" style="color: {{$tituloBannerEvento->cor_texto}};">{{ $tituloBannerEvento->{trans('banco.titulo')} }}</p>
        <a href="{{ route('agenda') }}" class="link-agenda-banner">{{ trans('frontend.home.agenda') }}</a>
        <script type="text/javascript" src="{{ asset('assets/js/wowslider/engine1/wowslider.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/wowslider/engine1/script.js') }}"></script>
        <!-- End WOWSlider.com BODY section -->
        @endif
    </section>

    <section class="nucleo center">
        <!-- <article class="logo">
            <img src="{{ asset('assets/img/evento/'.$evento->{trans('banco.nucleo_logo')}) }}" alt="{{ $evento->{trans('banco.realizacao_nome')} }}" class="img-nucleo">
        </article> -->
        @php
        $dataAtual = date('Y-m-d');
        if($evento->data_inicio > $dataAtual) {
        $exibir = true;
        } else {
        $exibir = false;
        }
        @endphp
        @if($exibir == true)
        <a href="https://www.braziljewelryweek.com/" target="_blank" class="cronometro-evento">{{ trans('frontend.home.cronometro-faltam') }}
            <p hidden class="data-inicio">{{ $evento->data_inicio }}</p>
            <p class="x-dias"></p> {{ trans('frontend.home.cronometro-dias') }}
            <p class="x-horas"></p> {{ trans('frontend.home.cronometro-horas') }}
            <p class="x-minutos"></p> {{ trans('frontend.home.cronometro-minutos-aofim') }}
        </a>
        @endif
        <article class="texto">
            {!! $evento->{trans('banco.nucleo_texto')} !!}
        </article>
    </section>

    <section class="joias-destaques center">
        <h2 class="titulo">{{ trans('frontend.home.titulo-joias') }}</h2>
        <hr class="linha-titulo">
        <article id="joiasDestaques">
            @foreach($joias as $joia)
            <div class="joia">
                <a href="{{ route('joalheiros.show', $joia->slug_joalheiro) }}" class="link-joia" id="{{ $joia->id }}" title="{{ $joia->nome_joalheiro }}">
                    <img src="{{ asset('assets/img/joalheiros/joias/'.$joia->imagem1) }}" class="img-joia" style="max-width:100%">
                    <p class="nome-joia">{{ $joia->{trans('banco.nome_joia')} }}</p>
                    <p class="joalheiro">{{ $joia->nome_joalheiro }} • {{ $joia->{trans('banco.pais_nome')} }}</p>
                    @if($joia->preco_sob_consulta == 1)
                    <p class="preco-joia">[{{ trans('frontend.joalheiros.preco') }}]</p>
                    @else
                    <p class="preco-joia">{{ $joia->valor }}</p>
                    @endif
                </a>
                <div class="acoes">
                    @if(auth('evento')->check())
                    {!! Form::open([
                    'route' => ['favoritas.post', 'user' => auth('evento')->user()->id, 'joia' => $joia->id],
                    'method' => 'post'
                    ]) !!}
                    <button type="submit" class="favoritar"><img src="{{ asset('assets/img/layout/icone-joia-favoritar.svg') }}" alt="{{ trans('frontend.joalheiros.favoritar') }}"></button>
                    {!! Form::close() !!}
                    <button class="compartilhar" ype="button" id="btnModalCompartilhar" data-toggle="modal" data-target="#modalCompartilhar" joia="{{$joia->id}}"><img src="{{ asset('assets/img/layout/icone-joia-compartilhar.svg') }}" alt="{{ trans('frontend.joalheiros.compartilhar') }}"></button>

                    @else
                    <button type="submit" class="favoritar link-deslogado"><img src="{{ asset('assets/img/layout/icone-joia-favoritar.svg') }}" alt="{{ trans('frontend.joalheiros.favoritar') }}"></button>
                    <button class="compartilhar link-deslogado" ype="button" id="btnModalCompartilhar" data-toggle="modal" data-target="#modalCompartilhar"><img src="{{ asset('assets/img/layout/icone-joia-compartilhar.svg') }}" alt="{{ trans('frontend.joalheiros.compartilhar') }}"></button>
                    @endif
                </div>
            </div>

            <!-- MODAL COMPARTILHAR -->
            <div class="modal fade modal-compartilhar" id="modalCompartilhar{{$joia->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
                <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close">X</button>
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <h4 class="modal-titulo" id="myModalLabel">{{ trans('frontend.joalheiros.modal-compartilhar') }} <img src="{{ asset('assets/img/layout/ico-compartilhar.svg') }}" alt=""></h4>
                        <ul class="compartilhamento">
                            @if(auth('evento')->check())
                            <a class="comp-whatsapp" href="{{ route('relatorio.whatsapp.compartilhar', ['user' => auth('evento')->user()->id, 'joia' => $joia->id]) }}" target="_blank"><img src="{{ asset('assets/img/layout/ico-whatsapp30.svg') }}" alt=""></a>
                            <a class="comp-facebook" href="{{ route('relatorio.facebook.compartilhar', ['user' => auth('evento')->user()->id, 'joia' => $joia->id]) }}" title="Facebook" target="_blank"><img src="{{ asset('assets/img/layout/ico-facebook30.svg') }}" alt=""></a>
                            <a class="comp-email" href="{{ route('relatorio.email.compartilhar', ['user' => auth('evento')->user()->id, 'joia' => $joia->id]) }}" target="_blank" title="E-mail"><img src="{{ asset('assets/img/layout/ico-email30.svg') }}" alt=""></a>

                            @else
                            <a class="comp-whatsapp link-deslogado" href="" target="_blank"><img src="{{ asset('assets/img/layout/ico-whatsapp30.svg') }}" alt=""></a>
                            <a class="comp-facebook link-deslogado" href="" title="Facebook" target="_blank"><img src="{{ asset('assets/img/layout/ico-facebook30.svg') }}" alt=""></a>
                            <a class="comp-email link-deslogado" href="" target="_blank" title="E-mail"><img src="{{ asset('assets/img/layout/ico-email30.svg') }}" alt=""></a>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            @endforeach
        </article>
        <a href="{{ route('joalheiros') }}" class="link-ver-mais">{{ trans('frontend.home.ver-mais') }}</a>
    </section>

    <section class="banners-joalheiros center">
        @foreach($bannersJoalheiros as $banner)
        <article class="banner" style="background-image: url({{ asset('assets/img/banners/'.$banner->imagem) }})">
            <p class="titulo" style="color: {{$banner->cor_texto}};">{{ $banner->{trans('banco.titulo')} }}</p>
            <a href="{{ route('joalheiros') }}" class="link-joalheiros" style="color: {{$banner->cor_texto}};border: 1px solid {{$banner->cor_texto}};">{{ trans('frontend.home.ver-mais') }}</a>
        </article>
        @endforeach
    </section>

    <section class="joalheiros-destaques center">
        <h2 class="titulo">{{ trans('frontend.home.titulo-joalheiros') }}</h2>
        <hr class="linha-titulo">
        <article id="joalheirosDestaques">
            @foreach($joalheiros as $joalheiro)
            <a href="{{ route('joalheiros.show', $joalheiro->slug) }}" class="link-joalheiro">
                <img src="{{ asset('assets/img/joalheiros/thumbs/'.$joalheiro->capa) }}" alt="{{ $joalheiro->nome }}" class='img-joalheiro'>
                <p class='nome-joalheiro'>{{ $joalheiro->nome }}</p>
                <p class='pais'>{{ $joalheiro->{trans('banco.pais_nome')} }}</p>
            </a>
            @endforeach
        </article>
        <a href="{{ route('joalheiros') }}" class="link-ver-mais">{{ trans('frontend.home.ver-mais') }}</a>
    </section>

    <section class="banners-revista">
        @foreach($bannersRevista as $banner)
        <article class="banner" style="background-image: url({{ asset('assets/img/banners/'.$banner->imagem) }})">
            <p class="titulo-revista" style="color: {{$banner->cor_texto}};">{{ trans('frontend.header.revista') }}</p>
            <hr class="linha-titulo" style="border-bottom: 1px solid {{$banner->cor_texto}};">
            <p class="titulo" style="color: {{$banner->cor_texto}};">{{ $banner->{trans('banco.titulo')} }}</p>
            <a href="https://cdn.flipsnack.com/widget/v2/widget.html?hash=1nqofa5hp8" target="_blank" class="link-revista" style="color: {{$banner->cor_texto}};border: 1px solid {{$banner->cor_texto}};">{{ trans('frontend.home.entrar') }}</a>
        </article>
        @endforeach
    </section>

    <section class="escolas-destaques center">
        <h2 class="titulo">{{ trans('frontend.home.titulo-escolas') }}</h2>
        <hr class="linha-titulo">
        <article id="escolasDestaques">
            @foreach($escolas as $escola)
            <a href="{{ route('escolas-e-grupos.show', $escola->slug) }}" class='link-escola'>
                <img src="{{ asset('assets/img/grupos-escolas/thumbs/'.$escola->capa) }}" alt="{{ $escola->nome }}" class='img-escola'>
                <p class='nome-escola'>{{ $escola->nome }}</p>
                <p class='pais'>{{ $escola->{trans('banco.pais_nome')} }}</p>
            </a>
            @endforeach
        </article>
        <a href="{{ route('escolas-e-grupos') }}" class="link-ver-mais">{{ trans('frontend.home.ver-mais') }}</a>
    </section>

    <section class="grupos-destaques center">
        <h2 class="titulo">{{ trans('frontend.home.titulo-grupos') }}</h2>
        <hr class="linha-titulo">
        <article id="gruposDestaques">
            @foreach($grupos as $grupo)
            <a href="{{ route('escolas-e-grupos.show', $grupo->slug) }}" class='link-grupo'>
                <img src="{{ asset('assets/img/grupos-escolas/thumbs/'.$grupo->capa) }}" alt="{{ $grupo->nome }}" class='img-grupo'>
                <p class='nome-grupo'>{{ $grupo->nome }}</p>
                <p class='pais'>{{ $grupo->{trans('banco.pais_nome')} }}</p>
            </a>
            @endforeach
        </article>
        <a href="{{ route('escolas-e-grupos') }}" class="link-ver-mais">{{ trans('frontend.home.ver-mais') }}</a>
    </section>

    <section class="ateliers-destaques center">
        <h2 class="titulo">{{ trans('frontend.home.titulo-ateliers') }}</h2>
        <hr class="linha-titulo">
        <article id="ateliersDestaques">
            @foreach($ateliers as $atelier)
            <a href="{{ route('joalheiros.show', $atelier->slug) }}" class='link-atelier'>
                <img src="{{ asset('assets/img/joalheiros/ateliers/'.$atelier->capa_ateliers) }}" alt="{{ $atelier->nome }}" class='img-atelier'>
                <p class='nome-atelier'>{{ $atelier->nome }}</p>
                <p class='pais'>{{ $atelier->{trans('banco.pais_nome')} }}</p>
            </a>
            @endforeach
        </article>
        <a href="{{ route('joalheiros') }}" class="link-ver-mais">{{ trans('frontend.home.ver-mais') }}</a>
    </section>

    <section class="sobre">
        <div class="center">
            @if(Lang::getLocale() == "en")
            <img src="{{ asset('assets/img/layout/marca-bjw-permanente-ingles.svg') }}" alt="BJW" class="img-logo">
            @elseif(Lang::getLocale() == "es")
            <img src="{{ asset('assets/img/layout/marca-bjw-permanente-espanhol.svg') }}" alt="BJW" class="img-logo">
            @else
            <img src="{{ asset('assets/img/layout/marca-bjw-permanente-portugues.svg') }}" alt="BJW" class="img-logo">
            @endif
            <p class="periodo">{{ $evento->{trans('banco.periodo')} }}</p>
            <div class="texto">{!! $evento->{trans('banco.evento_texto')} !!}</div>
            <div class="duracao">{!! $evento->{trans('banco.evento_duracao')} !!}</div>
            <div class="links-evento">
                @if(auth('evento')->check())
                <a href="{{ route('ebook.download', auth('evento')->user()->id) }}" class="btn-ebook">{{ trans('frontend.home.obtenha-ebook') }} <img src="{{ asset('assets/img/layout/icone-ebook.svg') }}" alt="" class="img-ebook"></a>
                <a href="{{ route('assine-livro', auth('evento')->user()->id) }}" class="btn-livro"><img src="{{ asset('assets/img/layout/icone-assine.svg') }}" alt="" class="img-assine"> {{ trans('frontend.home.assine-livro') }}</a>
                @else
                <a href="" class="btn-ebook link-deslogado">{{ trans('frontend.home.obtenha-ebook') }} <img src="{{ asset('assets/img/layout/icone-ebook.svg') }}" alt="" class="img-ebook"></a>
                <a href="" class="btn-livro link-deslogado"><img src="{{ asset('assets/img/layout/icone-assine.svg') }}" alt="" class="img-assine"> {{ trans('frontend.home.assine-livro') }}</a>
                @endif
            </div>
        </div>
    </section>



</main>

@endsection