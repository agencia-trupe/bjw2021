<section class="filtro" @if(Tools::routeIs(['agenda*', 'joalheiros', 'escolas-e-grupos', 'escolas-e-grupos-show*'])) style="margin-top: 0;" @endif>
    <div class="center-filtro">
        <h2 class="titulo">{{ trans('frontend.filtro.titulo') }}</h2>
        <select name="pais_id" class="paises">
            <option value="">{{ trans('frontend.filtro.pais') }}</option>
            @foreach($paises as $pais)
            <option value="{{ $pais->id }}">{{ $pais->{trans('banco.nome')} }}</option>
            @endforeach
        </select>
        <select name="tipo_id" class="tipos">
            <option value="">{{ trans('frontend.filtro.tipo') }}</option>
            @foreach($tipos as $tipo)
            <option value="{{ $tipo->id }}">{{ $tipo->{trans('banco.titulo')} }}</option>
            @endforeach
        </select>
        <form action="{{ route('filtro') }}" method="get" class="form-palavra">
            <input type="hidden" class="pais" name="pais" value="">
            <input type="hidden" class="tipo" name="tipo" value="">
            <input type="text" name="palavraChave" value="" placeholder="{{ trans('frontend.filtro.palavra') }}" class="palavra-chave">
            <button type="submit" class="btn-busca"><img src="{{ asset('assets/img/layout/icone-lupa-busca.svg') }}" alt="" class="img-lupa"></button>
        </form>
        <select name="joalheiros_id" class="joalheiros">
            <option value="">{{ trans('frontend.filtro.joalheiros') }}</option>
            @foreach($joalheiros as $joalheiro)
            <option value="{{ $joalheiro->id }}" href="{{ route('joalheiros.show', $joalheiro->slug) }}">{{ $joalheiro->{trans('banco.nome')} }}</option>
            @endforeach
        </select>
        <select name="escola_id" class="escolas">
            <option value="">{{ trans('frontend.filtro.escolas') }}</option>
            @foreach($escolas as $escola)
            <option value="{{ $escola->id }}" href="{{ route('escolas-e-grupos.show', $escola->slug) }}">{{ $escola->{trans('banco.nome')} }}</option>
            @endforeach
        </select>
        <select name="grupo_id" class="grupos">
            <option value="">{{ trans('frontend.filtro.grupos') }}</option>
            @foreach($grupos as $grupo)
            <option value="{{ $grupo->id }}" href="{{ route('escolas-e-grupos.show', $grupo->slug) }}">{{ $grupo->{trans('banco.nome')} }}</option>
            @endforeach
        </select>
        <select name="atelier_id" class="ateliers">
            <option value="">{{ trans('frontend.filtro.ateliers') }}</option>
            @foreach($ateliers as $atelier)
            <option value="{{ $atelier->id }}" href="{{ route('joalheiros.show', $atelier->slug) }}">{{ $atelier->{trans('banco.nome')} }}</option>
            @endforeach
        </select>
    </div>
</section>