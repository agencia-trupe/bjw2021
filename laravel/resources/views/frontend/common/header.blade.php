<header @if(Tools::routeIs(['agenda*' ,'assine-livro*'])) style="background-color: #E9EDED;" @endif>
    @if(Lang::getLocale() == "en")
    <a href="{{ route('home') }}" class="link-home @if(Tools::routeIs('home*')) active @endif" title="{{ $config->title }}">
        <img src="{{ asset('assets/img/layout/marca-bjw-permanente-ingles.svg') }}" alt="BJW" class="img-logo">
    </a>
    @elseif(Lang::getLocale() == "es")
    <a href="{{ route('home') }}" class="link-home @if(Tools::routeIs('home*')) active @endif" title="{{ $config->title }}">
        <img src="{{ asset('assets/img/layout/marca-bjw-permanente-espanhol.svg') }}" alt="BJW" class="img-logo">
    </a>
    @else
    <a href="{{ route('home') }}" class="link-home @if(Tools::routeIs('home*')) active @endif" title="{{ $config->title }}">
        <img src="{{ asset('assets/img/layout/marca-bjw-permanente-portugues.svg') }}" alt="BJW" class="img-logo">
    </a>
    @endif
    <button id="mobile-toggle" type="button" role="button">
        <span class="lines internas"></span>
    </button>
    <nav>
        @include('frontend.common.nav')
    </nav>
</header>

<!-- MODAL LOGIN -->
<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close">X</button>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <h4 class="modal-titulo" id="myModalLabel">{{ trans('frontend.modal.login-titulo') }}</h4>
            <form action="{{ route('evento.login.post') }}" method="POST">
                {!! csrf_field() !!}
                <label for="email">LOGIN (E-MAIL)</label>
                <input type="email" name="email" value="{{ old('email') }}" required>
                <label for="senha">{{ trans('frontend.modal.senha') }}</label>
                <input type="password" name="senha" value="{{ old('senha') }}" required>
                <button type="submit" class="btn btn-primary" id="btnProsseguir">{{ trans('frontend.modal.btn-prosseguir') }}</button>
            </form>
            <button type="button" class="btn btn-primary" id="btnEsqueciMinhaSenha" data-toggle="modal" data-target="#modalEsqueciMinhaSenha">{{ trans('frontend.modal.link-esqueci') }} »</button>
            <div class="criar-cadastro">
                <h4 class="modal-cadastro">{{ trans('frontend.modal.criar-cadastro') }}</h4>
                <button type="button" class="btn btn-primary" id="btnCadastrar" data-toggle="modal" data-target="#modalCadastro">{{ trans('frontend.modal.btn-criar-cadastro') }}</button>
            </div>
        </div>
    </div>
</div>

<!-- MODAL ESQUECI MINHA SENHA -->
<div class="modal fade" id="modalEsqueciMinhaSenha" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close">X</button>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <h4 class="modal-titulo" id="myModalLabel">{{ trans('frontend.modal.esq-senha-titulo') }}</h4>
            <form action="{{ route('evento.password-request.post') }}" method="POST">
                {!! csrf_field() !!}
                <label for="email">E-MAIL</label>
                <input type="email" class="email" name="email" value="{{ old('email') }}" required>
                <button type="submit" class="btn btn-primary" id="btnProsseguirEsqSenha">{{ trans('frontend.modal.btn-prosseguir') }}</button>
            </form>
        </div>
    </div>
</div>

<!-- MODAL CADASTRO -->
<div class="modal fade" id="modalCadastro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close">X</button>
    <div class="modal-dialog modal-dialog-cadastro" role="document">
        <div class="modal-content">
            <h4 class="modal-titulo" id="myModalLabel">{{ trans('frontend.modal.cadastro-titulo') }}</h4>
            <form action="{{ route('evento.register.post') }}" method="POST">
                {!! csrf_field() !!}
                <label for="email">LOGIN (E-MAIL)</label>
                <input type="email" name="email" value="{{ old('email') }}" required>
                <label for="senha">{{ trans('frontend.modal.senha') }}</label>
                <input type="password" name="senha" value="{{ old('senha') }}" required>
                <label for="senha_confirmation">{{ trans('frontend.modal.repetir-senha') }}</label>
                <input type="password" name="senha_confirmation" id="senha_confirmation" required>
                <label for="nome">{{ trans('frontend.modal.nome') }}</label>
                <input type="text" name="nome" value="{{ old('nome') }}" required>
                <label for="telefone">{{ trans('frontend.modal.telefone') }}</label>
                <input type="text" name="telefone" value="{{ old('telefone') }}" required>
                <label for="pais">{{ trans('frontend.modal.pais') }}</label>
                <input type="text" name="pais" value="{{ old('pais') }}" required>
                <button type="submit" class="btn btn-primary" id="btnProsseguirCadastro">{{ trans('frontend.modal.btn-prosseguir') }}</button>
            </form>
        </div>
    </div>
</div>

<!-- BUSCA -->
<div class="busca-header" style="display: none;">
    @include('frontend.filtro')
</div>