    <a href="{{ route('agenda') }}" class="link-nav @if(Tools::routeIs('agenda*')) active @endif">{{ trans('frontend.header.agenda') }}</a>

    <a href="{{ route('joalheiros') }}" class="link-nav @if(Tools::routeIs('joalheiros*')) active @endif">{{ trans('frontend.header.joalheiros') }}</a>

    <a href="{{ route('escolas-e-grupos') }}" class="link-nav @if(Tools::routeIs('escolas-e-grupos*')) active @endif">{{ trans('frontend.header.escolas-e-grupos') }}</a>

    <a href="https://cdn.flipsnack.com/widget/v2/widget.html?hash=1nqofa5hp8" target="_blank" class="link-nav">{{ trans('frontend.header.revista') }}</a>

    <div class="links-modal">
        <a href="#" class="link-busca">
            <p class="busca-mobile" style="display: none;">{{ trans('frontend.header.buscar') }}</p>
            <img src="{{ asset('assets/img/layout/icone-lupa-busca.svg') }}" alt="{{ trans('frontend.header.buscar') }}" class="img-lupa">
        </a>

        @if(auth('evento')->check())
        <a href="" class="link-logado">
            {{ trans('frontend.header.ola') }} {{ auth('evento')->user()->nome }}!
            <img src="{{ asset('assets/img/layout/seta-selecao-combobox.svg') }}" alt="" class="img-seta">
        </a>

        <div class="submenu-logado" style="display: none;">
            <a href="{{ route('favoritas', auth('evento')->user()->id) }}" class="logado-restrita">{{ trans('frontend.footer.link-restrita') }}</a>
            <a href="{{ route('evento.logout') }}" class="logado-logout">LOGOUT</a>
        </div>

        @else
        <button type="button" id="btnModalLogin" data-toggle="modal" data-target="#modalLogin">
            <p class="login-mobile" style="display: none;">LOGIN</p>
            <img src="{{ asset('assets/img/layout/icone-login.svg') }}" alt="Login" class="img-login">
        </button>
        @endif
    </div>

    <div class="idiomas">
        @foreach([
        'pt' => 'PT',
        'en' => 'EN',
        'es' => 'ES',
        ] as $key => $title)
        <a href="{{ route('lang', $key) }}" class="link-idioma" id="{{$key}}">{{ $title }}</a>
        @endforeach
    </div>