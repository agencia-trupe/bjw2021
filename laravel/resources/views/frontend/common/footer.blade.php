<footer>
    <div class="center">
        <article class="evento">
            @if(Lang::getLocale() == "en")
            <a href="{{ route('home') }}" class="link-home @if(Tools::routeIs('home*')) active @endif" title="{{ $config->title }}">
                <img src="{{ asset('assets/img/layout/marca-bjw-permanente-ingles.svg') }}" alt="BJW" class="img-logo">
            </a>
            @elseif(Lang::getLocale() == "es")
            <a href="{{ route('home') }}" class="link-home @if(Tools::routeIs('home*')) active @endif" title="{{ $config->title }}">
                <img src="{{ asset('assets/img/layout/marca-bjw-permanente-espanhol.svg') }}" alt="BJW" class="img-logo">
            </a>
            @else
            <a href="{{ route('home') }}" class="link-home @if(Tools::routeIs('home*')) active @endif" title="{{ $config->title }}">
                <img src="{{ asset('assets/img/layout/marca-bjw-permanente-portugues.svg') }}" alt="BJW" class="img-logo">
            </a>
            @endif
        </article>

        <article class="nav-footer">
            <a href="{{ route('home') }}" class="link-footer @if(Tools::routeIs('home*')) active @endif" title="{{ trans('frontend.footer.link-home') }}">// {{ trans('frontend.footer.link-home') }} {{ $config->title }}</a>
            <a href="{{ route('agenda') }}" class="link-footer @if(Tools::routeIs('agenda*')) active @endif" title="{{ trans('frontend.footer.link-agenda') }}">// {{ trans('frontend.footer.link-agenda') }}</a>
            <a href="{{ route('joalheiros') }}" class="link-footer @if(Tools::routeIs('joalheiros*')) active @endif" title="{{ trans('frontend.footer.link-joalheiros') }}">// {{ trans('frontend.footer.link-joalheiros') }}</a>
            <a href="{{ route('escolas-e-grupos') }}" class="link-footer @if(Tools::routeIs('escolas-e-grupos*')) active @endif" title="{{ trans('frontend.footer.link-grupos') }}">// {{ trans('frontend.footer.link-grupos') }}</a>
            <a href="https://cdn.flipsnack.com/widget/v2/widget.html?hash=1nqofa5hp8" target="_blank" class="link-footer" title="{{ trans('frontend.footer.link-revista') }}">// {{ trans('frontend.footer.link-revista') }}</a>
            @if(auth('evento')->check())
            <a href="{{ route('favoritas', auth('evento')->user()->id) }}" class="link-footer @if(Tools::routeIs('area-restrita*')) active @endif" title="{{ trans('frontend.footer.link-restrita') }}">// {{ trans('frontend.footer.link-restrita') }}</a>
            @else
            <a href="" class="link-footer link-deslogado" title="{{ trans('frontend.footer.link-restrita') }}">// {{ trans('frontend.footer.link-restrita') }}</a>
            @endif
        </article>

        <article class="informacoes">
            <img src="{{ asset('assets/img/evento/'.$evento->{trans('banco.realizacao_logo')}) }}" alt="{{ $evento->{trans('banco.realizacao_nome')} }}" class="img-nucleo">

            <div class="realizacao">
                <p class="titulo">{{ trans('frontend.footer.realizacao') }}:</p>
                <p class="nome">{{ $evento->{trans('banco.realizacao_nome')} }}</p>
                <a href="{{ $evento->realizacao_website }}" target="_blank" class="link-realizacao">{{ str_replace("/", "", str_replace("https://", "", $evento->realizacao_website)) }}</a>
                <a href="https://api.whatsapp.com/send?phone={{ str_replace(' ','', $evento->realizacao_whatsapp) }}" class="link-whatsapp" target="_blank">{{ $evento->realizacao_whatsapp }}</a>
            </div>

            <div class="direitos">
                <a href="{{ route('politica-de-privacidade') }}" class="link-politica @if(Tools::routeIs('politica-de-privacidade*')) active @endif" title="{{ trans('frontend.footer.politica-de-privacidade') }}">{{ trans('frontend.footer.politica-de-privacidade') }}</a>
                <p class="copyright">© {{ date('Y') }} {{ $config->title }} | {{ trans('frontend.footer.direitos') }}</p>
                <div class="trupe">
                    <a href="https://www.trupe.net" target="_blank" class="link-trupe">{{ trans('frontend.footer.criacao') }}</a>
                    <a href="https://www.trupe.net" target="_blank" class="link-trupe">Trupe Agência Criativa</a>
                </div>
            </div>
        </article>
    </div>
</footer>