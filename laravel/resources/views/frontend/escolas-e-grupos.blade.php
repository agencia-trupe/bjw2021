@extends('frontend.common.template')

@section('content')

<main class="escolas-e-grupos">

    <h2 class="grupos-titulo">{{ trans('frontend.escolas-e-grupos.titulo') }}</h2>
    
    @include('frontend.filtro')

    <div class="center">
        @foreach($grupos as $grupo)
        <a href="{{ route('escolas-e-grupos.show', $grupo->slug) }}" class="link-grupo">
            <img src="{{ asset('assets/img/grupos-escolas/thumbs/'.$grupo->capa) }}" alt="" class="img-grupo">
            <p class="nome">{{ $grupo->nome }}</p>
            <p class="pais">{{ $grupo->{trans('banco.pais_nome')} }}</p>
        </a>
        @endforeach
    </div>
    
    @include('frontend.links')

</main>

@endsection