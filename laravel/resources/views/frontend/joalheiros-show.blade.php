@extends('frontend.common.template')

@section('content')

<main class="joalheiros-show">

    <h2 class="nome-joalheiro">{{ $joalheiro->nome }}</h2>
    <h4 class="pais">{{ $joalheiro->{trans('banco.pais_nome')} }}</h4>

    <div class="loading">
        <img src="{{ asset('assets/img/layout/load.gif') }}" alt="">
    </div>

    <section class="joias center">
        @if(count($joias) > 0)
        @foreach($joias as $joia)
        <div class="joia">
            <div class="capas-joias" id="{{$joia->id}}">
                @if(isset($joia->imagem1))
                <img src="{{ asset('assets/img/joalheiros/joias/'.$joia->imagem1) }}" alt="" class="capa">
                @endif
                @if(isset($joia->imagem2))
                <img src="{{ asset('assets/img/joalheiros/joias/'.$joia->imagem2) }}" alt="" class="capa">
                @endif
                @if(isset($joia->imagem3))
                <img src="{{ asset('assets/img/joalheiros/joias/'.$joia->imagem3) }}" alt="" class="capa">
                @endif
            </div>
            <div class="dados">
                <div class="left">
                    <p class="nome">{{ $joia->{trans('banco.nome_joia')} }}</p>
                    <p class="tipo">{{ $joia->{trans('banco.tipo')} }}</p>
                    <div class="descricao">{!! $joia->{trans('banco.descricao')} !!}</div>
                    <hr class="linha-divisao">
                    @if($joia->preco_sob_consulta == 1)
                    <p class="valor">[{{ trans('frontend.joalheiros.preco') }}]</p>
                    @else
                    <p class="valor">{{ $joia->valor }}</p>
                    @endif
                </div>
                <div class="right">
                    <div class="thumbs-joias">
                        @if(isset($joia->imagem1))
                        <a href="{{ asset('assets/img/joalheiros/joias/'.$joia->imagem1) }}" class="link-variacao" id="{{$joia->id}}">
                            <img src="{{ asset('assets/img/joalheiros/joias/thumbs/'.$joia->imagem1) }}" alt="" class="variacao">
                        </a>
                        @endif
                        @if(isset($joia->imagem2))
                        <a href="{{ asset('assets/img/joalheiros/joias/'.$joia->imagem2) }}" class="link-variacao" id="{{$joia->id}}">
                            <img src="{{ asset('assets/img/joalheiros/joias/thumbs/'.$joia->imagem2) }}" alt="" class="variacao">
                        </a>
                        @endif
                        @if(isset($joia->imagem3))
                        <a href="{{ asset('assets/img/joalheiros/joias/'.$joia->imagem3) }}" class="link-variacao" id="{{$joia->id}}">
                            <img src="{{ asset('assets/img/joalheiros/joias/thumbs/'.$joia->imagem3) }}" alt="" class="variacao">
                        </a>
                        @endif
                    </div>
                    <div class="acoes">
                        @if(isset($joalheiro->whatsapp))
                        @php $formatoWhats = str_replace(" ", "", $joalheiro->whatsapp) @endphp
                        <a href="https://api.whatsapp.com/send?phone={{ $formatoWhats }}" class="whatsapp" target="_blank">
                            <img src="{{ asset('assets/img/layout/icone-whatsapp.svg') }}" alt=""> {{ trans('frontend.joalheiros.contatar') }}
                        </a>
                        @endif

                        @if(auth('evento')->check())
                        {!! Form::open([
                        'route' => ['favoritas.post', 'user' => auth('evento')->user()->id, 'joia' => $joia->id],
                        'method' => 'post'
                        ]) !!}
                        <button type="submit" class="favoritar"><img src="{{ asset('assets/img/layout/icone-joia-favoritar.svg') }}" alt="{{ trans('frontend.joalheiros.favoritar') }}"></button>
                        {!! Form::close() !!}
                        <button class="compartilhar" ype="button" id="btnModalCompartilhar" data-toggle="modal" data-target="#modalCompartilhar" joia="{{$joia->id}}"><img src="{{ asset('assets/img/layout/icone-joia-compartilhar.svg') }}" alt="{{ trans('frontend.joalheiros.compartilhar') }}"></button>

                        @else
                        <button type="submit" class="favoritar link-deslogado"><img src="{{ asset('assets/img/layout/icone-joia-favoritar.svg') }}" alt="{{ trans('frontend.joalheiros.favoritar') }}"></button>
                        <button class="compartilhar link-deslogado" ype="button" id="btnModalCompartilhar" data-toggle="modal" data-target="#modalCompartilhar"><img src="{{ asset('assets/img/layout/icone-joia-compartilhar.svg') }}" alt="{{ trans('frontend.joalheiros.compartilhar') }}"></button>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        @if(isset($joia))
        <!-- MODAL COMPARTILHAR -->
        <div class="modal fade modal-compartilhar" id="modalCompartilhar{{$joia->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
            <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close">X</button>
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <h4 class="modal-titulo" id="myModalLabel">{{ trans('frontend.joalheiros.modal-compartilhar') }} <img src="{{ asset('assets/img/layout/ico-compartilhar.svg') }}" alt=""></h4>
                    <ul class="compartilhamento">
                        @if(auth('evento')->check())
                        <a class="comp-whatsapp" href="{{ route('relatorio.whatsapp.compartilhar', ['user' => auth('evento')->user()->id, 'joia' => $joia->id]) }}" target="_blank"><img src="{{ asset('assets/img/layout/ico-whatsapp30.svg') }}" alt=""></a>
                        <a class="comp-facebook" href="{{ route('relatorio.facebook.compartilhar', ['user' => auth('evento')->user()->id, 'joia' => $joia->id]) }}" title="Facebook" target="_blank"><img src="{{ asset('assets/img/layout/ico-facebook30.svg') }}" alt=""></a>
                        <a class="comp-email" href="{{ route('relatorio.email.compartilhar', ['user' => auth('evento')->user()->id, 'joia' => $joia->id]) }}" target="_blank" title="E-mail"><img src="{{ asset('assets/img/layout/ico-email30.svg') }}" alt=""></a>

                        @else
                        <a class="comp-whatsapp link-deslogado" href="" target="_blank"><img src="{{ asset('assets/img/layout/ico-whatsapp30.svg') }}" alt=""></a>
                        <a class="comp-facebook link-deslogado" href="" title="Facebook" target="_blank"><img src="{{ asset('assets/img/layout/ico-facebook30.svg') }}" alt=""></a>
                        <a class="comp-email link-deslogado" href="" target="_blank" title="E-mail"><img src="{{ asset('assets/img/layout/ico-email30.svg') }}" alt=""></a>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        @endif
        @endforeach
        @endif
    </section>

    <section class="joalheiro">
        <div class="center">
            <div class="textos">
                <h2 class="nome">{{ $joalheiro->nome }}</h2>
                <p class="pais">{{ $joalheiro->{trans('banco.pais_nome')} }}</p>
                <div class="texto">{!! $joalheiro->{trans('banco.texto')} !!}</div>
            </div>
            <div class="capa">
                <img src="{{ asset('assets/img/joalheiros/'.$joalheiro->capa) }}" alt="" class="img-capa">
            </div>
            <div class="contatos">
                @if(isset($joalheiro->whatsapp))
                @php $formatoWhats = str_replace(" ", "", $joalheiro->whatsapp) @endphp
                <a href="https://api.whatsapp.com/send?phone={{ $formatoWhats }}" class="whatsapp" target="_blank">
                    <img src="{{ asset('assets/img/layout/icone-whatsapp-preto.svg') }}" alt=""> {{ trans('frontend.joalheiros.conversa') }}
                </a>
                @endif

                @if(auth('evento')->check())
                @if(isset($joalheiro->instagram))
                <a href="{{ $joalheiro->instagram }}" target="_blank" class="instagram"><img src="{{ asset('assets/img/layout/icone-instagram.svg') }}" alt=""> {{ trans('frontend.joalheiros.instagram') }}</a>
                @endif
                @if(isset($joalheiro->email))
                <a href="mailto:{{ $joalheiro->email }}" target="_blank" class="email"><img src="{{ asset('assets/img/layout/icone-email.svg') }}" alt=""> {{ trans('frontend.joalheiros.email') }}</a>
                @endif
                @if(isset($joalheiro->webmail))
                <a href="{{ $joalheiro->website }}" target="_blank" class="website"><img src="{{ asset('assets/img/layout/icone-website.svg') }}" alt=""> {{ trans('frontend.joalheiros.website') }}</a>
                @endif

                @else
                @if(isset($joalheiro->instagram))
                <a href="{{ $joalheiro->instagram }}" target="_blank" class="instagram link-deslogado"><img src="{{ asset('assets/img/layout/icone-instagram.svg') }}" alt=""> {{ trans('frontend.joalheiros.instagram') }}</a>
                @endif
                @if(isset($joalheiro->email))
                <a href="mailto:{{ $joalheiro->email }}" target="_blank" class="email link-deslogado"><img src="{{ asset('assets/img/layout/icone-email.svg') }}" alt=""> {{ trans('frontend.joalheiros.email') }}</a>
                @endif
                @if(isset($joalheiro->webmail))
                <a href="{{ $joalheiro->website }}" target="_blank" class="website link-deslogado"><img src="{{ asset('assets/img/layout/icone-website.svg') }}" alt=""> {{ trans('frontend.joalheiros.website') }}</a>
                @endif

                @endif
            </div>
        </div>
    </section>

    @if(isset($processo))
    <section class="processo center">
        <h4 class="titulo">{{ trans('frontend.joalheiros.processo') }}</h4>
        <hr class="linha-titulo">
        @if($processo->video)
        <iframe src="https://www.youtube.com/embed/{{ $processo->video }}" frameborder="0" allow="autoplay; fullscreen" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

        @if(isset($processo->imagem1) || isset($processo->imagem2) || isset($processo->imagem3) || isset($processo->imagem4))
        <div class="imagens">
            @if(isset($processo->imagem1))
            <a href="{{ asset('assets/img/joalheiros/processo/'.$processo->imagem1) }}" class="fancybox" rel="processo">
                <img src="{{ asset('assets/img/joalheiros/processo/thumbs/'.$processo->imagem1) }}" alt="" class="img-processo">
            </a>
            @endif
            @if(isset($processo->imagem2))
            <a href="{{ asset('assets/img/joalheiros/processo/'.$processo->imagem2) }}" class="fancybox" rel="processo">
                <img src="{{ asset('assets/img/joalheiros/processo/thumbs/'.$processo->imagem2) }}" alt="" class="img-processo">
            </a>
            @endif
            @if(isset($processo->imagem3))
            <a href="{{ asset('assets/img/joalheiros/processo/'.$processo->imagem3) }}" class="fancybox" rel="processo">
                <img src="{{ asset('assets/img/joalheiros/processo/thumbs/'.$processo->imagem3) }}" alt="" class="img-processo">
            </a>
            @endif
            @if(isset($processo->imagem4))
            <a href="{{ asset('assets/img/joalheiros/processo/'.$processo->imagem4) }}" class="fancybox" rel="processo">
                <img src="{{ asset('assets/img/joalheiros/processo/thumbs/'.$processo->imagem4) }}" alt="" class="img-processo">
            </a>
            @endif
        </div>
        @endif

        @else
        <div class="imagens sem-video">
            @if(isset($processo->imagem1))
            <a href="{{ asset('assets/img/joalheiros/processo/'.$processo->imagem1) }}" class="fancybox" rel="processo">
                <img src="{{ asset('assets/img/joalheiros/processo/thumbs/'.$processo->imagem1) }}" alt="" class="img-processo">
            </a>
            @endif
            @if(isset($processo->imagem2))
            <a href="{{ asset('assets/img/joalheiros/processo/'.$processo->imagem2) }}" class="fancybox" rel="processo">
                <img src="{{ asset('assets/img/joalheiros/processo/thumbs/'.$processo->imagem2) }}" alt="" class="img-processo">
            </a>
            @endif
            @if(isset($processo->imagem3))
            <a href="{{ asset('assets/img/joalheiros/processo/'.$processo->imagem3) }}" class="fancybox" rel="processo">
                <img src="{{ asset('assets/img/joalheiros/processo/thumbs/'.$processo->imagem3) }}" alt="" class="img-processo">
            </a>
            @endif
            @if(isset($processo->imagem4))
            <a href="{{ asset('assets/img/joalheiros/processo/'.$processo->imagem4) }}" class="fancybox" rel="processo">
                <img src="{{ asset('assets/img/joalheiros/processo/thumbs/'.$processo->imagem4) }}" alt="" class="img-processo">
            </a>
            @endif
        </div>
        @endif

    </section>
    @endif

    @if(isset($serie))
    <section class="serie center">
        <h4 class="titulo">{{ trans('frontend.joalheiros.serie') }}</h4>
        <hr class="linha-titulo">
        @if($serie->video)
        <iframe src="https://www.youtube.com/embed/{{ $serie->video }}" frameborder="0" allow="autoplay; fullscreen" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

        @if(isset($serie->imagem1) || isset($serie->imagem2) || isset($serie->imagem3) || isset($serie->imagem4))
        <div class="imagens">
            @if(isset($serie->imagem1))
            <a href="{{ asset('assets/img/joalheiros/serie/'.$serie->imagem1) }}" class="fancybox" rel="serie">
                <img src="{{ asset('assets/img/joalheiros/serie/thumbs/'.$serie->imagem1) }}" alt="" class="img-serie">
            </a>
            @endif
            @if(isset($serie->imagem2))
            <a href="{{ asset('assets/img/joalheiros/serie/'.$serie->imagem2) }}" class="fancybox" rel="serie">
                <img src="{{ asset('assets/img/joalheiros/serie/thumbs/'.$serie->imagem2) }}" alt="" class="img-serie">
            </a>
            @endif
            @if(isset($serie->imagem3))
            <a href="{{ asset('assets/img/joalheiros/serie/'.$serie->imagem3) }}" class="fancybox" rel="serie">
                <img src="{{ asset('assets/img/joalheiros/serie/thumbs/'.$serie->imagem3) }}" alt="" class="img-serie">
            </a>
            @endif
            @if(isset($serie->imagem4))
            <a href="{{ asset('assets/img/joalheiros/serie/'.$serie->imagem4) }}" class="fancybox" rel="serie">
                <img src="{{ asset('assets/img/joalheiros/serie/thumbs/'.$serie->imagem4) }}" alt="" class="img-serie">
            </a>
            @endif
        </div>
        @endif

        @else
        <div class="imagens sem-video">
            @if(isset($serie->imagem1))
            <a href="{{ asset('assets/img/joalheiros/serie/'.$serie->imagem1) }}" class="fancybox" rel="serie">
                <img src="{{ asset('assets/img/joalheiros/serie/thumbs/'.$serie->imagem1) }}" alt="" class="img-serie">
            </a>
            @endif
            @if(isset($serie->imagem2))
            <a href="{{ asset('assets/img/joalheiros/serie/'.$serie->imagem2) }}" class="fancybox" rel="serie">
                <img src="{{ asset('assets/img/joalheiros/serie/thumbs/'.$serie->imagem2) }}" alt="" class="img-serie">
            </a>
            @endif
            @if(isset($serie->imagem3))
            <a href="{{ asset('assets/img/joalheiros/serie/'.$serie->imagem3) }}" class="fancybox" rel="serie">
                <img src="{{ asset('assets/img/joalheiros/serie/thumbs/'.$serie->imagem3) }}" alt="" class="img-serie">
            </a>
            @endif
            @if(isset($serie->imagem4))
            <a href="{{ asset('assets/img/joalheiros/serie/'.$serie->imagem4) }}" class="fancybox" rel="serie">
                <img src="{{ asset('assets/img/joalheiros/serie/thumbs/'.$serie->imagem4) }}" alt="" class="img-serie">
            </a>
            @endif
        </div>
        @endif
    </section>
    @endif

    <section class="instagram center">

    </section>

    @include('frontend.filtro')

    <section class="joalheiros-destaques center">
        <h2 class="titulo">{{ trans('frontend.joalheiros.destaques') }}</h2>
        <hr class="linha-titulo">
        <article id="joalheirosDestaquesInternas">
            @foreach($joalheiros as $joalheiro)
            <a href="{{ route('joalheiros.show', $joalheiro->slug) }}" class="link-joalheiro">
                <img src="{{ asset('assets/img/joalheiros/thumbs/'.$joalheiro->capa) }}" alt="{{ $joalheiro->nome }}" class='img-joalheiro'>
                <p class='nome-joalheiro'>{{ $joalheiro->nome }}</p>
                <p class='pais'>{{ $joalheiro->{trans('banco.pais_nome')} }}</p>
            </a>
            @endforeach
        </article>
        <a href="{{ route('joalheiros') }}" class="link-ver-mais">{{ trans('frontend.home.ver-mais') }}</a>
    </section>

    @include('frontend.links')


</main>

@endsection