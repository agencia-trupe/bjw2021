<div class="links-evento">
    @if(auth('evento')->check())
    <a href="{{ route('ebook.download', auth('evento')->user()->id) }}" class="btn-ebook">{{ trans('frontend.home.obtenha-ebook') }} <img src="{{ asset('assets/img/layout/icone-ebook.svg') }}" alt="" class="img-ebook"></a>
    <a href="{{ route('assine-livro', auth('evento')->user()->id) }}" class="btn-livro"><img src="{{ asset('assets/img/layout/icone-assine.svg') }}" alt="" class="img-assine"> {{ trans('frontend.home.assine-livro') }}</a>
    @else
    <a href="" class="btn-ebook link-deslogado">{{ trans('frontend.home.obtenha-ebook') }} <img src="{{ asset('assets/img/layout/icone-ebook.svg') }}" alt="" class="img-ebook"></a>
    <a href="" class="btn-livro link-deslogado"><img src="{{ asset('assets/img/layout/icone-assine.svg') }}" alt="" class="img-assine"> {{ trans('frontend.home.assine-livro') }}</a>
    @endif
</div>