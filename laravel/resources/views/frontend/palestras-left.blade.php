<div class="palestras">
    <div class="fio-vertical"></div>
    @php
    $count = 0;
    $left = true;
    @endphp

    @foreach($palestras as $palestra)

    @if($palestra->agenda_id == $agenda)

    @if($count == 0)
    @if($left)
    <div class="palestra palestra-left">
        <div class="horario">{{ $palestra->horario }}
            <p class="tipo-hora">{{ trans('frontend.agenda.horario-brasil') }}</p>
        </div>
        <div class="dados">
            <p class="tipo">{{ $palestra->{trans('banco.tipo')} }}</p>
            <p class="titulo">"{{ $palestra->{trans('banco.titulo')} }}"</p>
            <p class="nome">{{ $palestra->{trans('banco.nome')} }}</p>
            <div class="descricao">{!! $palestra->{trans('banco.descricao')} !!}</div>

            @if(auth('evento')->check())
            <a href="{{ route('relatorio.zoom', ['user' => auth('evento')->user()->id, 'palestra' => $palestra->id]) }}" class="link" target="_blank">{{ $palestra->{trans('banco.nome_link')} }}<img src="{{ asset('assets/img/layout/icone-seta-link-zoom.svg') }}" alt="" class="setinha"></a>
            @if($palestra->id_reuniao)
            <a href="{{ route('relatorio.zoom', ['user' => auth('evento')->user()->id, 'palestra' => $palestra->id]) }}" class="id-reuniao" target="_blank">{{ trans('frontend.agenda.id-reuniao') }}: <strong>{{ $palestra->id_reuniao }}</strong><img src="{{ asset('assets/img/layout/icone-seta-link-zoom.svg') }}" alt="" class="setinha"></a>
            @endif
            @if($palestra->texto_link)
            <div class="texto-link">{!! $palestra->{trans('banco.texto_link')} !!}</div>
            @endif

            @else
            <a href="" class="link link-deslogado" target="_blank">{{ $palestra->{trans('banco.nome_link')} }}<img src="{{ asset('assets/img/layout/icone-seta-link-zoom.svg') }}" alt="" class="setinha"></a>
            @if($palestra->id_reuniao)
            <a href="" class="id-reuniao link-deslogado" target="_blank">{{ trans('frontend.agenda.id-reuniao') }}: <strong>{{ $palestra->id_reuniao }}</strong><img src="{{ asset('assets/img/layout/icone-seta-link-zoom.svg') }}" alt="" class="setinha"></a>
            @endif
            @if($palestra->texto_link)
            <div class="texto-link">{!! $palestra->{trans('banco.texto_link')} !!}</div>
            @endif
            @endif

            @if($palestra->senha_acesso)
            <p class="senha-acesso">{{ trans('frontend.agenda.senha') }}: <strong>{{ $palestra->senha_acesso }}</strong></p>
            @endif
        </div>
        <img src="{{ asset('assets/img/agenda/'.$palestra->imagem) }}" alt="" class="img-agenda">

        @else
        <div class="palestra palestra-left">
            <div class="horario">{{ $palestra->horario }}
                <p class="tipo-hora">{{ trans('frontend.agenda.horario-brasil') }}</p>
            </div>
            <div class="dados">
                <p class="tipo">{{ $palestra->{trans('banco.tipo')} }}</p>
                <p class="titulo">{{ $palestra->{trans('banco.titulo')} }}</p>
                <p class="nome">{{ $palestra->{trans('banco.nome')} }}</p>
                <div class="descricao">{!! $palestra->{trans('banco.descricao')} !!}</div>

                @if(auth('evento')->check())
                <a href="{{ route('relatorio.zoom', ['user' => auth('evento')->user()->id, 'palestra' => $palestra->id]) }}" class="link" target="_blank">{{ $palestra->{trans('banco.nome_link')} }}<img src="{{ asset('assets/img/layout/icone-seta-link-zoom.svg') }}" alt="" class="setinha"></a>
                @if($palestra->id_reuniao)
                <a href="{{ route('relatorio.zoom', ['user' => auth('evento')->user()->id, 'palestra' => $palestra->id]) }}" class="id-reuniao" target="_blank">{{ trans('frontend.agenda.id-reuniao') }}: <strong>{{ $palestra->id_reuniao }}</strong><img src="{{ asset('assets/img/layout/icone-seta-link-zoom.svg') }}" alt="" class="setinha"></a>
                @endif
                @if($palestra->texto_link)
                <div class="texto-link">{!! $palestra->{trans('banco.texto_link')} !!}</div>
                @endif

                @else
                <a href="" class="link link-deslogado" target="_blank">{{ $palestra->{trans('banco.nome_link')} }}<img src="{{ asset('assets/img/layout/icone-seta-link-zoom.svg') }}" alt="" class="setinha"></a>
                @if($palestra->id_reuniao)
                <a href="" class="id-reuniao link-deslogado" target="_blank">{{ trans('frontend.agenda.id-reuniao') }}: <strong>{{ $palestra->id_reuniao }}</strong><img src="{{ asset('assets/img/layout/icone-seta-link-zoom.svg') }}" alt="" class="setinha"></a>
                @endif
                @if($palestra->texto_link)
                <div class="texto-link">{!! $palestra->{trans('banco.texto_link')} !!}</div>
                @endif
                @endif

                @if($palestra->senha_acesso)
                <p class="senha-acesso">{{ trans('frontend.agenda.senha') }}: <strong>{{ $palestra->senha_acesso }}</strong></p>
                @endif
            </div>
            <img src="{{ asset('assets/img/agenda/'.$palestra->imagem) }}" alt="" class="img-agenda">
            @endif
            @endif
            @php
            $count++;
            @endphp
            @if($count == 1)
        </div>

        @php
        $count = 0;
        $left = !$left;
        @endphp

        @endif

        @endif

        @endforeach
    </div>
</div>