@extends('frontend.common.template')

@section('content')

<main class="joalheiros">

    <h2 class="joalheiros-titulo">{{ trans('frontend.joalheiros.titulo') }}</h2>

    @include('frontend.filtro')

    <div class="center itens">
        @foreach($joalheiros as $joalheiro)
        <a href="{{ route('joalheiros.show', $joalheiro->slug) }}" class="link-joalheiro">
            <img src="{{ asset('assets/img/joalheiros/thumbs/'.$joalheiro->capa) }}" alt="" class="img-joalheiro">
            <p class="nome">{{ $joalheiro->nome }}</p>
            <p class="pais">{{ $joalheiro->{trans('banco.pais_nome')} }}</p>
        </a>
        @endforeach
    </div>

    <a href="#" class="link-ver-mais joalheiros">{{ trans('frontend.home.ver-mais') }}</a>

    @include('frontend.links')

</main>

@endsection