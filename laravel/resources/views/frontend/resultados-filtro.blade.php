@extends('frontend.common.template')

@section('content')

<div class="resultados-filtro">

    <div class="center">
        <h2 class="titulo">{{ trans('frontend.filtro.resultados') }}</h2>
        <div class="itens-pesquisados">
            @if(isset($palavra))<p class="palavra"><span>{{ trans('frontend.filtro.palavra') }}:</span> {{ $palavra }} | </p>@endif
            @if(isset($pais))<p class="pais"><span>{{ trans('frontend.filtro.pais') }}:</span> {{ $pais->{trans('banco.nome')} }} | </p>@endif
            @if(isset($tipo))<p class="tipo"><span>{{ trans('frontend.filtro.tipo') }}:</span> {{ $tipo->{trans('banco.titulo')} }}</p>@endif
        </div>

        @if(count($joias) > 0)
        <div class="joias-filtro">
            @foreach($joias as $joia)
            <div class="joia">
                <img src="{{ asset('assets/img/joalheiros/joias/'.$joia->imagem1) }}" alt="" class="img-joia">
                <p class="nome-joia">{{ $joia->{trans('banco.nome_joia')} }}</p>
                <p class="joalheiro">{{ $joia->nome }} • {{ $joia->{trans('banco.pais_nome')} }}</p>
                @if($joia->preco_sob_consulta == 1)
                <p class="valor">[{{ trans('frontend.joalheiros.preco') }}]</p>
                @else
                <p class="valor">{{ $joia->valor }}</p>
                @endif
                <div class="acoes">
                    @if(auth('evento')->check())
                    {!! Form::open([
                    'route' => ['favoritas.post', 'user' => auth('evento')->user()->id, 'joia' => $joia->id],
                    'method' => 'post'
                    ]) !!}
                    <button type="submit" class="favoritar"><img src="{{ asset('assets/img/layout/icone-joia-favoritar.svg') }}" alt="{{ trans('frontend.joalheiros.favoritar') }}"></button>
                    {!! Form::close() !!}
                    <button class="compartilhar" ype="button" id="btnModalCompartilhar" data-toggle="modal" data-target="#modalCompartilhar" joia="{{$joia->id}}"><img src="{{ asset('assets/img/layout/icone-joia-compartilhar.svg') }}" alt="{{ trans('frontend.joalheiros.compartilhar') }}"></button>

                    @else
                    <button type="submit" class="favoritar link-deslogado"><img src="{{ asset('assets/img/layout/icone-joia-favoritar.svg') }}" alt="{{ trans('frontend.joalheiros.favoritar') }}"></button>
                    <button class="compartilhar link-deslogado" ype="button" id="btnModalCompartilhar" data-toggle="modal" data-target="#modalCompartilhar"><img src="{{ asset('assets/img/layout/icone-joia-compartilhar.svg') }}" alt="{{ trans('frontend.joalheiros.compartilhar') }}"></button>
                    @endif
                </div>
            </div>

            @if(isset($joia))
            <!-- MODAL COMPARTILHAR -->
            <div class="modal fade modal-compartilhar" id="modalCompartilhar{{$joia->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
                <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close">X</button>
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <h4 class="modal-titulo" id="myModalLabel">{{ trans('frontend.joalheiros.modal-compartilhar') }} <img src="{{ asset('assets/img/layout/ico-compartilhar.svg') }}" alt=""></h4>
                        <ul class="compartilhamento">
                            @if(auth('evento')->check())
                            <a class="comp-whatsapp" href="{{ route('relatorio.whatsapp.compartilhar', ['user' => auth('evento')->user()->id, 'joia' => $joia->id]) }}" target="_blank"><img src="{{ asset('assets/img/layout/ico-whatsapp30.svg') }}" alt=""></a>
                            <a class="comp-facebook" href="{{ route('relatorio.facebook.compartilhar', ['user' => auth('evento')->user()->id, 'joia' => $joia->id]) }}" title="Facebook" target="_blank"><img src="{{ asset('assets/img/layout/ico-facebook30.svg') }}" alt=""></a>
                            <a class="comp-email" href="{{ route('relatorio.email.compartilhar', ['user' => auth('evento')->user()->id, 'joia' => $joia->id]) }}" target="_blank" title="E-mail"><img src="{{ asset('assets/img/layout/ico-email30.svg') }}" alt=""></a>

                            @else
                            <a class="comp-whatsapp link-deslogado" href="" target="_blank"><img src="{{ asset('assets/img/layout/ico-whatsapp30.svg') }}" alt=""></a>
                            <a class="comp-facebook link-deslogado" href="" title="Facebook" target="_blank"><img src="{{ asset('assets/img/layout/ico-facebook30.svg') }}" alt=""></a>
                            <a class="comp-email link-deslogado" href="" target="_blank" title="E-mail"><img src="{{ asset('assets/img/layout/ico-email30.svg') }}" alt=""></a>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            @endif
            @endforeach
        </div>
        @else
        <div class="joias-filtro">
            <p class="nenhuma">{{ trans('frontend.filtro.nenhuma') }}</p>
        </div>
        @endif
    </div>

</div>

@include('frontend.filtro')

@include('frontend.links')

@endsection