@extends('frontend.common.template')

@section('content')

<main class="escolas-e-grupos-show">

    <div class="center">
        <h2 class="nome">{{ $grupo->nome }}</h2>
        <img src="{{ asset('assets/img/grupos-escolas/'.$grupo->capa) }}" alt="" class="img-capa">
        <div class="dados">
            <div class="left">
                @if(isset($grupo->whatsapp))
                @php $formatoWhats = str_replace(" ", "", $grupo->whatsapp) @endphp
                <a href="https://api.whatsapp.com/send?phone={{ $formatoWhats }}" class="whatsapp" target="_blank">
                    <img src="{{ asset('assets/img/layout/icone-whatsapp.svg') }}" alt=""> {{ trans('frontend.escolas-e-grupos.conversa') }}
                </a>
                @endif
                <p class="nome-grupo">{{ $grupo->nome }}</p>
                <p class="pais">{{ $grupo->{trans('banco.pais_nome')} }}</p>
            </div>
            <div class="right">
                {!! $grupo->{trans('banco.texto')} !!}
            </div>
        </div>
    </div>

    <div class="bg-cinza">
        @if(isset($grupo->imagem1) || isset($grupo->imagem2) || isset($grupo->imagem3) || isset($grupo->imagem4) || isset($grupo->imagem5) || isset($grupo->imagem6))
        <div class="imagens center">
            @if(isset($grupo->imagem1))
            <a href="{{ asset('assets/img/grupos-escolas/imagens/'.$grupo->imagem1) }}" class="fancybox" rel="grupos">
                <img src="{{ asset('assets/img/grupos-escolas/imagens/thumbs/'.$grupo->imagem1) }}" alt="" class="img-grupo">
            </a>
            @endif
            @if(isset($grupo->imagem2))
            <a href="{{ asset('assets/img/grupos-escolas/imagens/'.$grupo->imagem2) }}" class="fancybox" rel="grupos">
                <img src="{{ asset('assets/img/grupos-escolas/imagens/thumbs/'.$grupo->imagem2) }}" alt="" class="img-grupo">
            </a>
            @endif
            @if(isset($grupo->imagem3))
            <a href="{{ asset('assets/img/grupos-escolas/imagens/'.$grupo->imagem3) }}" class="fancybox" rel="grupos">
                <img src="{{ asset('assets/img/grupos-escolas/imagens/thumbs/'.$grupo->imagem3) }}" alt="" class="img-grupo">
            </a>
            @endif
            @if(isset($grupo->imagem4))
            <a href="{{ asset('assets/img/grupos-escolas/imagens/'.$grupo->imagem4) }}" class="fancybox" rel="grupos">
                <img src="{{ asset('assets/img/grupos-escolas/imagens/thumbs/'.$grupo->imagem4) }}" alt="" class="img-grupo">
            </a>
            @endif
            @if(isset($grupo->imagem5))
            <a href="{{ asset('assets/img/grupos-escolas/imagens/'.$grupo->imagem5) }}" class="fancybox" rel="grupos">
                <img src="{{ asset('assets/img/grupos-escolas/imagens/thumbs/'.$grupo->imagem5) }}" alt="" class="img-grupo">
            </a>
            @endif
            @if(isset($grupo->imagem6))
            <a href="{{ asset('assets/img/grupos-escolas/imagens/'.$grupo->imagem6) }}" class="fancybox" rel="grupos">
                <img src="{{ asset('assets/img/grupos-escolas/imagens/thumbs/'.$grupo->imagem6) }}" alt="" class="img-grupo">
            </a>
            @endif
        </div>
        @endif

        @if($grupo->video)
        <iframe src="https://www.youtube.com/embed/{{ $grupo->video }}" frameborder="0" allow="autoplay; fullscreen" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        @endif

        <div class="contatos center">
            @if(isset($grupo->whatsapp))
            @php $formatoWhats = str_replace(" ", "", $grupo->whatsapp) @endphp
            <a href="https://api.whatsapp.com/send?phone={{ $formatoWhats }}" class="whatsapp" target="_blank">
                <img src="{{ asset('assets/img/layout/icone-whatsapp-preto.svg') }}" alt=""> {{ trans('frontend.escolas-e-grupos.conversa') }}
            </a>
            @endif

            @if(auth('evento')->check())
            @if(isset($grupo->instagram))
            <a href="{{ $grupo->instagram }}" target="_blank" class="instagram"><img src="{{ asset('assets/img/layout/icone-instagram.svg') }}" alt=""> {{ trans('frontend.joalheiros.instagram') }}</a>
            @endif
            @if(isset($grupo->email))
            <a href="mailto:{{ $grupo->email }}" target="_blank" class="email"><img src="{{ asset('assets/img/layout/icone-email.svg') }}" alt=""> {{ trans('frontend.joalheiros.email') }}</a>
            @endif
            @if(isset($grupo->webmail))
            <a href="{{ $grupo->website }}" target="_blank" class="website"><img src="{{ asset('assets/img/layout/icone-website.svg') }}" alt=""> {{ trans('frontend.joalheiros.website') }}</a>
            @endif

            @else
            @if(isset($grupo->instagram))
            <a href="{{ $grupo->instagram }}" target="_blank" class="instagram link-deslogado"><img src="{{ asset('assets/img/layout/icone-instagram.svg') }}" alt=""> {{ trans('frontend.joalheiros.instagram') }}</a>
            @endif
            @if(isset($grupo->email))
            <a href="mailto:{{ $grupo->email }}" target="_blank" class="email link-deslogado"><img src="{{ asset('assets/img/layout/icone-email.svg') }}" alt=""> {{ trans('frontend.joalheiros.email') }}</a>
            @endif
            @if(isset($grupo->webmail))
            <a href="{{ $grupo->website }}" target="_blank" class="website link-deslogado"><img src="{{ asset('assets/img/layout/icone-website.svg') }}" alt=""> {{ trans('frontend.joalheiros.website') }}</a>
            @endif

            @endif
        </div>
    </div>

    @include('frontend.filtro')

    <section class="escolas-destaques">
        <div class="center">
            <h2 class="titulo">{{ trans('frontend.escolas-e-grupos.destaques') }}</h2>
            <hr class="linha-titulo">
            <article id="escolasDestaquesInternas">
                @foreach($escolas as $escola)
                <a href="{{ route('escolas-e-grupos.show', $escola->slug) }}" class='link-escola'>
                    <img src="{{ asset('assets/img/grupos-escolas/thumbs/'.$escola->capa) }}" alt="{{ $escola->nome }}" class='img-escola'>
                    <p class='nome-escola'>{{ $escola->nome }}</p>
                    <p class='pais'>{{ $escola->{trans('banco.pais_nome')} }}</p>
                </a>
                @endforeach
            </article>
            <a href="{{ route('escolas-e-grupos') }}" class="link-ver-mais">{{ trans('frontend.home.ver-mais') }}</a>
        </div>
    </section>

    @include('frontend.links')

</main>

@endsection