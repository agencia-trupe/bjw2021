@extends('frontend.common.template')

@section('content')

<main class="agenda">
    <h2 class="agenda-titulo">{{ trans('frontend.agenda.titulo') }}</h2>

    @if(count($palestras))
    <div class="agenda-completa">
        @php
        $count = 0;
        $white = true;
        @endphp

        @foreach($agendas as $agenda)

        @if($count == 0)
        @if($white)

        <div class="agenda-geral agenda-gray">
            <div class="center">
                @php
                $dataCompleta = strftime("%d %B %Y", strtotime($agenda->data));
                $dataDia = strftime("%A", strtotime($agenda->data));
                @endphp
                @if($agenda->dia_evento)
                <p class="dia-evento">{{ $agenda->{trans('banco.dia_evento')} }} • {{ $dataCompleta }} • {{ $dataDia }}</p>
                @else
                <p class="dia-evento">{{ $dataCompleta }} • {{ $dataDia }}</p>
                @endif
                @include('frontend.palestras-right', ['agenda' => $agenda->id])
            </div>

            @else
            <div class="agenda-geral agenda-white">
                <div class="center">
                    @php
                    $dataCompleta = strftime("%d %B %Y", strtotime($agenda->data));
                    $dataDia = strftime("%A", strtotime($agenda->data));
                    @endphp
                    @if($agenda->dia_evento)
                    <p class="dia-evento">{{ $agenda->{trans('banco.dia_evento')} }} • {{ $dataCompleta }} • {{ $dataDia }}</p>
                    @else
                    <p class="dia-evento">{{ $dataCompleta }} • {{ $dataDia }}</p>
                    @endif
                    @include('frontend.palestras-left', ['agenda' => $agenda->id])
                </div>

                @endif
                @endif

                @php
                $count++;
                @endphp
                @if($count == 1)
            </div>

            @php
            $count = 0;
            $white = !$white;
            @endphp

            @endif

            @endforeach
        </div>
    </div>

    <button class="btn-agenda-mais">{{ trans('frontend.agenda.ver-mais') }} +</button>

    <section class="agenda-videos">
        <h2 class="titulo-videos">{{ trans('frontend.agenda.videos') }}</h2>

        @foreach($videos as $video)
        <article class="video center">
            <iframe src="https://www.youtube.com/embed/{{ $video->video }}" frameborder="0" allow="autoplay; fullscreen" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            <p class="titulo-video">{{ $video->{trans('banco.titulo')} }}</p>
        </article>
        @endforeach
    </section>
    @endif

    @include('frontend.filtro')

    <section class="joias-destaques-agenda">
        <div class="center">
            <h2 class="titulo">{{ trans('frontend.home.titulo-joias') }}</h2>
            <hr class="linha-titulo">
            <article id="joiasDestaquesAgenda">
                @foreach($joias as $joia)
                <div class="joia">
                    <a href="{{ route('joalheiros.show', $joia->slug_joalheiro) }}" class="link-joia" id="{{ $joia->id }}" title="{{ $joia->nome_joalheiro }}">
                        <img src="{{ asset('assets/img/joalheiros/joias/'.$joia->imagem1) }}" class="img-joia" style="max-width:100%">
                        <p class="nome-joia">{{ $joia->{trans('banco.nome_joia')} }}</p>
                        <p class="joalheiro">{{ $joia->nome_joalheiro }} • {{ $joia->{trans('banco.pais_nome')} }}</p>
                        @if($joia->preco_sob_consulta == 1)
                        <p class="preco-joia">[{{ trans('frontend.joalheiros.preco') }}]</p>
                        @else
                        <p class="preco-joia">{{ $joia->valor }}</p>
                        @endif
                    </a>
                    <div class="acoes">
                        @if(auth('evento')->check())
                        {!! Form::open([
                        'route' => ['favoritas.post', 'user' => auth('evento')->user()->id, 'joia' => $joia->id],
                        'method' => 'post'
                        ]) !!}
                        <button type="submit" class="favoritar"><img src="{{ asset('assets/img/layout/icone-joia-favoritar.svg') }}" alt="{{ trans('frontend.joalheiros.favoritar') }}"></button>
                        {!! Form::close() !!}
                        <button class="compartilhar" ype="button" id="btnModalCompartilhar" data-toggle="modal" data-target="#modalCompartilhar" joia="{{$joia->id}}"><img src="{{ asset('assets/img/layout/icone-joia-compartilhar.svg') }}" alt="{{ trans('frontend.joalheiros.compartilhar') }}"></button>

                        @else
                        <button type="submit" class="favoritar link-deslogado"><img src="{{ asset('assets/img/layout/icone-joia-favoritar.svg') }}" alt="{{ trans('frontend.joalheiros.favoritar') }}"></button>
                        <button class="compartilhar link-deslogado" ype="button" id="btnModalCompartilhar" data-toggle="modal" data-target="#modalCompartilhar"><img src="{{ asset('assets/img/layout/icone-joia-compartilhar.svg') }}" alt="{{ trans('frontend.joalheiros.compartilhar') }}"></button>
                        @endif
                    </div>
                </div>

                <!-- MODAL COMPARTILHAR -->
                <div class="modal fade modal-compartilhar" id="modalCompartilhar{{$joia->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
                    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close">X</button>
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <h4 class="modal-titulo" id="myModalLabel">{{ trans('frontend.joalheiros.modal-compartilhar') }} <img src="{{ asset('assets/img/layout/ico-compartilhar.svg') }}" alt=""></h4>
                            <ul class="compartilhamento">
                                @if(auth('evento')->check())
                                <a class="comp-whatsapp" href="{{ route('relatorio.whatsapp.compartilhar', ['user' => auth('evento')->user()->id, 'joia' => $joia->id]) }}" target="_blank"><img src="{{ asset('assets/img/layout/ico-whatsapp30.svg') }}" alt=""></a>
                                <a class="comp-facebook" href="{{ route('relatorio.facebook.compartilhar', ['user' => auth('evento')->user()->id, 'joia' => $joia->id]) }}" title="Facebook" target="_blank"><img src="{{ asset('assets/img/layout/ico-facebook30.svg') }}" alt=""></a>
                                <a class="comp-email" href="{{ route('relatorio.email.compartilhar', ['user' => auth('evento')->user()->id, 'joia' => $joia->id]) }}" target="_blank" title="E-mail"><img src="{{ asset('assets/img/layout/ico-email30.svg') }}" alt=""></a>

                                @else
                                <a class="comp-whatsapp link-deslogado" href="" target="_blank"><img src="{{ asset('assets/img/layout/ico-whatsapp30.svg') }}" alt=""></a>
                                <a class="comp-facebook link-deslogado" href="" title="Facebook" target="_blank"><img src="{{ asset('assets/img/layout/ico-facebook30.svg') }}" alt=""></a>
                                <a class="comp-email link-deslogado" href="" target="_blank" title="E-mail"><img src="{{ asset('assets/img/layout/ico-email30.svg') }}" alt=""></a>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                @endforeach
            </article>
            <a href="{{ route('joalheiros') }}" class="link-ver-mais">{{ trans('frontend.home.ver-mais') }}</a>
        </div>
    </section>

    @include('frontend.links')

</main>

@endsection