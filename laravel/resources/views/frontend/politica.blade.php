@extends('frontend.common.template')

@section('content')

<div class="politica center">
    <h2 class="titulo">{{ trans('frontend.footer.politica-de-privacidade') }}</h2>
    <div class="texto">{!! $politica->{trans('banco.texto')} !!}</div>
</div>

@include('frontend.filtro')

@include('frontend.links')

@endsection