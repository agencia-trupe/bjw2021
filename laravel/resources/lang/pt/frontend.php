<?php

return [

    'header' => [
        'agenda'            => 'ENCONTROS E MAIS',
        'joalheiros'        => 'JOIAS & JOALHEIROS',
        'escolas-e-grupos'  => 'ESCOLAS & GRUPOS',
        'revista'           => 'REVISTA',
        'buscar'            => 'Buscar',
        'ola'               => 'OLÁ',
    ],

    'modal' => [
        'login-titulo'       => 'FAÇA LOGIN',
        'senha'              => 'SENHA',
        'btn-prosseguir'     => 'PROSSEGUIR',
        'link-esqueci'       => 'esqueci minha senha',
        'criar-cadastro'     => 'OU CRIE SEU CADASTRO',
        'btn-criar-cadastro' => 'CADASTRAR-ME',
        'esq-senha-titulo'   => 'ESQUECI MINHA SENHA',
        'cadastro-titulo'    => 'PRIMEIRO CADASTRO',
        'repetir-senha'      => 'REPETIR SENHA',
        'nome'               => 'NOME',
        'telefone'           => 'TELEFONE',
        'pais'               => 'PAÍS',
        'redefinicao-senha'  => 'REDEFINIÇÃO DE SENHA',
    ],

    'footer' => [
        'link-home'            => 'HOME ∙ EVENTO',
        'link-agenda'          => 'ENCONTROS E MAIS',
        'link-busca'           => 'BUSCA PERSONALIZADA',
        'link-joalheiros'      => 'JOIAS & JOALHEIROS',
        'link-grupos'          => 'ESCOLAS & GRUPOS',
        'link-revista'         => 'REVISTA JOALHERIA CONTEMPORÂNEA',
        'link-restrita'        => 'ÁREA RESTRITA',
        'realizacao'           => 'realização',
        'politica-de-privacidade' => 'POLÍTICA DE PRIVACIDADE',
        'direitos'             => 'TODOS OS DIREITOS RESERVADOS',
        'criacao'              => 'criação de sites: ',
    ],

    'home' => [
        'agenda'                   => 'ENCONTROS E MAIS',
        'titulo-joias'             => 'JOIAS EM DESTAQUE',
        'joia'                     => 'Joia',
        'ver-mais'                 => 'VER MAIS +',
        'titulo-joalheiros'        => 'JOALHEIROS EM DESTAQUE',
        'entrar'                   => 'ENTRAR',
        'titulo-escolas'           => 'ESCOLAS EM DESTAQUE',
        'titulo-grupos'            => 'GRUPOS EM DESTAQUE',
        'titulo-ateliers'          => 'ATELIERS EM DESTAQUE',
        'cronometro-faltam'        => 'Faltam',
        'cronometro-dias'          => 'dias',
        'cronometro-horas'         => 'horas e',
        'cronometro-minutos-aofim' => 'minutos para a 4ª edição do evento • saiba mais »',
        'obtenha-ebook'            => 'OBTENHA O E-BOOK DA EDIÇÃO DE 2020',
        'assine-livro'             => 'ASSINE O LIVRO DO EVENTO 2021',
    ],

    'agenda' => [
        'titulo'         => 'ENCONTROS E MAIS',
        'transmissao'    => 'Transmissão',
        'id-reuniao'     => 'ID da reunião',
        'senha'          => 'Senha de acesso',
        'ver-mais'       => 'VER MAIS',
        'horario-brasil' => 'horário do Brasil',
        'videos'         => 'CONFIRA EVENTOS QUE JÁ ACONTECERAM',
    ],

    'filtro' => [
        'titulo'     => 'ENCONTRE',
        'palavra'    => 'palavra-chave',
        'pais'       => 'PAÍS',
        'tipo'       => 'TIPO DE JOIA',
        'joalheiros' => 'JOALHEIROS INDIVIDUAIS',
        'escolas'    => 'ESCOLAS',
        'grupos'     => 'GRUPOS',
        'ateliers'   => 'ATELIERS',
        'resultados' => 'RESULTADO DA PESQUISA',
        'nenhuma'    => 'Nenhum resultado encontrado.',
        'titulo-joalheiros' => 'JOALHEIROS & ATELIERS',
        'titulo-grupos'     => 'ESCOLAS & GRUPOS',
    ],

    'joalheiros' => [
        'titulo'        => 'CONHEÇA OS JOALHEIROS',
        'contatar'      => 'CONTATAR JOALHEIRO',
        'favoritar'     => 'Favoritar Joia',
        'compartilhar'  => 'Compartilhar Joia',
        'conversa'      => 'INICIAR CONVERSA COM ESTE JOALHEIRO',
        'instagram'     => 'CONFERIR O INSTAGRAM',
        'email'         => 'ENVIAR E-MAIL',
        'website'       => 'VISITAR O WEBSITE',
        'processo'      => 'POR TRÁS DA OBRA',
        'serie'         => 'MAIS SOBRE A SÉRIE',
        'destaques'     => 'CONHEÇA MAIS • JOALHEIROS EM DESTAQUE',
        'preco'         => 'preço sob consulta',
        'modal-compartilhar' => 'COMPARTILHAR',
    ],

    'escolas-e-grupos' => [
        'titulo'    => 'CONHEÇA AS ESCOLAS E GRUPOS',
        'conversa'  => 'INICIAR CONVERSA COM ESTE CONTATO',
        'destaques' => 'CONHEÇA MAIS • ESCOLAS EM DESTAQUE',
    ],

    'favoritas' => [
        'nenhuma'            => 'Lista de peças favoritas vazia.',
        'titulo'             => 'LISTA DE PEÇAS FAVORITAS',
        'exluir'             => 'DESFAVORITAR JOIA',
        'compartilhar'       => 'COMPARTILHAR JOIA',
        'atualizar-cadastro' => 'MANTER MEU CADASTRO ATUALIZADO',
        'btn-atualizar'      => 'ATUALIZAR',
    ],

    'assine-livro' => [
        'titulo'         => 'ASSINE O LIVRO DO EVENTO',
        'mensagem'       => 'DEIXE A SUA MENSAGEM',
        'aviso'          => 'A assinatura será o nome e país que constam no seu cadastro conosco.',
        'btn-assinar'    => 'ASSINAR',
        'ver-mais'       => 'VER MAIS',
    ],

    'reset' => [
        'titulo'        => 'REDEFINIÇÃO DE SENHA',
        'btn-redefinir' => 'REDEFINIR SENHA',
        'recuperacao'   => 'Recuperação de senha',
        'ola'           => 'Olá',
        'frase'         => 'Você solicitou recuperação da sua senha em nosso sistema. As senhas são criptografadas e seguras, portanto você deve criar uma nova senha acessando o link',
        'clique-aqui'   => 'Clique aqui para redefinir sua senha.',
    ],

];
