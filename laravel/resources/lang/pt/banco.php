<?php

return [

    'dia_evento'      => 'dia_evento',
    'tipo'            => 'tipo',
    'titulo'          => 'titulo',
    'descricao'       => 'descricao',
    'nome'            => 'nome',
    'texto'           => 'texto',
    'periodo'         => 'periodo',
    'evento_texto'    => 'evento_texto',
    'evento_duracao'  => 'evento_duracao',
    'nucleo_logo'     => 'nucleo_logo',
    'nucleo_texto'    => 'nucleo_texto',
    'realizacao_logo' => 'realizacao_logo',
    'realizacao_nome' => 'realizacao_nome',
    'nome_joia'       => 'nome_joia',
    'pais_nome'       => 'pais_nome',
    'tipo'            => 'tipo',
    'nome_link'       => 'nome_link',
    'texto_link'      => 'texto_link',
];
