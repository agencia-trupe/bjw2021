<?php

return [

    'dia_evento'      => 'dia_evento_es',
    'tipo'            => 'tipo_es',
    'titulo'          => 'titulo_es',
    'descricao'       => 'descricao_es',
    'nome'            => 'nome_es',
    'texto'           => 'texto_es',
    'periodo'         => 'periodo_es',
    'evento_texto'    => 'evento_texto_es',
    'evento_duracao'  => 'evento_duracao_es',
    'nucleo_logo'     => 'nucleo_logo_es',
    'nucleo_texto'    => 'nucleo_texto_es',
    'realizacao_logo' => 'realizacao_logo_es',
    'realizacao_nome' => 'realizacao_nome_es',
    'nome_joia'       => 'nome_joia_es',
    'pais_nome'       => 'pais_nome_es',
    'tipo'            => 'tipo_es',
    'nome_link'       => 'nome_link_es',
    'texto_link'      => 'texto_link_es',

];
