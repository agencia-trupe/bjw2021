<?php

return [

    'header' => [
        'agenda'            => 'CITAS Y MAS',
        'joalheiros'        => 'JOYAS Y JOYEROS',
        'escolas-e-grupos'  => 'ESCUELAS Y GRUPOS',
        'revista'           => 'REVISTA',
        'buscar'            => 'Buscar',
        'ola'               => 'HOLA',
    ],

    'modal' => [
        'login-titulo'       => 'INICIAR SESIÓN',
        'senha'              => 'CONTRASEÑA',
        'btn-prosseguir'     => 'CONTINUAR',
        'link-esqueci'       => 'olvide mi contraseña',
        'criar-cadastro'     => 'O CREA TU REGISTRO',
        'btn-criar-cadastro' => 'REGISTRAME',
        'esq-senha-titulo'   => 'OLVIDE MI CONTRASEÑA',
        'cadastro-titulo'    => 'PRIMERA INSCRIPCIÓN',
        'repetir-senha'      => 'REPITE LA CONTRASEÑA',
        'nome'               => 'NOMBRE',
        'telefone'           => 'TELÉFONO',
        'pais'               => 'PADRES',
        'redefinicao-senha'  => 'RESTABLECIMIENTO DE CONTRASEÑA',
    ],

    'footer' => [
        'link-home'            => 'INICIO ∙ EVENTO',
        'link-agenda'          => 'CITAS Y MAS',
        'link-busca'           => 'BÚSQUEDA PERSONALIZADA',
        'link-joalheiros'      => 'JOYAS Y JOYEROS',
        'link-grupos'          => 'ESCUELAS Y GRUPOS',
        'link-revista'         => 'REVISTA DE JOYERÍA CONTEMPORÁNEA',
        'link-restrita'        => 'ÁREA RESTRINGIDA',
        'realizacao'           => 'logro',
        'politica-de-privacidade' => 'POLÍTICA DE PRIVACIDAD',
        'direitos'             => 'TODOS LOS DERECHOS RESERVADOS',
        'criacao'              => 'creación de sitio web: ',
    ],

    'home' => [
        'agenda'                   => 'CITAS Y MAS',
        'titulo-joias'             => 'JOYAS DESTACADAS',
        'joia'                     => 'Joya',
        'ver-mais'                 => 'VER MÁS +',
        'titulo-joalheiros'        => 'JOYEROS DESTACADOS',
        'entrar'                   => 'ENTRAR',
        'titulo-escolas'           => 'ESCUELAS DESTACADAS',
        'titulo-grupos'            => 'GRUPOS DESTACADOS',
        'titulo-ateliers'          => 'ATELIERS DESTACADOS',
        'cronometro-faltam'        => 'Quedan',
        'cronometro-dias'          => 'días',
        'cronometro-horas'         => 'horas y',
        'cronometro-minutos-aofim' => 'minutos para la 4ª edición del evento • saber más »',
        'obtenha-ebook'            => 'OBTENGA EL E-BOOK DE LA EDICIÓN 2020',
        'assine-livro'             => 'FIRMA EL LIBRO DE EVENTOS 2021',
    ],

    'agenda' => [
        'titulo'         => 'CITAS Y MAS',
        'transmissao'    => 'Transmisión',
        'id-reuniao'     => 'ID de reunión',
        'senha'          => 'Contraseña de acceso',
        'ver-mais'       => 'VER MÁS',
        'horario-brasil' => 'horario de Brasil',
        'videos'         => 'CONSULTA LOS EVENTOS QUE YA HAN SUCEDIDO',
    ],

    'filtro' => [
        'titulo'     => 'ENCONTRAR',
        'palavra'    => 'palabra clave',
        'pais'       => 'PADRES',
        'tipo'       => 'TIPO DE GEMA',
        'joalheiros' => 'JOYEROS INDIVIDUALES',
        'escolas'    => 'ESCUELAS',
        'grupos'     => 'GRUPOS',
        'ateliers'   => 'ATELIERS',
        'resultados' => 'RESULTADO DE BÚSQUEDA',
        'nenhuma'    => 'Ningún resultado encontrado.',
        'titulo-joalheiros' => 'JOYEROS Y ATELEROS',
        'titulo-grupos'     => 'ESCUELAS Y GRUPOS',
    ],

    'joalheiros' => [
        'titulo'        => 'CONOCE A LOS JOYEROS',
        'contatar'      => 'CONTACTO JOYEROS',
        'favoritar'     => 'Gema favorita',
        'compartilhar'  => 'Compartir gema',
        'conversa'      => 'EMPEZAR UNA CONVERSACIÓN CON ESTE JOYERO',
        'instagram'     => 'VER INSTAGRAM',
        'email'         => 'ENVIAR CORREO ELECTRÓNICO',
        'website'       => 'VISITA EL SITIO WEB',
        'processo'      => 'DETRÁS DEL TRABAJO',
        'serie'         => 'MÁS SOBRE LA SERIE',
        'destaques'     => 'MÁS INFORMACIÓN • JOYEROS DESTACADOS',
        'preco'         => 'precio en demanda',
        'modal-compartilhar' => 'PARA COMPARTIR',
    ],

    'escolas-e-grupos' => [
        'titulo'    => 'DESCUBRE LAS ESCUELAS Y LOS GRUPOS',
        'conversa'  => 'INICIAR CONVERSACIÓN CON ESTE CONTACTO',
        'destaques' => 'MÁS INFORMACIÓN • ESCUELAS DESTACADAS',
    ],

    'favoritas' => [
        'nenhuma'            => 'Lista de piezas favoritas vacía.',
        'titulo'             => 'LISTA DE PIEZAS FAVORITAS',
        'exluir'             => 'JOYA DESFAVORITA',
        'compartilhar'       => 'COMPARTIR GEMA',
        'atualizar-cadastro' => 'MANTENGA MI REGISTRO ACTUALIZADO',
        'btn-atualizar'      => 'ACTUALIZAR',
    ],

    'assine-livro' => [
        'titulo'         => 'FIRMA EL LIBRO DEL EVENTO',
        'mensagem'       => 'DEJA TU MENSAJE',
        'aviso'          => 'La firma será el nombre y el país que aparecen en su registro con nosotros.',
        'btn-assinar'    => 'PARA FIRMAR',
        'ver-mais'       => 'VER MÁS',
    ],

    'reset' => [
        'titulo'        => 'RESTABLECIMIENTO DE CONTRASEÑA',
        'btn-redefinir' => 'REDEFINIR CONTRASEÑA',
        'recuperacao'   => 'Recuperación de contraseña',
        'ola'           => 'Hola',
        'frase'         => 'Ha solicitado la recuperación de su contraseña en nuestro sistema. Las contraseñas están encriptadas y son seguras, por lo que debe crear una nueva contraseña yendo al enlace',
        'clique-aqui'   => 'Haga clic aquí para restablecer la contraseña.',
    ],

];
