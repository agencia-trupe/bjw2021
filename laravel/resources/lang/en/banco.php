<?php

return [

    'dia_evento'      => 'dia_evento_en',
    'tipo'            => 'tipo_en',
    'titulo'          => 'titulo_en',
    'descricao'       => 'descricao_en',
    'nome'            => 'nome_en',
    'texto'           => 'texto_en',
    'periodo'         => 'periodo_en',
    'evento_texto'    => 'evento_texto_en',
    'evento_duracao'  => 'evento_duracao_en',
    'nucleo_logo'     => 'nucleo_logo_en',
    'nucleo_texto'    => 'nucleo_texto_en',
    'realizacao_logo' => 'realizacao_logo_en',
    'realizacao_nome' => 'realizacao_nome_en',
    'nome_joia'       => 'nome_joia_en',
    'pais_nome'       => 'pais_nome_en',
    'tipo'            => 'tipo_en',
    'nome_link'       => 'nome_link_en',
    'texto_link'      => 'texto_link_en',
    
];
