<?php

return [

    'header' => [
        'agenda'            => 'MORE ABOUT',
        'joalheiros'        => 'JEWELS & JEWELERS',
        'escolas-e-grupos'  => 'SCHOOLS & GROUPS',
        'revista'           => 'MAGAZINE',
        'buscar'            => 'Search',
        'ola'               => 'HELLO',
    ],

    'modal' => [
        'login-titulo'       => 'LOG IN',
        'senha'              => 'PASSWORD',
        'btn-prosseguir'     => 'PROCEED',
        'link-esqueci'       => 'I forgot my password',
        'criar-cadastro'     => 'OR CREATE YOUR REGISTRATION',
        'btn-criar-cadastro' => 'REGISTER ME',
        'esq-senha-titulo'   => 'I FORGOT MY PASSWORD',
        'cadastro-titulo'    => 'FIRST REGISTRATION',
        'repetir-senha'      => 'REPEAT PASSWORD',
        'nome'               => 'NAME',
        'telefone'           => 'TELEPHONE',
        'pais'               => 'PARENTS',
        'redefinicao-senha'  => 'PASSWORD RESET',
    ],

    'footer' => [
        'link-home'            => 'HOME ∙ EVENT',
        'link-agenda'          => 'MORE ABOUT',
        'link-busca'           => 'CUSTOM SEARCH',
        'link-joalheiros'      => 'JEWELS & JEWELERS',
        'link-grupos'          => 'SCHOOLS & GROUPS',
        'link-revista'         => 'CONTEMPORARY JEWELRY MAGAZINE',
        'link-restrita'        => 'RESTRICTED AREA',
        'realizacao'           => 'achievement',
        'politica-de-privacidade' => 'PRIVACY POLICY',
        'direitos'             => 'ALL RIGHTS RESERVED',
        'criacao'              => 'website creation: ',
    ],

    'home' => [
        'agenda'                   => 'MORE ABOUT',
        'titulo-joias'             => 'FEATURED JEWELS',
        'joia'                     => 'Jewel',
        'ver-mais'                 => 'SEE MORE +',
        'titulo-joalheiros'        => 'FEATURED JEWELERS',
        'entrar'                   => 'TO ENTER',
        'titulo-escolas'           => 'FEATURED SCHOOLS',
        'titulo-grupos'            => 'FEATURED GROUPS',
        'titulo-ateliers'          => 'FEATURED ATELIERS',
        'cronometro-faltam'        => 'There are',
        'cronometro-dias'          => 'days',
        'cronometro-horas'         => 'hours and',
        'cronometro-minutos-aofim' => 'minutes to the 4th edition of the event • learn more »',
        'obtenha-ebook'            => 'GET THE 2020 EDITION E-BOOK',
        'assine-livro'             => 'SIGN THE 2021 EVENT BOOK',
    ],

    'agenda' => [
        'titulo'         => 'MORE ABOUT',
        'transmissao'    => 'Streaming',
        'id-reuniao'     => 'meeting ID',
        'senha'          => 'Access password',
        'ver-mais'       => 'VER MAIS',
        'horario-brasil' => "Brazil's time",
        'videos'         => 'CHECK OUT EVENTS THAT HAVE ALREADY HAPPENED',
    ],

    'filtro' => [
        'titulo'     => 'FIND',
        'palavra'    => 'keyword',
        'pais'       => 'PARENTS',
        'tipo'       => 'TYPE OF GEM',
        'joalheiros' => 'INDIVIDUAL JEWELERS',
        'escolas'    => 'SCHOOLS',
        'grupos'     => 'GROUPS',
        'ateliers'   => 'ATELIERS',
        'resultados' => 'RESEARCH RESULT',
        'nenhuma'    => 'No results found.',
        'titulo-joalheiros' => 'JEWELERS & ATELIERS',
        'titulo-grupos'     => 'SCHOOLS & GROUPS',
    ],

    'joalheiros' => [
        'titulo'        => 'MEET THE JEWELERS',
        'contatar'      => 'CONTACT JEWELERS',
        'favoritar'     => 'Favorite Gem',
        'compartilhar'  => 'Share Gem',
        'conversa'      => 'START A CONVERSATION WITH THIS JEWELER',
        'instagram'     => 'CHECK INSTAGRAM',
        'email'         => 'SEND E-MAIL',
        'website'       => 'VISIT THE WEBSITE',
        'processo'      => 'BEHIND THE WORK',
        'serie'         => 'MORE ABOUT THE SERIES',
        'destaques'     => 'FIND OUT MORE • FEATURED JEWELERS',
        'preco'         => 'price on request',
        'modal-compartilhar' => 'TO SHARE',
    ],

    'escolas-e-grupos' => [
        'titulo'    => 'DISCOVER THE SCHOOLS AND GROUPS',
        'conversa'  => 'START CONVERSATION WITH THIS CONTACT',
        'destaques' => 'FIND OUT MORE • FEATURED SCHOOLS',
    ],

    'favoritas' => [
        'nenhuma'            => 'Empty favorite parts list.',
        'titulo'             => 'LIST OF FAVORITE PARTS',
        'exluir'             => 'DISFAVORITE GEM',
        'compartilhar'       => 'SHARE GEM',
        'atualizar-cadastro' => 'KEEP MY REGISTRATION UPDATED',
        'btn-atualizar'      => 'UPDATE',
    ],

    'assine-livro' => [
        'titulo'         => 'SIGN THE EVENT BOOK',
        'mensagem'       => 'LEAVE YOUR MESSAGE',
        'aviso'          => 'The signature will be the name and country that appear on your registration with us.',
        'btn-assinar'    => 'TO SIGN',
        'ver-mais'       => 'SEE MORE',
    ],

    'reset' => [
        'titulo'        => 'PASSWORD RESET',
        'btn-redefinir' => 'REDEFINE PASSWORD',
        'recuperacao'   => 'Password recovery',
        'ola'           => 'Hello',
        'frase'         => 'You have requested recovery of your password on our system. Passwords are encrypted and secure, so you must create a new password by going to the link',
        'clique-aqui'   => 'Click here to reset your password.',
    ],

];
