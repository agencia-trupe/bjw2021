import AjaxSetup from "./AjaxSetup";
import MobileToggle from "./MobileToggle";

AjaxSetup();
MobileToggle();

$(window).on("load", function () {
  // OK - IMAGENS JOIAS - SLICK CAROUSEL
  $(".capas-joias").slick({
    dots: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    speed: 500,
    fade: true,
  });
  $(".loading").hide();
  $("section.joias").css({ visibility: "visible", "max-height": "100%" });

  // favoritas
  $(".joias-favoritas .imgs-joias").slick({
    dots: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    speed: 500,
    fade: true,
  });
  $(".loading").hide();
  $(".joias-favoritas").css({ visibility: "visible", "max-height": "100%" });
});

$(document).ready(function () {
  // HEADER - active lang
  var langActive = $(".lang-active").html();
  $("header .link-idioma#" + langActive).addClass("active");

  // HEADER - submenu logado
  if ($(window).width() < 1200) {
    $("header .link-logado").click(function (e) {
      e.preventDefault();
      $("header .submenu-logado").css("display", "none");
      $("header .link-logado.sub-active").removeClass("sub-active");
      $(this).parent().children(".submenu-logado").css("display", "flex");
      $(this).addClass("sub-active");
    });
  } else {
    $("header .link-logado").mouseenter(function () {
      $(this).parent().children(".submenu-logado").css("display", "flex");
      $(this).addClass("sub-active");
    });
    $("header .submenu-logado").mouseleave(function () {
      $(this).css("display", "none");
      $("header .link-logado.sub-active").removeClass("sub-active");
    });
  }

  // HEADER - busca
  $("header .link-busca").click(function (e) {
    e.preventDefault();
    $(".busca-header").show();
    $(".busca-header .filtro").css("margin-top", "0");
    e.stopPropagation();
  });
  $(".busca-header").click(function (e) {
    e.stopPropagation();
  });
  $(window).scroll(function () {
    $(".busca-header").hide();
  });
  $(document).click(function () {
    $(".busca-headerr").hide();
  });

  // HEADER - busca e login (se mobile)
  if ($(window).width() < 1200) {
    $("header .busca-mobile, header .login-mobile").css("display", "block");
  }

  // HOME - BANNERS
  $(".banners-joalheiros, .banners-revista").cycle({
    slides: ".banner",
    fx: "fade",
    speed: 4000,
    timeout: 2000,
  });

  // HOME - CRONOMETRO PARA FIM DO EVENTO
  var dataInicio = $(".data-inicio").html();
  $(".cronometro-evento").countdown(dataInicio, function (event) {
    $(".x-dias").html(event.strftime("%-D"));
    $(".x-horas").html(event.strftime("%-H"));
    $(".x-minutos").html(event.strftime("%-M"));
  });

  // AGENDA - ver mais
  var itensAgenda = $(".agenda-geral");
  var spliceItensAgenda = 2;
  if (itensAgenda.length <= spliceItensAgenda) {
    $(".btn-agenda-mais").hide();
  }
  var setDivAgenda = function () {
    var spliceItens = itensAgenda.splice(0, spliceItensAgenda);
    $(".agenda-completa").append(spliceItens);
    $(spliceItens).show();
    if ($(spliceItens[0]).length > 0) {
      $("html,body .agenda-completa").animate(
        { scrollTop: $(spliceItens[0]).offset().top - 200 },
        "slow"
      );
    }
    if (itensAgenda.length <= 0) {
      $(".btn-agenda-mais").hide();
    }
  };
  $(".btn-agenda-mais").click(function () {
    setDivAgenda();
  });
  $(".agenda-geral").hide();
  setDivAgenda();

  // JOALHEIROS - ver mais
  var itensJoalheiros = $("main.joalheiros .link-joalheiro");
  var spliceItensJoalheiros = 20;
  if (itensJoalheiros.length <= spliceItensJoalheiros) {
    $(".link-ver-mais.joalheiros").hide();
  }
  var setDivJoalheiros = function () {
    var spliceItens = itensJoalheiros.splice(0, spliceItensJoalheiros);
    $("main.joalheiros .center.itens").append(spliceItens);
    $(spliceItens).show();
    if (itensJoalheiros.length <= 0) {
      $(".link-ver-mais.joalheiros").hide();
    }
  };
  $(".link-ver-mais.joalheiros").click(function (e) {
    e.preventDefault();
    setDivJoalheiros();
  });
  $("main.joalheiros .link-joalheiro").hide();
  setDivJoalheiros();

  // JOALHEIROS - VARIAÇÕES IMGS JOIAS
  $("main.joalheiros-show .joias .link-variacao").click(function (e) {
    e.preventDefault();
    var linkAtivo = $(this).attr("href");
    var id = $(this).attr("id");
    $("main.joalheiros-show .capas-joias#" + id + " .capa").attr(
      "src",
      linkAtivo
    );
  });

  $("main.joalheiros-show .joias .link-variacao:nth-child(3n)").css(
    "margin-right",
    "0"
  );

  // PROCESSO E SÉRIE - IMAGENS - fancybox
  $(".fancybox").fancybox({
    padding: 0,
    prevEffect: "fade",
    nextEffect: "fade",
    closeBtn: false,
    openEffect: "elastic",
    openSpeed: "150",
    closeEffect: "elastic",
    closeSpeed: "150",
    helpers: {
      title: {
        type: "outside",
        position: "top",
      },
      overlay: {
        css: {
          background: "rgba(132, 134, 136, .88)",
        },
      },
    },
    fitToView: false,
    autoSize: false,
    beforeShow: function () {
      this.maxWidth = "90%";
      this.maxHeight = "90%";
    },
  });

  // MODAL LOGIN / ESQUECI MINHA SENHA / CADASTRO
  $("#btnModalLogin").click(function () {
    $("#modalLogin").css("display", "flex");
    $("#modalEsqueciMinhaSenha").css("display", "none");
    $("#modalCadastro").css("display", "none");
  });

  $("#btnEsqueciMinhaSenha").click(function () {
    $("#modalLogin").css("display", "none");
    $("#modalEsqueciMinhaSenha").css("display", "flex");
  });

  $("#btnCadastrar").click(function () {
    $("#modalLogin").css("display", "none");
    $("#modalEsqueciMinhaSenha").css("display", "none");
    $("#modalCadastro").css("display", "flex");
  });

  $(".btn-close").click(function () {
    $("#modalLogin").css("display", "none");
    $("#modalEsqueciMinhaSenha").css("display", "none");
    $("#modalCadastro").css("display", "none");
    $(".modal-compartilhar").css("display", "none");
  });

  // MODAL COMPARTILHAR
  $(".compartilhar").click(function () {
    var idJoia = $(this).attr("joia");
    console.log(idJoia);
    $("#modalCompartilhar"+idJoia).css("display", "flex");
    $("#modalCompartilhar"+idJoia).css("position", "fixed");
  });

  // USUARIO DESLOGADO
  $(".link-deslogado").click(function (e, auth) {
    e.preventDefault();
    var auth = $(".itens-menu .logado-nome").html();
    if (auth == "" || auth == undefined) {
      $("#modalLogin").css("display", "flex");
      $("#modalLogin").css("position", "fixed");
    }
  });

  // JOALHEIROS + GRUPOS - margin-right
  $("main.joalheiros .link-joalheiro:nth-child(4n)").css("margin-right", "0");
  $("main.escolas-e-grupos .link-grupo:nth-child(4n)").css("margin-right", "0");
  $("main.escolas-e-grupos-show .fancybox:nth-child(3n)").css(
    "margin-right",
    "0"
  );
  $(".resultados-filtro .joalheiros .link-joalheiro:nth-child(4n)").css("margin-right", "0");
  $(".resultados-filtro .grupos .link-grupo:nth-child(4n)").css("margin-right", "0");

  // ASSINE LIVRO - GRID ASSINATURAS
  var $grid = $(".masonry-grid")
    .masonry({
      itemSelector: ".grid-item",
      columnWidth: ".grid-item",
      percentPosition: true,
    })
    .on("layoutComplete", function (event, laidOutItems) {
      $(".masonry-grid").masonry("layout");
    });

  // ASSINE LIVRO - BTN VER MAIS ASSINATURAS
  var itensAssinaturas = $(".grid-item");
  var spliceItensAssinaturas = 4;

  if (itensAssinaturas.length <= spliceItensAssinaturas) {
    $(".btn-assinaturas-mais").hide();
  }

  var setDivAssinatura = function () {
    var spliceItens = itensAssinaturas.splice(0, spliceItensAssinaturas);
    $(".masonry-grid").append(spliceItens);
    $(spliceItens).show();
    $(".masonry-grid").masonry("layout");
    if (itensAssinaturas.length <= 0) {
      $(".btn-assinaturas-mais").hide();
    }
  };

  $(".btn-assinaturas-mais").click(function () {
    setDivAssinatura();
  });

  $(".grid-item").hide();
  setDivAssinatura();

  // FILTRO - select tipo
  $("body").on("change", ".filtro .tipos", function (e) {
    e.preventDefault();
    e.stopPropagation();

    var tipoSelecionado = $(this).val();
    console.log(tipoSelecionado);
    $(".filtro .form-palavra input.tipo").val(tipoSelecionado);
  });

  // FILTRO - select pais
  $("body").on("change", ".filtro .paises", function (e) {
    e.preventDefault();
    e.stopPropagation();

    var paisSelecionado = $(this).val();
    console.log(paisSelecionado);
    $(".filtro .form-palavra input.pais").val(paisSelecionado);

    filtroPais(paisSelecionado);
  });

  var filtroPais = function (paisSelecionado) {
    var url = "/filtro/" + paisSelecionado;

    $.ajax({
      type: "GET",
      url: url,
      success: function (data, textStatus, jqXHR) {
        console.log(data);
        var langActive = $(".lang-active").html();

        $(".filtro .joalheiros").html("");
        $(".filtro .escolas").html("");
        $(".filtro .grupos").html("");
        $(".filtro .ateliers").html("");

        if (langActive == "en") {
          var optionJoalheiros =
            "<option value='' selected>INDIVIDUAL JEWELERS</option>";
          var optionEscolas = "<option value='' selected>SCHOOLS</option>";
          var optionGrupos = "<option value='' selected>GROUPS</option>";
          var optionAteliers = "<option value='' selected>ATELIERS</option>";
        } else if (langActive == "es") {
          var optionJoalheiros =
            "<option value='' selected>JOYEROS INDIVIDUALES</option>";
          var optionEscolas = "<option value='' selected>ESCUELAS</option>";
          var optionGrupos = "<option value='' selected>GRUPOS</option>";
          var optionAteliers = "<option value='' selected>ATELIERS</option>";
        } else {
          var optionJoalheiros =
            "<option value='' selected>JOALHEIROS INDIVIDUAIS</option>";
          var optionEscolas = "<option value='' selected>ESCOLAS</option>";
          var optionGrupos = "<option value='' selected>GRUPOS</option>";
          var optionAteliers = "<option value='' selected>ATELIERS</option>";
        }

        $(".filtro .joalheiros").append(optionJoalheiros);
        $(".filtro .escolas").append(optionEscolas);
        $(".filtro .grupos").append(optionGrupos);
        $(".filtro .ateliers").append(optionAteliers);

        if (data.joalheiros != null) {
          data.joalheiros.forEach((joalheiro) => {
            optionJoalheiros =
              "<option value='" +
              joalheiro.id +
              "' href='" +
              window.location.origin +
              "/joalheiros/" +
              joalheiro.slug +
              "'>" +
              joalheiro.nome +
              "</option>";
            $(".filtro .joalheiros").append(optionJoalheiros);
          });
        }

        if (data.escolas != null) {
          data.escolas.forEach((escola) => {
            optionEscolas =
              "<option value='" +
              escola.id +
              "' href='" +
              window.location.origin +
              "/escolas-e-grupos/" +
              escola.slug +
              "'>" +
              escola.nome +
              "</option>";
            $(".filtro .escolas").append(optionEscolas);
          });
        }

        if (data.grupos != null) {
          data.grupos.forEach((grupo) => {
            optionGrupos =
              "<option value='" +
              grupo.id +
              "' href='" +
              window.location.origin +
              "/escolas-e-grupos/" +
              grupo.slug +
              "'>" +
              grupo.nome +
              "</option>";
            $(".filtro .grupos").append(optionGrupos);
          });
        }

        if (data.ateliers != null) {
          data.ateliers.forEach((atelier) => {
            optionAteliers =
              "<option value='" +
              atelier.id +
              "' href='" +
              window.location.origin +
              "/joalheiros/" +
              atelier.slug +
              "'>" +
              atelier.nome +
              "</option>";
            $(".filtro .ateliers").append(optionAteliers);
          });
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR, textStatus, errorThrown);
      },
    });
  };

  // FILTRO - select joalheiros, grupos, escolas e ateliers
  $("body").on(
    "change",
    ".filtro .joalheiros, .filtro .escolas, .filtro .grupos, .filtro .ateliers",
    function (e) {
      e.preventDefault();
      var urlOption = $(this).children("option:selected").attr("href");
      if (urlOption) {
        window.location = urlOption;
      }
      return false;
    }
  );
});
