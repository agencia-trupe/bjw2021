export default function MobileToggle() {
  var $handle = $("#mobile-toggle"),
    $nav = $("header nav");

  $handle.on("click touchstart", function (event) {
    event.preventDefault();
    $nav.slideToggle("medium", function () {
      if ($(this).is(":visible")) $(this).css("display", "flex");
    });
    $handle.toggleClass("close");
  });
}
